/* Console Child */
%h{
#include <gnome.h>
#include <parser.h>
#include "drgenius-mdi-child.h"
%}

%{
#include "config.h"
#include <gnome.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "drgeo_gtkdrawable.h"

#include "drgeo-mdi-child.h"
#include "drgeo-mdi-child-private.h"
%}

%{
extern GnomeMDI *mdi;

%}

class :DrGeo:MDI:Child from DrGenius:MDI:Child
{
	private drgeoFigure * figure;

	init(child)
	{
		DRGENIUS_MDI_CHILD(child)->child_type = FIGURE_CHILD;
		DRGENIUS_MDI_CHILD(child)->supports_save = TRUE;
		DRGENIUS_MDI_CHILD(child)->filename = NULL;
		child->_priv->figure = NULL;
	}


	public
	DrGeniusMDIChild * new(xmlNodePtr drgeoXml) 
	{
		DrGeoMDIChild *child;
		static int counter = 1;

		child = (DrGeoMDIChild *)GET_NEW;
		
		child->_priv->figure = new drgeoFigure (drgeoXml);

		if (drgeoXml == NULL) 
		{ 
			/* create a new name */
			GNOME_MDI_CHILD(child)->name =
				g_strdup_printf (_("Untitled Figure %d"), counter++);
		}
		else 
		{ 
			/* get the name from the XML Tree */		    
			GNOME_MDI_CHILD(child)->name =
				(gchar *) (xmlGetProp (drgeoXml, BAD_CAST "name"));
		}
		return DRGENIUS_MDI_CHILD(child);
	}	

	/* FIXME: Ugly, return gpointer instead of drgeoFigure*, 
	   this is what happend when mixing C and C++ */
	public
	gpointer get_figure (DrGenius:MDI:Child * self (check null type))
		onerror NULL
	{
		DrGeoMDIChild *child = DRGEO_MDI_CHILD(self);

		return (gpointer) (child->_priv->figure);
	}

	override (DrGenius:MDI:Child)
	gboolean save (DrGenius:MDI:Child *self (check null type),
	               const char *filename (check null)) onerror FALSE
	{
	  gint ret;
	  xmlDocPtr xml;

	        DrGeoMDIChild *child = DRGEO_MDI_CHILD (self);
	
		if (self->filename != filename)
		{
			g_free (self->filename);
			self->filename = g_strdup (filename);
		}
		gnome_mdi_child_set_name (GNOME_MDI_CHILD(self),g_basename(filename));
		xml = xmlNewDoc (BAD_CAST "1.0");
		xml->root = xmlNewDocNode (xml, NULL,
					   BAD_CAST "drgenius", NULL);
		child->_priv->figure->saveAs (xml->root, g_basename(filename));
		ret = xmlSaveFile (filename, xml);
		xmlFreeDoc (xml);
		return (ret != -1);
	}
	
	override (DrGenius:MDI:Child)
	gboolean undo (DrGenius:MDI:Child *self, gint n (check >= 0)) onerror FALSE
	{
	        DrGeoMDIChild *child = DRGEO_MDI_CHILD (self);
		child->_priv->figure->undo (n);
		return TRUE;
	}

	override (DrGenius:MDI:Child)
  	gboolean redo (DrGenius:MDI:Child *self, gint n (check >= 0)) onerror FALSE
	{
	        DrGeoMDIChild *child = DRGEO_MDI_CHILD (self);
		child->_priv->figure->redo (n);
		return TRUE;
	}

	override (DrGenius:MDI:Child)
	gboolean undoActive (DrGenius:MDI:Child *self)
	{
	        DrGeoMDIChild *child = DRGEO_MDI_CHILD (self);
		return child->_priv->figure->undoActive ();
	}

	override (DrGenius:MDI:Child)
	gboolean redoActive (DrGenius:MDI:Child *self)
	{
	        DrGeoMDIChild *child = DRGEO_MDI_CHILD (self);
		return child->_priv->figure->redoActive ();
	}

	override (Gnome:MDI:Child)
	GtkWidget * create_view (Gnome:MDI:Child *self (check null type), gpointer data)
	{
		/* create the class drgeoGtkDrawable there */
		/* there, the class drgeoFigure is already created */
		drgeoGtkDrawable *view;

		DrGeoMDIChild *child = DRGEO_MDI_CHILD(self);

		view = new drgeoGtkDrawable (child->_priv->figure);
		child->_priv->figure->setDrawable ((drgeoDrawable *) view);

		return (view->getGtkWidget ());
	}

	override(Gtk:Object)
	void destroy(Gtk:Object * self (check null type))
	{
		DrGeoMDIChild *child = DRGEO_MDI_CHILD(self);

		delete ((drgeoGtkDrawable *) (child->_priv->figure->getDrawable ()));
		delete (child->_priv->figure);
		g_free (DRGENIUS_MDI_CHILD(child)->filename);
		g_free (GNOME_MDI_CHILD(child)->name);

		if(* GTK_OBJECT_CLASS (parent_class)->destroy)
			(* GTK_OBJECT_CLASS (parent_class)->destroy) (self);
	}	
}

%{

/* nothing here */

%}
