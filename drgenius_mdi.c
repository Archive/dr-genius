#include <config.h>
#include <glade/glade.h>

#include "drgenius_mdi.h"
#include "drgenius_menus.h"
#include "drgeo_view.h"
#include "genius_stuff.h"
#include "gobobjs/drgenius-mdi-child.h"
#include "gobobjs/geledit-mdi-child.h"
#include "gobobjs/console-mdi-child.h"
#include "gobobjs/drgeo-mdi-child.h"

static void mdi_destroy_handler (GnomeMDI * mdi);
static void app_created_handler (GnomeMDI * mdi, GnomeApp * app);
static void child_changed_handler (GnomeMDI * mdi, GnomeMDIChild * child);
static gint remove_child_handler (GnomeMDI * mdi, GnomeMDIChild * child);
static void app_deleted_handler (GnomeApp * widget, gpointer data);
static gint remove_view_handler (GnomeMDI * mdi, GtkWidget * view);

GnomeMDI *
drgenius_mdi_new (const gchar * file_name)
{
	GnomeMDI *mdi;

	mdi = GNOME_MDI (gnome_mdi_new (PACKAGE, _ ("Dr Genius")));
	gnome_mdi_set_mode (mdi, GNOME_MDI_NOTEBOOK);

	/* connect signals */
	gtk_signal_connect (GTK_OBJECT (mdi), "destroy",
			    GTK_SIGNAL_FUNC (mdi_destroy_handler), NULL);
	gtk_signal_connect (GTK_OBJECT (mdi), "remove_view",
			    GTK_SIGNAL_FUNC (remove_view_handler), NULL);
	gtk_signal_connect (GTK_OBJECT (mdi), "app_created",
			    GTK_SIGNAL_FUNC (app_created_handler), NULL);
	gtk_signal_connect (GTK_OBJECT (mdi), "child_changed",
			    GTK_SIGNAL_FUNC (child_changed_handler), NULL);
	gtk_signal_connect (GTK_OBJECT (mdi), "remove_child",
			    GTK_SIGNAL_FUNC (remove_child_handler), NULL);

	return (mdi);
}

void 
drgenius_mdi_close (GnomeMDI * mdi)
{
	/* This function can be invoked to explicitly close the application.  */
	if (!gnome_mdi_remove_all (mdi, FALSE)) {
		/* Warn the user if there are unsaved files.  */
		/* XXX */
	}
}

static void 
mdi_destroy_handler (GnomeMDI * mdi)
{
	/* on destruction of GnomeMDI we call gtk_main_quit(), since we have 
	   opened no windows on our own and therefore our GUI is gone.  */
	gtk_main_quit ();
}

static gint 
remove_view_handler (GnomeMDI * mdi, GtkWidget * view)
{
	/* This signal handler is invoked each time an MDI view is removed.  */
	/* FIXME call some checks from here to verify that the file is saved.  */
	DrGeniusMDIChild * child;
	
	child = (DrGeniusMDIChild *) gnome_mdi_get_active_child (mdi);
	/* gtk_object_destroy ((GtkObject *) child); */
	return (TRUE);		/* It's OK to remove this view.  */
}

static void 
app_created_handler (GnomeMDI * mdi, GnomeApp * app)
{
	GtkWidget *widget, *bar, *button, *toolbar;
	GladeXML *xml, *xml1;
	GnomeDockItem *dockItem;

	/* Setup the application correctly. */
	gtk_window_set_policy (GTK_WINDOW (app), FALSE, TRUE, FALSE);
	gtk_window_set_default_size (GTK_WINDOW (app), 640, 480);
	gtk_window_set_wmclass (GTK_WINDOW (app), "drgenius", "DrGenius");

	widget = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_NEVER);

	gnome_app_set_statusbar (app, widget);
	
	/* Set default menu and toolbar */
	drgeo_install_menus_and_toolbar (GTK_WIDGET (app));
 
       	/* Load specific toolbars */
	xml = glade_xml_new (DRGENIUS_GLADEDIR
			     "/drgeo.glade", "menuBar");
	glade_xml_signal_autoconnect (xml);
	bar = glade_xml_get_widget (xml, "menuBar");

	/* FIXME: LibGlade doesn't handle properly toolbar style */
	gtk_toolbar_set_style (GTK_TOOLBAR (bar), GTK_TOOLBAR_ICONS);
	gtk_widget_ref (bar);
	gtk_object_set_data (GTK_OBJECT (app), "drgeoToolbar", bar);

	/* FIXME: LibGlade doesn't set correctly the shadow style of the menu bar */ 
	/* FIXME: LibGlade doesn't handle properly radio button group, set it by hand */

	/* Prepare the sub-toolbar for the drgeoToolbar */

	/* The point Bar */
	xml1 = glade_xml_new (DRGENIUS_GLADEDIR
			     "/drgeo.glade", "pointBar");
	glade_xml_signal_autoconnect (xml1);
	widget = glade_xml_get_widget (xml1, "pointBar");
	toolbar = glade_xml_get_widget (xml1, "toolbar");
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar), GTK_RELIEF_HALF);
	button = glade_xml_get_widget (xml, "pointMenu");
	gtk_object_set_data (GTK_OBJECT (button), "subBar", widget);
	/* Attach the sub bar reference to the main toolbar so we can change 
	   their orientation */
	gtk_object_set_data (GTK_OBJECT (bar), "pointBar", toolbar);
	gtk_object_destroy (GTK_OBJECT (xml1));

	/* The curve Bar */
	xml1 = glade_xml_new (DRGENIUS_GLADEDIR
			     "/drgeo.glade", "curveBar");
	glade_xml_signal_autoconnect (xml1);
	widget = glade_xml_get_widget (xml1, "curveBar");
	toolbar = glade_xml_get_widget (xml1, "toolbar");
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar), GTK_RELIEF_HALF);
	button = glade_xml_get_widget (xml, "curveMenu");
	gtk_object_set_data (GTK_OBJECT (button), "subBar", widget);
	gtk_object_set_data (GTK_OBJECT (bar), "curveBar", toolbar);
	gtk_object_destroy (GTK_OBJECT (xml1));

	/* The transformation Bar */
	xml1 = glade_xml_new (DRGENIUS_GLADEDIR
			     "/drgeo.glade", "transformationBar");
	glade_xml_signal_autoconnect (xml1);
	widget = glade_xml_get_widget (xml1, "transformationBar");
	toolbar = glade_xml_get_widget (xml1, "toolbar");
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar), GTK_RELIEF_HALF);
	button = glade_xml_get_widget (xml, "transformationMenu");
	gtk_object_set_data (GTK_OBJECT (button), "subBar", widget);
	gtk_object_set_data (GTK_OBJECT (bar), "transformationBar", toolbar);
	gtk_object_destroy (GTK_OBJECT (xml1));

	/* The numeric Bar */
	xml1 = glade_xml_new (DRGENIUS_GLADEDIR
			     "/drgeo.glade", "numericBar");
	glade_xml_signal_autoconnect (xml1);
	widget = glade_xml_get_widget (xml1, "numericBar");
	toolbar = glade_xml_get_widget (xml1, "toolbar");
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar), GTK_RELIEF_HALF);
	button = glade_xml_get_widget (xml, "numericMenu");
	gtk_object_set_data (GTK_OBJECT (button), "subBar", widget);
	gtk_object_set_data (GTK_OBJECT (bar), "numericBar", toolbar);
	gtk_object_destroy (GTK_OBJECT (xml1));

	/* The macro Bar */
	xml1 = glade_xml_new (DRGENIUS_GLADEDIR
			     "/drgeo.glade", "macroBar");
	glade_xml_signal_autoconnect (xml1);
	widget = glade_xml_get_widget (xml1, "macroBar");
	toolbar = glade_xml_get_widget (xml1, "toolbar");
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar), GTK_RELIEF_HALF);
	button = glade_xml_get_widget (xml, "macroMenu");
	gtk_object_set_data (GTK_OBJECT (button), "subBar", widget);
	gtk_object_set_data (GTK_OBJECT (bar), "macroBar", toolbar);
	gtk_object_destroy (GTK_OBJECT (xml1));

	/* The other Bar */
	xml1 = glade_xml_new (DRGENIUS_GLADEDIR
			     "/drgeo.glade", "otherBar");
	glade_xml_signal_autoconnect (xml1);
	widget = glade_xml_get_widget (xml1, "otherBar");
	toolbar = glade_xml_get_widget (xml1, "toolbar");
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar), GTK_RELIEF_HALF);
	button = glade_xml_get_widget (xml, "otherMenu");
	gtk_object_set_data (GTK_OBJECT (button), "subBar", widget);
	gtk_object_set_data (GTK_OBJECT (bar), "otherBar", toolbar);
	gtk_object_destroy (GTK_OBJECT (xml1));

	gtk_object_destroy (GTK_OBJECT (xml));


	/* prepare the dock item for the per child toolbar */
	dockItem = GNOME_DOCK_ITEM (gnome_dock_item_new ("DockItem", 
							 GNOME_DOCK_ITEM_BEH_NEVER_FLOATING));
	gnome_app_add_dock_item (app, dockItem, GNOME_DOCK_TOP,
				 2, 0, 0); 

	

	/* Keep the reference of the dockItem of this Gnome App */
	gtk_object_set_data (GTK_OBJECT (app), "childDockItem", dockItem);

	gnome_mdi_set_child_list_path (mdi, _ ("Windows/"));

	gtk_signal_connect (GTK_OBJECT (app),
			    "delete_event",
			    GTK_SIGNAL_FUNC (app_deleted_handler),
			    NULL);
}
static void 
child_changed_handler (GnomeMDI * mdi, GnomeMDIChild * child)
{
	DrGeniusMDIChild *newChild;
	GnomeDockItem *dockItem;
	GtkWidget *bar;
	GnomeApp *app;

	newChild = (DrGeniusMDIChild *) gnome_mdi_get_active_child (mdi);
	app = gnome_mdi_get_active_window (mdi);
	dockItem = GNOME_DOCK_ITEM ( gtk_object_get_data 
				     (GTK_OBJECT (app), "childDockItem"));
	reconcile_grayout_widget ();
	if (child)
	{
		if (newChild->child_type == ((DrGeniusMDIChild *)child)->child_type)
			/* Same kind of child, do nothing */
			return;
		else
		{
			/* Different kind of child, change the child toolbar */
			/* First, eventualy remove an actual child toolbar */
			bar = gnome_dock_item_get_child (dockItem);
			if (bar)
				gtk_container_remove (GTK_CONTAINER (dockItem), bar);		
		}
	}
	/* Add the toolbar specific to the active child */
	switch (newChild->child_type) 
	{
	case FIGURE_CHILD:
		bar = GTK_WIDGET ( gtk_object_get_data 
				   (GTK_OBJECT (app), "drgeoToolbar"));
		gtk_container_add (GTK_CONTAINER (dockItem), bar);
		break;
	}
}
static gint 
remove_child_handler (GnomeMDI * mdi, GnomeMDIChild * child)
{
	GnomeDockItem *dockItem;
	GtkWidget *bar;
	GnomeApp *app;

	app = gnome_mdi_get_active_window (mdi);
	dockItem = GNOME_DOCK_ITEM (gtk_object_get_data 
				    (GTK_OBJECT (app), "childDockItem"));
	/* Is is the last child? */
	if (g_list_length (mdi->children) == 1)
	{
		/* Remove any toolbar specific to the active child */
		switch (((DrGeniusMDIChild *)child)->child_type) 
		{
		case FIGURE_CHILD:
			bar = GTK_WIDGET ( gtk_object_get_data 
				   (GTK_OBJECT (app), "drgeoToolbar"));
			gtk_container_remove (GTK_CONTAINER (dockItem), bar);
			break;
		}
	}
	return TRUE;
}
static void 
app_deleted_handler (GnomeApp * app, gpointer data)
{
	/* XXX invoked for each app removed. 
	   remove ref of the child toolbar 
	*/
}

/* Open XML file with multiple date */
void 
drgenius_mdi_session_open (GnomeMDI * mdi, char *filename)
{
	xmlDocPtr drgeniusXml;
	xmlNodePtr tree;

	if ((drgeniusXml = xmlParseFile (filename)) == NULL) 
	{
		char *s = g_strdup_printf ("drgenius_mdi_session_open: can't parse file %s\n", filename);
		gnome_error_dialog (s);
		free (s);
		return;
	}
	tree = drgeniusXml->root;
	if (strcasecmp (tree->name, "drgenius") != 0) 
	{
	        char *s = g_strdup_printf (_("%s is not a Dr Genius XML file\n"), filename);
		gnome_error_dialog (s);
		free (s);
		return;
	}
	tree = tree->childs;
	/* vistit the top child */
	while (tree != NULL) 
	{
		if (strcasecmp (tree->name, "drgeo") == 0) 
		{
			/* we get a Dr Geo figure */
			drgenius_mdi_figure_open (mdi, tree);
		}
		else if (strcasecmp (tree->name, "macro") == 0) 
		{
		    /* we get a Dr Geo macro-construction */
		    loadMacro (tree);
		}
		tree = tree->next;
	}
	xmlFreeDoc (drgeniusXml);
}


/* This function creates a new geometrical figure view in the MDI.
   if tree == NULL create a new empty figure
   else parse the figure from tree
*/
void 
drgenius_mdi_figure_open (GnomeMDI * mdi, xmlNodePtr tree)
{
	DrGeniusMDIChild *child;

	/*create a new child */
	if ((child = drgeo_mdi_child_new (tree)) != NULL) 
	{
		/* add the child to MDI */
		gnome_mdi_add_child (mdi, GNOME_MDI_CHILD (child));

		/* and add a new view of the child */
		gnome_mdi_add_view (mdi, GNOME_MDI_CHILD (child));

		gtk_signal_connect (GTK_OBJECT (child), "save",
				    GTK_SIGNAL_FUNC (drgeo_save_handler),
				    NULL);
	}
	else 
	{
		/* XXX error handling here.  */
	}
}

/* This function creates a new gel edit view in the MDI.  */

void 
drgenius_mdi_gel_open (GnomeMDI * mdi, char *filename)
{
	DrGeniusMDIChild *child;

	/* if we find a filename that is already open, just add view to that child */
	if (filename &&
	    (child = geledit_get_child_with_file (filename))) {
		gnome_mdi_add_view (mdi, GNOME_MDI_CHILD (child));
		return;
	}
	/* create a new child named 'name' */
	if ((child = geledit_mdi_child_new (filename)) != NULL) {
		/* add the child to MDI */
		gnome_mdi_add_child (mdi, GNOME_MDI_CHILD (child));

		/* and add a new view of the child */
		gnome_mdi_add_view (mdi, GNOME_MDI_CHILD (child));
	} else {
		/* XXX error handling here.  */
	}
}

/* This function creates a new calculator window in the MDI.  */

void 
drgenius_mdi_calculator_open (GnomeMDI * mdi)
{
	DrGeniusMDIChild *child;

	/*create a new child named 'name' */
	if ((child = console_mdi_child_new ()) != NULL) {
		/* add the child to MDI */
		gnome_mdi_add_child (mdi, GNOME_MDI_CHILD (child));

		/* and add a new view of the child */
		gnome_mdi_add_view (mdi, GNOME_MDI_CHILD (child));
	} else {
		/* XXX error handling here.  */
	}
}

/* close the current child of the mdi */
void 
drgenius_mdi_close_view (GnomeMDI * mdi)
{
	GnomeMDIChild *child;

	/* We remove the current view, and the child containing it.  */
	if (mdi->active_view) {
		child = gnome_mdi_get_child_from_view (mdi->active_view);
		if (gnome_mdi_remove_view (mdi, mdi->active_view, FALSE)) {
			/* Remove the child after the view it was holding.  The
			   view is only removed if the user agreed to it (see
			   remove_view_handler callback).  */
			gnome_mdi_remove_child (mdi, child, FALSE);
		}
	}
}
