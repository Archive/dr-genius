/* Dr.Genius
 * Copyright (C) 2000 the Free Software Foundation.
 *
 * Author: George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the  Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 */

#ifndef _GEL_H_
#define _GEL_H_

GelCalcCtx *	gel_calc_ctx_new		(void);
void		gel_calc_ctx_ref		(GelCalcCtx *cctx);
void		gel_calc_ctx_unref		(GelCalcCtx *cctx);

/* set defaults in a vararg kind of way */
/* FIXME: implement */
void		gel_calc_ctx_set_defaults	(GelCalcCtx *cctx,
						 ...);

GelExecCtx *	gel_exec_ctx_new		(GelCalcCtx *cctx);
void		gel_exec_ctx_ref		(GelExecCtx *ectx);
void		gel_exec_ctx_unref		(GelExecCtx *ectx);

/* setup the functions that will take care of errors and infos */
void		gel_exec_ctx_set_errorout	(GelExecCtx *ectx,
						 GelExecMessageFunc errorout,
						 gpointer data);
void		gel_exec_ctx_set_infoout	(GelExecCtx *ectx,
						 GelExecMessageFunc infoout,
						 gpointer data);

/* print errors and infos to our message functions */
void		gel_exec_ctx_print_error	(GelExecCtx *ectx,
						 char *string);
void		gel_exec_ctx_print_info		(GelExecCtx *ectx,
						 char *string);

#endif
