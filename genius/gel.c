/* Dr.Genius
 * Copyright (C) 2000 the Free Software Foundation.
 *
 * Author: George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the  Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 */

#include "config.h"
#include <glib.h>

#include "structs.h"

#include "geldict.h"
#include "geloutput.h"
#include "mpwrap.h"

#include "gel.h"

static void
our_mpw_error_func(MpwCtx *mctx, char *text)
{
	GelExecCtx *ectx;
	
	g_return_if_fail(!mctx);
	
	ectx = mpw_ctx_get_data(mctx);

	g_return_if_fail(!ectx);
	
	gel_exec_ctx_print_error(ectx, text);
}

GelExecCtx *
gel_exec_ctx_new(GelCalcCtx *cctx)
{
	GelExecCtx *ectx;

	g_return_val_if_fail(cctx!=NULL,NULL);

	ectx = g_new(GelExecCtx,1);

	ectx->refcount = 1;
	ectx->parent = cctx;
	ectx->dict = g_hash_table_new(NULL,NULL);
	/* fixme, init from defaults */
	ectx->mctx = mpw_ctx_new(our_mpw_error_func, 256, FALSE, ectx);

	if(!cctx->default_output)
		ectx->output = gel_output_new();
	else {
		ectx->output = cctx->default_output;
		gel_output_ref(ectx->output);
	}

	return ectx;
}

static void
free_value(gpointer key, gpointer value, gpointer data)
{
	g_free(value);
}

static void
gel_exec_ctx_destroy(GelExecCtx *ectx)
{
	GSList *li;

	g_return_if_fail(ectx!=NULL);

	ectx->parent->execs = g_slist_remove(ectx->parent->execs,ectx);

	g_hash_table_foreach(ectx->dict,free_value,NULL);
	g_hash_table_destroy(ectx->dict);
	
	mpw_ctx_unref(ectx->mctx);

	/* just for sanity sake if we would by mistake access it */
	ectx->dict = NULL;
	ectx->refcount = 0;

	for(li=ectx->stack; li; li=li->next) {
		GSList *l;
		for(l=li->data; l; l=l->next) {
			GelEFunc *func = l->data;
			/* all our token stuff was killed above */
			func->id = NULL;
			gel_dict_free_func(ectx,func);
		}
		g_slist_free(l->data);
	}
	g_slist_free(ectx->stack);
	ectx->stack = NULL;
	ectx->curcontext = -1;

	g_free(ectx);
}

void
gel_exec_ctx_ref(GelExecCtx *ectx)
{
	g_return_if_fail(ectx!=NULL);

	ectx->refcount++;
}

void
gel_exec_ctx_unref(GelExecCtx *ectx)
{
	g_return_if_fail(ectx!=NULL);

	ectx->refcount--;

	if(ectx->refcount <= 0)
		gel_exec_ctx_destroy(ectx);
}

void
gel_exec_ctx_set_errorout(GelExecCtx *ectx,
			  GelExecMessageFunc errorout,
			  gpointer data)
{
	g_return_if_fail(ectx!=NULL);

	ectx->errorout = errorout;
	ectx->errorout_data = data;
}

void
gel_exec_ctx_set_infoout(GelExecCtx *ectx,
			 GelExecMessageFunc infoout,
			 gpointer data)
{
	g_return_if_fail(ectx!=NULL);

	ectx->infoout = infoout;
	ectx->infoout_data = data;
}

void
gel_exec_ctx_print_error(GelExecCtx *ectx, char *text)
{
	g_return_if_fail(ectx!=NULL);
	g_return_if_fail(text!=NULL);

	if(ectx->errorout)
		ectx->errorout(ectx, text, ectx->errorout_data);
	else
		gel_output_full_string(ectx->output, text);

}

void
gel_exec_ctx_print_info(GelExecCtx *ectx, char *text)
{
	g_return_if_fail(ectx!=NULL);
	g_return_if_fail(text!=NULL);

	if(ectx->infoout)
		ectx->infoout(ectx, text, ectx->infoout_data);
	else
		gel_output_full_string(ectx->output, text);

}

GelCalcCtx *
gel_calc_ctx_new(void)
{
	GelCalcCtx *cctx = g_new0(GelCalcCtx,1);
	GelToken *tok;
	cctx->refcount = 1;
	cctx->dict = g_hash_table_new(g_str_hash,g_str_equal);

	tok = g_new0(GelToken,1);
	tok->token = g_strdup("Ans");
	g_hash_table_insert(cctx->dict,tok->token,tok);
	g_hash_table_insert(cctx->dict,g_strdup("ans"),tok);

	/*
	gel_funclib_addall(cctx);
	*/

	gel_dict_protect_all(cctx);


	return cctx;
}

void
gel_calc_ctx_ref(GelCalcCtx *cctx)
{
	g_return_if_fail(cctx!=NULL);

	cctx->refcount++;
}

static void
gel_calc_ctx_destroy(GelCalcCtx *cctx)
{
	GSList *li;

	g_return_if_fail(cctx!=NULL);
	
	for(li=cctx->execs; li; li=li->next) {
		gel_exec_ctx_destroy(li->data);
	}
	/* just for sanity sake if we would by mistake access it */
	cctx->execs = NULL;
	cctx->refcount = 0;

	/* FIXME: clear the dictionary */
	cctx->dict = NULL;
	cctx->globals = NULL;

	g_free(cctx);
}

void
gel_calc_ctx_unref(GelCalcCtx *cctx)
{
	g_return_if_fail(cctx!=NULL);

	cctx->refcount--;

	if(cctx->refcount <= 0)
		gel_calc_ctx_destroy(cctx);
}
