/* GnomENIUS Calculator
 * Copyright (C) 1999 the Free Software Foundation.
 *
 * Author: George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the  Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 */

#ifndef _UTYPE_H_
#define _UTYPE_H_

/*declarations of structures*/
#include "structs.h"

/* User type stuff */
typedef void (*GelUTDestroyFunc)(gpointer data);
typedef gpointer (*GelUTCopyFunc)(gpointer data);
/* make a new unique identifier for a user type and set the destruction
   function which is called when a variable of that type is killed,
   and a function to copy the data,
   returns -1 on error */
int gel_new_user_type(char *name,
		      GelUTDestroyFunc destructor,
		      GelUTCopyFunc copier);
/* destroy the type itself, not a specific variable */
void gel_destroy_user_type(int type);
/* lookup the id of a type */
int gel_get_user_type(char *name);
/* lookup the name of type 'type' (don't free!)*/
char * gel_get_user_type_name(int type);
/* make a new variable of type 'type' with data 'data' */
GelETree *gel_make_new_user_variable(int type, gpointer data);
/* free the 'data' of type 'type' using the destructor */
void gel_free_user_variable_data(int type, gpointer data);
/* copy the 'data' of type 'type' using the copier */
gpointer gel_copy_user_variable_data(int type, gpointer data);

#endif
