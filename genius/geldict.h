/* Dr.Genius
 * Copyright (C) 2000 the Free Software Foundation.
 *
 * Author: George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the  Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 */

#ifndef _GELDICT_H_
#define _GELDICT_H_

#include <string.h> /* for memcpy */

typedef GelETree *(*GelDictFunc)(GelCtx *ctx, GelETree * * /*arguments*/,int * /*exception*/);

/* intern a token 'id' optionally updating execcontext stuff to 'update' */
GelToken * gel_dict_intern(GelCalcCtx *cctx,
			   GelExecCtx *update,
			   char *id);

/* create a new builtin function, if the GelExecCtx is in the global
   context the two are equal in function */
GelEFunc * gel_dict_new_global_bifunc(GelCalcCtx *cctx, GelToken *id,
				      GelDictFunc f, int nargs);
GelEFunc * gel_dict_new_bifunc(GelExecCtx *cctx, GelToken *id,
			       GelDictFunc f, int nargs);

/* create a new user function, if the GelExecCtx is in the global
   context the two are equal in function */
GelEFunc * gel_dict_new_global_ufunc(GelCalcCtx *cctx, GelToken *id,
				     GelETree *value,
				     GSList *argnames, int nargs);
GelEFunc * gel_dict_new_ufunc(GelExecCtx *cctx, GelToken *id, 
			      GelETree *value,
			      GSList *argnames, int nargs);

/* create a new variable, if the GelExecCtx is in the global
   context the two are equal in function */
GelEFunc * gel_dict_new_global_vfunc(GelCalcCtx *cctx, GelToken *id,
				     GelETree *value);
GelEFunc * gel_dict_new_vfunc(GelExecCtx *cctx, GelToken *id,
			      GelETree *value);

/* create a new reference function, if the GelExecCtx is in the global
   context the two are equal in function */
GelEFunc * gel_dict_new_global_reffunc(GelCalcCtx *cctx, GelToken *id,
				       GelEFunc *ref);
GelEFunc * gel_dict_new_reffunc(GelExecCtx *cctx, GelToken *id,
				GelEFunc *ref);

/* make a deep copy of a function */
GelEFunc * gel_dict_copy_func(GelEFunc *o);
/* make a real function out of a fake one */
GelEFunc * gel_dict_make_real_func(GelExecCtx *ectx, GelEFunc *o, 
				   GelToken *id, int use);

/*make real func and replace n with it, without changing n's context or id*/
/*if use is set, we USE the original function, NULLing fake approriately*/
void gel_dict_set_real_func(GelEFunc *n, GelEFunc *fake, int use);

/*set value of an existing function (in local context), used for arguments
  WARNING, does not free the memory allocated by previous value!*/
/* XXX: used to be d_setvalue */
gboolean gel_dict_set_local_value(GelExecCtx *ectx, GelToken *id, GelETree *value);

/*FIXME: these are unimplemented*/
/* delete a function from current context */
gboolean gel_dict_delete(GelExecCtx *ectx, GelToken *id);
/* delete a function from the global context */
gboolean gel_dict_global_delete(GelCalcCtx *cctx, GelToken *id);

/* get a list of function in the current context */
GSList *gel_dict_get_context(GelExecCtx *ectx);

/* protect every function in the global calculator context, all except
   for any function with the id of "Ans" */
void gel_dict_protect_all(GelCalcCtx *cctx);

/* kill all the contexts and go to global context */
void gel_dict_go_global(GelExecCtx *ectx);

/* free a func, the ectx can be null if we are sure it's in the
   global context */ 
void gel_dict_free_func(GelExecCtx *ectx, GelEFunc *n);

/* push a new dictionary onto the context stack */
gboolean gel_dict_push_context(GelExecCtx *ectx);
/* kill the current context and jump to a higher context */
gboolean gel_dict_pop_context(GelExecCtx *ectx);

/* set n to point to 'ref' (and make it reference_func)*/
void gel_dict_set_ref(GelEFunc *n, GelEFunc *ref);
/* value of n to 'value' (and make it a variable)
   does not copy value */
void gel_dict_set_value(GelEFunc *n, GelETree *value);


/*lookup a function in the dictionary in the current context*/
GelEFunc * gel_dict_lookup_local(GelExecCtx *ectx, GelToken *id);
/* a global lookup but NOT in the current context */
GelEFunc * gel_dict_lookup_global_up1(GelExecCtx *ectx, GelToken *id);
/*lookup a function in the dictionary, if there are more return the one in the
  highest context*/
#define gel_dict_lookup_global(ectx,id) \
	(GEL_DICT_UPDATE_TOKEN((ectx),(id))->curref)


/* update token will update the token to the current GelExecCtx, it
   is assumed that the token belongs to the GelCalcCtx that is the
   parent of GelExecCtx */
G_INLINE_FUNC GelToken *gel_dict_update_token(GelExecCtx *ectx,
					      GelToken *token);
#ifdef G_CAN_INLINE
G_INLINE_FUNC GelToken *
gel_dict_update_token(GelExecCtx *ectx,
		      GelToken *token) {
	GelExecToken *etok = g_hash_table_lookup(ectx->dict,token);
	if(!etok) {
		etok = g_new0(GelExecToken,1);
		etok->token = token->token;
		g_hash_table_insert(ectx->dict,token,etok);
	}
	memcpy(token,etok,sizeof(etok));
	token->this_ectx = ectx;
	return token;
}
#endif

#define GEL_DICT_UPDATE_TOKEN(ectx,token)	\
	((token)->this_ectx==(ectx)?		\
	 (token):				\
	 gel_dict_update_token(ectx,token))

#define GEL_DICT_NEW_FUNC(n) {						\
	if(!gel_dict_free_funcs)					\
		(n) = g_new(GelEFunc,1);				\
	else {								\
		(n) = gel_dict_free_funcs;				\
		gel_dict_free_funcs = gel_dict_free_funcs->data.next;	\
	}								\
}
#define GEL_DICT_FREE_FUNC(n) {						\
	(n)->data.next = gel_dict_free_funcs;				\
	gel_dict_free_funcs = (n);					\
}

extern GelEFunc *gel_dict_free_funcs;

#endif
