/* Dr.Genius
 * Copyright (C) 2000 the Free Software Foundation.
 *
 * Author: George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the  Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 */

#include "config.h"
#include <glib.h>
#include <stdio.h>

#include "structs.h"
#include "gel.h"

#include "geloutput.h"


static int
get_cur_len(GelOutput *gelo)
{
	if(gelo->line_length_get)
		return gelo->line_length_get(gelo);
	else
		return gelo->line_length;
}

static void
gel_output_putchar(GelOutput *gelo, char ch, gboolean limit, int ll)
{
	if(ch=='\n') {
		gelo->inside_escape = FALSE;
		gelo->cur_line_pos = 0;
	} else if(ch=='\e') {
		gelo->inside_escape = TRUE;
	} else if(gelo->inside_escape) {
		if(ch == 'm')
			gelo->inside_escape = FALSE;
	} else {
		gelo->cur_line_pos++;
		if(limit) {
			if(gelo->cur_line_pos > ll-3)
				return;
			if(gelo->cur_line_pos == ll-3) {
				gel_output_putchar(gelo, '.', FALSE, 0);
				gel_output_putchar(gelo, '.', FALSE, 0);
				gel_output_putchar(gelo, '.', FALSE, 0);
				return;
			}
		}
	}
	if(gelo->output_type == GEL_OUTPUT_FILE) {
		g_assert(gelo->outfp);
		fputc(ch, gelo->outfp);
	} else if(gelo->output_type == GEL_OUTPUT_FILE) {
		if(gelo->outs)
			g_string_append_c(gelo->outs, ch);
		else {
			gelo->outs = g_string_new(NULL);
			g_string_append_c(gelo->outs, ch);
		}
	}
}

static void
gel_output_print_string(GelOutput *gelo, char *string, gboolean limit)
{
	int ll = get_cur_len(gelo);
	char *p;

	if(gelo->output_type == GEL_OUTPUT_BLACK_HOLE) {
		if(gelo->notify)
			gelo->notify(gelo);
		return;
	}
	
	if(ll <= 0)
		limit = FALSE;

	for(p=string; *p; p++) {
		if(*p=='\n') {
			gel_output_putchar(gelo, '\n', limit, ll);
		} else if(limit && gelo->cur_line_pos > ll &&
			  !gelo->inside_escape) {
			continue;
		} else if(*p=='\t') {
			int n;
			int left = (8-(gelo->cur_line_pos%8));
			for(n=0;n<left;n++)
				gel_output_putchar(gelo, ' ', limit, ll);
		} else {
			gel_output_putchar(gelo, *p, limit, ll);
		}
	}

	if(gelo->notify)
		gelo->notify(gelo);
}

void
gel_output_printf_full(GelOutput *gelo, gboolean limit, char *format, ...)
{
	gchar *buffer;
	va_list args;

	va_start (args, format);
	buffer = g_strdup_vprintf (format, args);
	va_end (args);

	gel_output_print_string(gelo, buffer, limit);

	g_free(buffer);
}

void
gel_output_printf(GelOutput *gelo, char *format, ...)
{
	gchar *buffer;
	va_list args;

	va_start (args, format);
	buffer = g_strdup_vprintf (format, args);
	va_end (args);

	gel_output_print_string(gelo, buffer, FALSE);

	g_free(buffer);
}

void
gel_output_string (GelOutput *gelo, char *string)
{
	g_return_if_fail(gelo!=NULL);
	g_return_if_fail(string!=NULL);
	
	gel_output_print_string(gelo, string, TRUE);
}

void
gel_output_full_string (GelOutput *gelo, char *string)
{
	g_return_if_fail(gelo!=NULL);
	g_return_if_fail(string!=NULL);

	gel_output_print_string(gelo, string, FALSE);
}


GelOutput *
gel_output_new (void)
{
	GelOutput *gelo;

	gelo = g_new0(GelOutput, 1);
	gelo->ref_count = 1;
	gelo->output_type = GEL_OUTPUT_FILE;
	gelo->outfp = stdout;

	return gelo;
}

static void
gel_output_destroy (GelOutput *gelo)
{
	g_return_if_fail(gelo!=NULL);

	if(gelo->outs)
		g_string_free(gelo->outs, TRUE);

	g_free(gelo);
}

void
gel_output_ref (GelOutput *gelo)
{
	g_return_if_fail(gelo!=NULL);

	gelo->ref_count++;
}

void
gel_output_unref (GelOutput *gelo)
{
	g_return_if_fail(gelo!=NULL);

	gelo->ref_count--;

	if(gelo->ref_count <= 0)
		gel_output_destroy(gelo);
}

void
gel_output_setup_string (GelOutput *gelo, int line_length,
			 GelOutputLineFunc line_length_get)
{
	g_return_if_fail(gelo!=NULL);

	gelo->output_type = GEL_OUTPUT_STRING;
	gelo->line_length = line_length;
	gelo->line_length_get = line_length_get;
	gelo->outfp = NULL;
	gelo->cur_line_pos = 0;
	gelo->inside_escape = FALSE;

	if(gelo->outs) {
		g_string_free(gelo->outs, TRUE);
		gelo->outs = NULL;
	}
}

char *
gel_output_snarf_string (GelOutput *gelo)
{
	char *r;

	g_return_val_if_fail(gelo!=NULL, NULL);
	g_return_val_if_fail(gelo->output_type!=GEL_OUTPUT_STRING, NULL);

	if(!gelo->outs || !gelo->outs->str || !*(gelo->outs->str))
		return NULL;

	r = gelo->outs->str;
	g_string_free(gelo->outs, FALSE);
	gelo->outs = NULL;

	return r;
}

void
gel_output_set_gstring(GelOutput *gelo, GString *gs)
{
	if(gelo->outs)
		g_string_free(gelo->outs, TRUE);
	gelo->outs = gs;
}

/* file output stuff */
void
gel_output_setup_file (GelOutput *gelo, FILE *outfp, int line_length,
		       GelOutputLineFunc line_length_get)
{
	g_return_if_fail(gelo!=NULL);
	g_return_if_fail(outfp!=NULL);

	gelo->output_type = GEL_OUTPUT_FILE;
	gelo->outfp = outfp;
	gelo->line_length = line_length;
	gelo->line_length_get = line_length_get;
	gelo->cur_line_pos = 0;
	gelo->inside_escape = FALSE;

	if(gelo->outs) {
		g_string_free(gelo->outs, TRUE);
		gelo->outs = NULL;
	}
}

void
gel_output_setup_black_hole (GelOutput *gelo)
{
	g_return_if_fail(gelo!=NULL);

	gelo->output_type = GEL_OUTPUT_BLACK_HOLE;
	gelo->outfp = NULL;
	gelo->line_length = 0;
	gelo->line_length_get = NULL;
	gelo->cur_line_pos = 0;
	gelo->inside_escape = FALSE;

	if(gelo->outs) {
		g_string_free(gelo->outs, TRUE);
		gelo->outs = NULL;
	}
}

/* some accessors */

void
gel_output_get_line_length (GelOutput *gelo,
			    int *line_length,
			    GelOutputLineFunc *line_length_get)
{
	g_return_if_fail(gelo!=NULL);
	
	if(line_length) *line_length = gelo->line_length;
	if(line_length_get) *line_length_get = gelo->line_length_get;
}

void
gel_output_set_line_length (GelOutput *gelo, 
			    int line_length,
			    GelOutputLineFunc line_length_get)
{
	g_return_if_fail(gelo!=NULL);
	g_return_if_fail(line_length>=0);

	gelo->line_length = line_length;
	gelo->line_length_get = line_length_get;
}

void
gel_output_set_notify(GelOutput *gelo,
		      GelOutputNotifyFunc notify)
{
	g_return_if_fail(gelo!=NULL);

	gelo->notify = notify;
}

void
gel_output_set_data(GelOutput *gelo, gpointer data)
{
	g_return_if_fail(gelo!=NULL);

	gelo->data = data;
}

gpointer
gel_output_get_data(GelOutput *gelo)
{
	g_return_val_if_fail(gelo!=NULL, NULL);

	return gelo->data;
}

void
gel_output_flush(GelOutput *gelo)
{
	g_return_if_fail(gelo!=NULL);

	if(gelo->output_type == GEL_OUTPUT_FILE) {
		g_assert(gelo->outfp);
		fflush(gelo->outfp);
	}
}
