/* Dr.Genius
 * Copyright (C) 2000 the Free Software Foundation.
 *
 * Author: George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the  Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 */

#ifndef _GELOUTPUT_H_
#define _GELOUTPUT_H_

#include <stdio.h>
#include "structs.h"

void		gel_output_printf_full		(GelOutput *gelo,
						 gboolean limit,
						 char *format,
						 ...) G_GNUC_PRINTF (3, 4);
void		gel_output_printf		(GelOutput *gelo,
						 char *format,
						 ...) G_GNUC_PRINTF (2, 3);
void		gel_output_string		(GelOutput *gelo,
						 char *string);
void		gel_output_full_string		(GelOutput *gelo,
						 char *string);

GelOutput *	gel_output_new			(void);
void		gel_output_ref			(GelOutput *gelo);
void		gel_output_unref		(GelOutput *gelo);

/* set input notify handler */
void		gel_output_set_notify		(GelOutput *gelo,
						 GelOutputNotifyFunc notify);

/* user data */
/* usable outside of genius */
void		gel_output_set_data		(GelOutput *gelo,
						 gpointer data);
gpointer	gel_output_get_data		(GelOutput *gelo);

/* string output stuff */
void		gel_output_setup_string		(GelOutput *gelo, 
						 int line_length,
						 GelOutputLineFunc line_length_get);
char *		gel_output_snarf_string		(GelOutput *gelo);

/* set the internal gstring to the given one, usefull if you want
   to print into your own gstring.  Just make sure to set it to
   null before the object is destroyed */
void		gel_output_set_gstring		(GelOutput *gelo,
						 GString *gs);

/* file output stuff */
void		gel_output_setup_file		(GelOutput *gelo, 
						 FILE *outfp,
						 int line_length,
						 GelOutputLineFunc line_length_get);

/* Black hole setup */
void		gel_output_setup_black_hole	(GelOutput *gelo);

/* some accessors */
void		gel_output_get_line_length	(GelOutput *gelo,
						 int *line_length,
						 GelOutputLineFunc *line_length_get);
void		gel_output_set_line_length	(GelOutput *gelo, 
						 int line_length,
						 GelOutputLineFunc line_length_get);

/* quivalent to fflush on the FILE pointer */
void		gel_output_flush		(GelOutput *gelo);

#endif
