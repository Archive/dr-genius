/* Dr.Genius
 * Copyright (C) 2000 the Free Software Foundation.
 *
 * Author: George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the  Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 */

#include "config.h"
#include <glib.h>

#include "structs.h"

#include "eval.h"
#include "compil.h"

#include "gel.h"

/*implement the inline functions*/
#undef G_INLINE_FUNC
#undef G_CAN_INLINE
#define	G_INLINE_FUNC extern
#define	G_CAN_INLINE 1
#include "geldict.h"

static void gel_dict_add_func(GelExecCtx *ectx, GelEFunc *func);
static void gel_dict_add_global_func(GelCalcCtx *cctx, GelEFunc *func);

/*replace old with stuff from new and free new,
  new has to have the same id, also new->id should
  not hold a reference to new*/
static void gel_dict_replace_func(GelEFunc *old, GelEFunc *new);

GelEFunc *gel_dict_free_funcs = NULL;
extern GHashTable *uncompiled;

GelToken *
gel_dict_intern(GelCalcCtx *cctx, GelExecCtx *update, char *id)
{
	GelToken *token;
	token = g_hash_table_lookup(cctx->dict,id);
	if(!token) {
		token = g_new0(GelToken,1);
		token->token = g_strdup(id);
		/* everything else is initialized to 0 */
	}
	if(update)
		return GEL_DICT_UPDATE_TOKEN(update,token);
	else
		return token;
}

GelEFunc *
gel_dict_new_global_bifunc(GelCalcCtx *cctx, GelToken *id,
			   GelDictFunc f, int nargs)
{
	GelEFunc *n;
	GEL_DICT_NEW_FUNC(n);

	n->id=id;
	n->data.func=f;
	n->nargs=nargs;
	n->named_args = NULL;
	n->context=0;
	n->type=GEL_BUILTIN_FUNC;

	gel_dict_add_global_func(cctx,n);

	return n;
}

GelEFunc *
gel_dict_new_bifunc(GelExecCtx *ectx, GelToken *id,
		    GelDictFunc f, int nargs)
{
	GelEFunc *n;
	GEL_DICT_NEW_FUNC(n);

	n->id=id;
	n->data.func=f;
	n->nargs=nargs;
	n->named_args = NULL;
	n->context=ectx->curcontext;
	n->type=GEL_BUILTIN_FUNC;

	if(n->context>0)
		gel_dict_add_func(ectx,n);
	else
		gel_dict_add_global_func(ectx->parent,n);

	return n;
}

GelEFunc *
gel_dict_new_global_ufunc(GelCalcCtx *cctx, GelToken *id, GelETree *value,
			  GSList *argnames, int nargs)
{
	GelEFunc *n;
	GEL_DICT_NEW_FUNC(n);

	n->id=id;
	n->data.user=value;
	n->nargs=nargs;
	n->named_args=argnames;
	n->context=0;
	n->type=GEL_USER_FUNC;

	gel_dict_add_global_func(cctx,n);

	return n;
}

GelEFunc *
gel_dict_new_ufunc(GelExecCtx *ectx, GelToken *id, GelETree *value,
		   GSList *argnames, int nargs)
{
	GelEFunc *n;
	GEL_DICT_NEW_FUNC(n);

	n->id=id;
	n->data.user=value;
	n->nargs=nargs;
	n->named_args=argnames;
	n->context=ectx->curcontext;
	n->type=GEL_USER_FUNC;

	if(n->context>0)
		gel_dict_add_func(ectx,n);
	else
		gel_dict_add_global_func(ectx->parent,n);

	return n;
}

GelEFunc *
gel_dict_new_global_vfunc(GelCalcCtx *cctx, GelToken *id, GelETree *value)
{
	GelEFunc *n;
	GEL_DICT_NEW_FUNC(n);

	n->id=id;
	n->data.user=value;
	n->nargs=0;
	n->named_args=NULL;
	n->context=0;
	n->type=GEL_VARIABLE_FUNC;

	gel_dict_add_global_func(cctx,n);

	return n;
}

GelEFunc *
gel_dict_new_vfunc(GelExecCtx *ectx, GelToken *id, GelETree *value)
{
	GelEFunc *n;
	GEL_DICT_NEW_FUNC(n);

	n->id=id;
	n->data.user=value;
	n->nargs=0;
	n->named_args=NULL;
	n->context=ectx->curcontext;
	n->type=GEL_VARIABLE_FUNC;

	if(n->context>0)
		gel_dict_add_func(ectx,n);
	else
		gel_dict_add_global_func(ectx->parent,n);

	return n;
}

GelEFunc *
gel_dict_new_global_reffunc(GelCalcCtx *cctx, GelToken *id, GelEFunc *ref)
{
	GelEFunc *n;
	GEL_DICT_NEW_FUNC(n);

	n->id=id;
	n->data.ref=ref;
	n->nargs=0;
	n->named_args=NULL;
	n->context=0;
	n->type=GEL_REFERENCE_FUNC;

	gel_dict_add_global_func(cctx,n);

	return n;
}

GelEFunc *
gel_dict_new_reffunc(GelExecCtx *ectx, GelToken *id, GelEFunc *ref)
{
	GelEFunc *n;
	GEL_DICT_NEW_FUNC(n);

	n->id=id;
	n->data.ref=ref;
	n->nargs=0;
	n->named_args=NULL;
	n->context=ectx->curcontext;
	n->type=GEL_REFERENCE_FUNC;

	if(n->context>0)
		gel_dict_add_func(ectx,n);
	else
		gel_dict_add_global_func(ectx->parent,n);

	return n;
}

/*replace old with stuff from new and free new,
  new has to have the same id, also new->id should
  not hold a reference to new*/
static void
gel_dict_replace_func(GelEFunc *old, GelEFunc *new)
{
	g_return_if_fail(old && new);
	g_return_if_fail(old->id == new->id);

	if(old->type == GEL_USER_FUNC ||
	   old->type == GEL_VARIABLE_FUNC)
		gel_freetree(old->data.user);
	g_slist_free(old->named_args);
	memcpy(old,new,sizeof(GelEFunc));
	/*prepend to free list*/
	GEL_DICT_FREE_FUNC(new);
}

/*lookup a function in the dictionary in the current context*/
GelEFunc *
gel_dict_lookup_local(GelExecCtx *ectx, GelToken *id)
{
	GelEFunc *func;
	
	if(!id)
		return NULL;

	func = GEL_DICT_UPDATE_TOKEN(ectx,id)->curref;

	/*check if exists and if so if it's in the local context */
	if(!func || func->context < ectx->curcontext)
		return NULL;
	else
		return func;
}

/*lookup a function in the dictionary NOT in the current context*/
GelEFunc *
gel_dict_lookup_global_up1(GelExecCtx *ectx, GelToken *id)
{
	GelEFunc *func;

	if(!id)
		return NULL;

	GEL_DICT_UPDATE_TOKEN(ectx,id);
	if(id->refs) {
		/*the first one must be the lowest context*/
		func = id->refs->data;
		if(func->context < ectx->curcontext)
			return func;
		else if(id->refs->next)
			return id->refs->next->data;
		else
			/* since id->refs was not NULL we were not
			   in a global context and if there is no
			   global by this id, it will be NULL */
			return id->globref;
	} else if(id->globref && id->globref->context < ectx->curcontext)
		return id->globref;
	return NULL;
}

static void
gel_dict_add_func(GelExecCtx *ectx, GelEFunc *func)
{
	GelEFunc *f;
	
	g_return_if_fail(func->context == ectx->curcontext);
	
	/*we already found it (in current context)*/
	f = gel_dict_lookup_local(ectx,func->id);
	if(f) {
		/* replace the f structure contents with the contents of
		   func and free func */
		gel_dict_replace_func(f, func);
		return;
	}

	if(func->context > 0) {
		ectx->stack->data = g_slist_prepend(ectx->stack->data, func);
		func->id->refs = g_slist_prepend(func->id->refs,func);
	} else {
		ectx->parent->globals = 
			g_slist_prepend(ectx->parent->globals,func);
		func->id->globref = func;
	}
	
	func->id->curref = func;
}

static void
gel_dict_add_global_func(GelCalcCtx *cctx, GelEFunc *func)
{
	g_return_if_fail(func->context == 0);
	
	/*we already found it (in current context)*/
	if(func->id->globref) {
		/* replace the f structure contents with the contents of
		   func and free func */
		gel_dict_replace_func(func->id->globref, func);
		return;
	}

	cctx->globals = g_slist_prepend(cctx->globals,func);
	func->id->globref = func;

	func->id->curref = func;
}

/*copy a function*/
GelEFunc *
gel_dict_copy_func(GelEFunc *o)
{
	GelEFunc *n;

	GEL_DICT_NEW_FUNC(n);
	memcpy(n,o,sizeof(GelEFunc));
	if(n->type == GEL_USER_FUNC ||
	   n->type == GEL_VARIABLE_FUNC) {
		if(!o->data.user) {
			g_assert(uncompiled);
			o->data.user =
				gel_decompile_tree(g_hash_table_lookup(uncompiled,o->id));
			g_hash_table_remove(uncompiled,o->id);
			g_assert(o->data.user);
		}
		n->data.user=copynode(o->data.user);
	}
	n->named_args = g_slist_copy(o->named_args);
	
	return n;
}

/*make a real function from a fake*/
GelEFunc *
gel_dict_make_real_func(GelExecCtx *ectx, GelEFunc *o, GelToken *id, int use)
{
	GelEFunc *n;

	GEL_DICT_NEW_FUNC(n);
	memcpy(n,o,sizeof(GelEFunc));
	n->id = id;
	n->context = ectx->curcontext;

	if(n->type == GEL_USER_FUNC ||
	   n->type == GEL_VARIABLE_FUNC) {
		if(!o->data.user) {
			g_assert(uncompiled);
			o->data.user =
				gel_decompile_tree(g_hash_table_lookup(uncompiled,o->id));
			g_hash_table_remove(uncompiled,o->id);
			g_assert(o->data.user);
		}
		if(use) {
			n->data.user = o->data.user;
			o->data.user = NULL;
		} else
			n->data.user = copynode(o->data.user);
	}
	if(use) {
		o->named_args = NULL;
		o->named_args = 0;
	} else
		n->named_args = g_slist_copy(o->named_args);
	
	return n;
}

/*make real func and replace n with it, without changing n's context or id*/
/*if use is set, we USE the original function, NULLing fake approriately*/
void
gel_dict_set_real_func(GelEFunc *n, GelEFunc *fake, int use)
{
	if(n->type == GEL_USER_FUNC ||
	   n->type == GEL_VARIABLE_FUNC)
		gel_freetree(n->data.user);
	
	n->type = fake->type;
	n->data = fake->data;
	if(n->type == GEL_USER_FUNC ||
	   n->type == GEL_VARIABLE_FUNC) {
		if(!fake->data.user) {
			g_assert(uncompiled);
			fake->data.user =
				gel_decompile_tree(g_hash_table_lookup(uncompiled,fake->id));
			g_hash_table_remove(uncompiled,fake->id);
			g_assert(fake->data.user);
		}
		if(use) {
			n->data.user = fake->data.user;
			fake->data.user = NULL;
		} else
			n->data.user = copynode(fake->data.user);
	}

	if(use) {
		n->named_args = fake->named_args;
		n->nargs = fake->nargs;
		fake->named_args = NULL;
		fake->nargs = 0;
	} else {
		n->named_args = g_slist_copy(fake->named_args);
		n->nargs = fake->nargs;
	}
}

/*FIXME: these are unimplemented*/
/* delete a function from current context */
gboolean
gel_dict_delete(GelExecCtx *ectx, GelToken *id)
{
	/*FIXME: we need to figure out a way to get references
	  not to crap out about this */
	return FALSE;
}

/* delete a function from the global context */
gboolean
gel_dict_global_delete(GelCalcCtx *cctx, GelToken *id)
{
	/*FIXME: we need to figure out a way to get references
	  not to crap out about this */
	return FALSE;
}

/*set value of an existing function (in local context), used for arguments
  WARNING, does not free the memory allocated by previous value!*/
/* XXX: used to be d_setvalue */
gboolean
gel_dict_set_local_value(GelExecCtx *ectx, GelToken *id, GelETree *value)
{
	GelEFunc *f;
	f=gel_dict_lookup_local(ectx, id);
	if(!f || (f->type!=GEL_USER_FUNC &&
		  f->type!=GEL_VARIABLE_FUNC))
		return FALSE;
	f->data.user=value;
	return TRUE;
}

/* get a list of function in the current context */
GSList *
gel_dict_get_context(GelExecCtx *ectx)
{
	if(ectx->curcontext == 0)
		return ectx->parent->globals;
	else {
		g_assert(ectx->stack);
		return ectx->stack->data;
	}
}

/* protect every function in the global calculator context, all except
   for any function with the id of "Ans" */
void
gel_dict_protect_all(GelCalcCtx *cctx)
{
	GSList *li;
	for(li=cctx->globals;li;li=g_slist_next(li)) {
		GelEFunc *func = li->data;
		if(!func->id || strcmp(func->id->token,"Ans")==0)
			continue;
		func->id->protected = 1;
	}
}

/* kill all the contexts and go to global context */
void
gel_dict_go_global(GelExecCtx *ectx)
{
	while(ectx->curcontext>0)
		gel_dict_pop_context(ectx);
}

/* free a func, the ectx can be null if we are sure it's in the
   global context */ 
void
gel_dict_free_func(GelExecCtx *ectx, GelEFunc *n)
{
	if(!n)
		return;
	if(n->id) {
		if(n->context>0) {
			GEL_DICT_UPDATE_TOKEN(ectx,n->id);
			n->id->refs = g_slist_remove(n->id->refs,n);
			n->id->curref = n->id->refs?n->id->refs->data:
				n->id->globref;
		} else {
			g_assert(n == n->id->globref);
			n->id->globref = NULL;
		}
	}
	if((n->type == GEL_USER_FUNC ||
	    n->type == GEL_VARIABLE_FUNC) &&
	   n->data.user)
		gel_freetree(n->data.user);
	g_slist_free(n->named_args);
	/*prepend to free list*/
	GEL_DICT_FREE_FUNC(n);
}

/*push a new dictionary onto the context stack*/
gboolean
gel_dict_push_context(GelExecCtx *ectx)
{
	ectx->stack = g_slist_prepend(ectx->stack,NULL);
	ectx->curcontext++;
	return TRUE;
}

/* kill the current context and jump to a higher context */
gboolean
gel_dict_pop_context(GelExecCtx *ectx)
{
	if(ectx->curcontext>0) {
		GSList *li;
		GSList *dict = ectx->stack->data;

		for(li=dict;li!=NULL;li=g_slist_next(li))
			gel_dict_free_func(ectx, li->data);
		g_slist_free(dict);
		li = ectx->stack;
		ectx->stack = g_slist_remove_link(ectx->stack,li);
		g_slist_free_1(li);

		ectx->curcontext--;
		return TRUE;
	} else
		return FALSE;
}

/* set n to point to 'ref' (and make it reference_func)*/
void
gel_dict_set_ref(GelEFunc *n, GelEFunc *ref)
{
	if(!n || !ref)
		return;
	if(n->type == GEL_USER_FUNC ||
	   n->type == GEL_VARIABLE_FUNC)
		gel_freetree(n->data.user);
	n->type = GEL_REFERENCE_FUNC;
	g_slist_free(n->named_args);
	n->nargs = 0;
	n->named_args = NULL;
	
	n->data.ref = ref;
}

/* value of n to 'value' (and make it a variable)
   does not copy value */
void
gel_dict_set_value(GelEFunc *n, GelETree *value)
{
	if(!n || !value)
		return;
	if(n->type == GEL_USER_FUNC ||
	   n->type == GEL_VARIABLE_FUNC)
		gel_freetree(n->data.user);
	n->type = GEL_VARIABLE_FUNC;
	g_slist_free(n->named_args);
	n->nargs = 0;
	n->named_args = NULL;
	
	n->data.user = value;
}
