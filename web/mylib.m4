m4_dnl  m4_define(`_LOCAL')
m4_changecom(`{{}}')m4_dnl
m4_define(`_COPY',<em> Copyright 1997-2001<br>Hilaire Fernandes<br>George Lebl<br></em>)m4_dnl
m4_define(`_VERSION',`0.5.10')m4_dnl
m4_define(`_EMAIL',` drgenius-dev@seul.org')m4_dnl
m4_define(`_UPDATE',<em>This page was updated on m4_esyscmd(date -u "+%Y/%m/%d at %T - %Z")</em>)m4_dnl
m4_define(`_LINK_TO_LABEL',<a href="#$2">$1</a>)m4_dnl
m4_define(`_INCLUDE_IMG',<img src="$1" ALT="$2">)m4_dnl
m4_define(`_SECTION_HEADER',<a name="$1"><h3>$2</h3></a>
$3<br />_LINK_TO_LABEL($4,$4)<hr />)
m4_define(`',
 m4_ifdef(`_LOCAL',`file:~/web-drgenius/',`'))m4_dnl
m4_define(`_HEAD',`<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="Author" content="Hilaire Fernandes" />
<meta name="Description" content="DR GENIUS IS GENERAL MATHEMATICS SOFTWARE." />
<meta name="KeyWords" content="MATHEMATIQUE, GEOMETRIE, GEOMETRY, DYNAMIC,
DYNAMIQUE, SKETCHPAD" />
<title>$1</title>
</head>')m4_dnl
m4_define(`_BODY',`<body text="#000000" bgcolor="#D8D0C8" link="#0000EE"
vlink="#FF6666" alink="#FF0000" background="$1" nosave>')m4_dnl
m4_define(`_AUTO_LINK',(&nbsp;<a href="$1">$1</a>&nbsp;))m4_dnl
m4_dnl
m4_define(`_LINK',<a href="$1">$2</a>)m4_dnl
m4_define(`_MAIN',<table cellspacing="0" cellpadding="0" width="100%" ><tr nosave>
<td align="left" valign="top" nowrap width="220">
<b>Go to the page :</b>&nbsp;<br />
<font color="#000000"><font size="-1">
<a href="index.html">project home page:</a>
<br />&nbsp;&nbsp;
  <a href="download.html">download</a>
<br />&nbsp;&nbsp;
  <a href="ml.html">mailing list</a>
<br />&nbsp;&nbsp;
  <a href="bug.html">reporting bugs</a>
<br />&nbsp;&nbsp;
  <a href="cvs.html">cvs</a>
<br />&nbsp;&nbsp;
  <a href="log.html">change log</a>
<br />&nbsp;&nbsp;
  <a href="todo.html">to do list</a>
<br />&nbsp;&nbsp;
  <a href="faq.html">faq</a>
<br />&nbsp;&nbsp;
  <a href="doc.html">documentation</a>
<br />&nbsp;&nbsp; 
  <a href="snap.html">screenshots</a>
<br /><br />
<br /><br />
Visit <a href="http://www.ofset.org">OFSET</a> to know more about<br>
free software and education&nbsp;<br />
</font>
<p><br>
<A href="http://sourceforge.net"> 
<IMG src="http://sourceforge.net/sflogo.php?group_id=13320" width="88" height="31"
border="0" alt="SourceForge Logo"></A>
</td>

<td>
)m4_dnl
m4_define(`_HOME_BODY_TOP',
<center>
<img src="drgenius.jpg" alt="Dr. Genius logo">
</center>
<br />
<center><font SIZE=+2><b>D</b>r Genius <b>R</b>efers to <b>G</b>eometry <b>E</b>xploration and 
<b>N</b>umeric<b> I</b>ntuitive <b>U</b>ser <b>S</b>ystem</font></center>
<div align="right">_COPY</div>
<br />
)m4_dnl
m4_define(`_SUB_BODY_TOP',
<br/>
<img src="drgenius-small.jpg" alt="Dr. Genius logo" align="right">
<center><font SIZE=+2><b>D</b>r Genius <b>R</b>efers to <b>G</b>eometry <b>E</b>xploration and 
<b>N</b>umeric<b> I</b>ntuitive <b>U</b>ser <b>S</b>ystem</font></center>
<div align="right">_COPY</div>
<br />
)m4_dnl
m4_define(`_END_MAIN',<br /><hr width="100%"><br />
<center>A _LINK(http://www.gnome.org, Gnome) project<br /></center>
</td></tr></table>)m4_dnl
m4_define(`_FOOT',
<br /><br /><br /><table width="100%">
<tr>
<td NOWRAP width="128">


</td>
<td align="center" valign="center" width="100%">
<center>$1</center></td></tr></table><p align="right">_UPDATE</p>)m4_dnl
