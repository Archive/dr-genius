m4_include(mylib.m4)
<HTML>
_HEAD(`Dr.&nbsp;Genius Log')
_BODY(ac536.png)

_MAIN
_SUB_BODY_TOP

<P>
<H2>New in Dr.&nbsp;Genius:</H2>


<ul>New in v 0.5.10
 <li>A super tetra cool buggy multi-level undo/redo system for the
geometric engine.

</ul>

<ul>New in v 0.5.9
 <li>Moslty a fix over version 0.5.8 that didn't compile.
 <li>Put the export menu item under a sub menu. Thanks to Fr�d�ric
Bonnaud.
</ul>

<ul>New in v 0.5.8
 <li>There is an eps exporter for geometric figure, this is a patch
from Fr�d�ric Bonnaud based on the latex exporter.

 <li>The geometry engine uses double buffering when the user moves the
figure. This is a X server side process so should be usable with
X-terminals.

 <li>There is now a specific tool-bar for geometric child. The
tool-bar comes with sub-tool-bars. These make the application more
easy for kids while looking for the different functions.

 <li>The canvas - the stuff to display function curve - is no more
part of Dr.&nbsp;Genius, it was unsupported and need more work. External
solution should be use instead.
</ul>

<ul>New in v 0.5.7
 <li>Laurent and Fabrice Bellet (Fabrice.Bellet@creatis.insa-lyon.fr)
fix a detection problem of GMP 3.x in configure.in
 <li>Exporting to LaTex now covers all kind of object. It's now
possible to choose the file name to export.
 <li> In the geometric engine, the style of object: clarify the situation
between invisible (in situation when it needs to be displayed) and
visible dashed line. Invisible line are displayed dashed with two
colors: yellow and the original. Visible dashed line are displayed as
expected.
</ul>

<ul>New in v 0.5.6
 <li>George remove the GOB tree from Dr.&nbsp;Genius. This means GOB
needs to be installed separatly. You can get it
_LINK(http://www.5z.com/jirka/gob.html, there).
 <li>Hilaire has implemented a new geometric engine. Most of the
functionnality are the same right now but the code is cleaner and
smaller. By incidence the xml format of the geometric figure and
macro-construction have changed. It's simpler. Look at
dr-genius/drgeo/DeveloperFAQ for more.
 <li>Laurent has submitted a couple of geometric figures with
locus. With that Hilaire has improved a bit the locus engine but it's a bit
slow.
 <li>Hilaire is working on a Latex export facility using PSTricks.
</ul>

<ul>New in v 0.5.5
 <li>George is working hard on the new genius interface. So we (canvas
& figure) will be able to interact with a console.
 <li>The XML stuff is finished, so it's now possible to save/load
multiple data at once. The data are  now figure & macro but the
interface is there to save other kind of data when ready.
 <li>Dr Geo view now use the George's GOB GTK builder to create new
MDI child.
</ul>

<ul>New in v 0.5.0
 <li>Integration of the Gnome Genius Calculator and GTK Dr Geo.
 <li>New GUI interace, including MDI with Genius, Dr Geo, GEL buffer view.
 <li>XML file format, still in progress, to save all kind of mixed data from the MDI view.
</ul>

<P>
<H2>Recent chage in the CVS:</H2>
_LINK(http://cvs.gnome.org/lxr/source/dr-genius/ChangeLog,Dr.&nbsp;Genius - Main ChangeLog)<BR>
_LINK(http://cvs.gnome.org/lxr/source/dr-genius/drgeo/ChangeLog, Dr.&nbsp;Genius - Geometric engine ChangeLog)<BR>
_LINK(http://cvs.gnome.org/lxr/source/dr-genius/genius/ChangeLog, Dr.&nbsp;Genius - Console Genius calculator ChangeLog)<BR>

<p>Here are the code commits during the last
_LINK(http://cvs.gnome.org/bonsai/cvsquery.cgi?module=all&branch=&branchtype=match&dir=dr-genius&file=&filetype=match&who=&whotype=match&sortby=Date&hours=2&date=week&mindate=&maxdate=&cvsroot=%2Fcvs%2Fgnome,week).

<BR>

_END_MAIN

_FOOT()
</BODY>
</HTML>


