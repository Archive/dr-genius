m4_include(mylib.m4)
<HTML>
_HEAD(`Dr.&nbsp;Genius To Do List')
_BODY(ac536.png)

_MAIN
_SUB_BODY_TOP

<UL>If you want to contribute some code, please do in one of this
area. You can also contribute to the documentation, contact
_LINK(http://www.fas.harvard.edu/~nbarth/,Nils Barth).

<LI>Including the documentation in the distribution. Should be done
since a long time!!

<LI>Finish implementing the new calculator interface.

<LI>Deeper integration between the Genius console and a geometric view

<LI>Printing support for geometric figure, this can be done very
independently by defining a new drawable class that use the gnome-print interface

<LI>Turning a geometric view to a bonobo view.

<LI>Remote control of a figure throught corba interface.

<LI>Adding Python support in Dr.&nbsp;Geo.

</UL>

<P>Also here is a dump of the various TODO file of the project:

<p><B>dr-genius/TODO:</B><BR>
<pre>
m4_include(../TODO)
<pre>

<p><B>dr-genius/TODO-NILS:</B><BR>
<pre>
m4_include(../TODO-NILS)
<pre>

_END_MAIN

_FOOT()
</BODY>
</HTML>
