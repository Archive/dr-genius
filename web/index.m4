m4_include(mylib.m4)
<HTML>
_HEAD(Dr.&nbsp;Genius _VERSION)
_BODY(ac536.png)

_MAIN
_HOME_BODY_TOP

<H3><U>What is Dr.&nbsp;Genius?</U></H3><BR>
<p> Dr.&nbsp;Genius is an interactive geometry program plus a calculator
similiar in some aspects to bc and Matlab. Dr.&nbsp;Genius is powered by
GEL, its extention language. In fact, a large part of the standard
calculator functions are written in GEL itself. 

<p> 
Dr.&nbsp;Genius is a piece of <B>GNU</B> software. This means you don't
have to pay for it but even more importantly you have access to the
source code.  You can modify and distribute it as long as the same
distribution license (_LINK(http://www.gnu.org/copyleft/gpl.html#SEC1,GPL))
is used.<BR> Dr.&nbsp;Genius is released under the GPL license. To learn more
about this license and the Free Software Foundation,
visit _LINK(http://www.gnu.org,the GNU web site) or read the file
COPYING in the distribution.<BR>

<P>
<H3><U>Minimum system requirements</U></H3><BR>
<UL>
<LI>A Unix-compatible operating system (Dr.&nbsp;Genius is developed with
GNU/Linux)
<LI>October _LINK(http://www.gnome.org,GNOME)
<LI>All tools needed to compile a typical Gnome application
<LI>You will also need gmp, the GNU Multiple Precision library which you
can get at the _LINK(http://www.swox.com/gmp/,GNU MP Homepage)
</UL>
<BR>

<H3><U>Developers</U></H3><BR> <p>Dr.&nbsp;Genius is the merge of two
projects know as Gnome Genius Caculator from
_LINK(http://www.5z.com/jirka,George Lebl) and GTK Dr.&nbsp;Geo from _LINK(http://drgenius.seul.org/~hilaire,Hilaire
Fernandes).<BR> Gnome Genius Calculator is a multiple precision calculator
with an interpreted language, GEL, plus a large mathematical library written
in GEL.<BR> Dr.&nbsp;Geo is a sort of vector drawing software but with
mathematical constraints -  we also call this interactive geometry.<BR><BR>

Other people are also involved in Dr.&nbsp;Genius:
<ul>
<LI>_LINK(http://drgenius.seul.org/~lolo,Laurent Gauthier) (MDI interface, redesign in the GUI interface)
<LI>_LINK(http://www.fas.harvard.edu/~nbarth/,Nils Barth) (Math. library written in GEL, documentation)
<LI>_LINK(http://www.blender-cafe.org/,Fr�d�ric Toussaint) (Graphic art)
<ul>

_END_MAIN

_FOOT()
</BODY>
</HTML>





