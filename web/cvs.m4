m4_include(mylib.m4)
<HTML>
_HEAD(`Dr.&nbsp;Genius _VERSION CVS')
_BODY(ac536.png)

_MAIN 
_SUB_BODY_TOP

From the Gnome web site:
<p>
Many people who are interested in working with GNOME development find
their first hurdle is figuring out how to use CVS. This document will
be a quick tutorial.
<p>
CVS is a powerful method of allowing many developers work on the same
source code. This is possible because each developer checks out a copy
of the current version of the source code. Then they each
independently work on their personal copy of the sources. When they
have made changes, they commit them back to the CVS repository. The
CVS server takes care of things like trying to merge their changes
with those of others. When that doesn't work, the developer is
notified and they do a hand merge of the conflicts.  

<p>
To get the newest Dr.&nbsp;Genius source from the CVS, you need to have cvs
installed, next you need to setup a few environmental variables.

<p>
The first command you need is:<HR>
<code>export CVSROOT=':pserver:anonymous@anoncvs.gnome.org:/cvs/gnome'</code><HR>
You might want to put this in your login script. 

<p>
Then do the command: 
<HR><code>cvs login</code><HR>
(there is no password, just hit return)

<p>Now you can grab the Dr.&nbsp;Genius module
<hR><code>cvs -z3 checkout dr-genius</code><HR>

<p>
To read more on the CVS on Gnome go _LINK(http://developer.gnome.org/tools/cvs.html,there).
<BR>
<p>Alternatively you can browse the Dr.&nbsp;Genius code _LINK(http://cvs.gnome.org/lxr/source/dr-genius/,online).
<BR>
_END_MAIN

_FOOT()
</BODY>
</HTML>
