#include "config.h"

#include <gnome.h>
#include <gtk/gtk.h>
#include <zvt/zvtterm.h>

#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include "genius/calc.h"
#include "genius/util.h"
#include "genius/dict.h"

#include "genius/plugin.h"
#include "genius/inter.h"

#include "genius_stuff.h"

char *genius_binary_dir=NULL;

/*FIXME: we need to do the context stuff right */
/*calculator state*/
calcstate_t curstate={
	256,
	0,
	FALSE,
	FALSE,
	FALSE,
	5,
	TRUE,
	10
	};


/* FIXME: just hacks before we make dialogs again*/
static int errors_printed = 0;
static void
puterror(char *s)
{
	char *file;
	int line;
	get_file_info(&file,&line);
	if(file)
		fprintf(stderr,"%s:%d: %s\n",file,line,s);
	else if(line>0)
		fprintf(stderr,_("line %d: %s\n"),line,s);
	else
		fprintf(stderr,"%s\n",s);
}

static void
calc_puterror(char *s)
{
	if(curstate.max_errors == 0 ||
	   errors_printed++<curstate.max_errors)
		puterror(s);
}

void
genius_init(char *argv0)
{
	char *file;

	genius_is_gui = TRUE;
	
	genius_binary_dir = g_dirname(argv0);

	read_plugin_list();

	/* FIXME: we need to put out errors onto a dialog or something */
	set_new_calcstate(curstate);
	set_new_errorout(calc_puterror);
	set_new_infoout(puterror);

	/*init the context stack and clear out any stale dictionaries
	  except the global one, if this is the first time called it
	  will also register the builtin routines with the global
	  dictionary*/
	d_singlecontext();

	load_compiled_file(NULL, LIBRARY_DIR "/gel/lib.cgel",FALSE);

	/*try the library file in the current directory*/
	load_compiled_file(NULL, "lib.cgel",FALSE);
	
	file = g_strconcat(getenv("HOME"),"/.geniusinit",NULL);
	if(file)
		load_file(NULL, file,FALSE);
	g_free(file);
}
