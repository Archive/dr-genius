# Numerial differentiation
# 
# The algorithms are described in:
# BurdenFaires
#FIXME: This file requires limits.gel and misc.gel for the simple NDerivative function
# and for is_differentiable

# In the below, f indicates the function whose derivative we wish to
# determine, x0 the point at which we wish to differentiate, and h how
# close to f we want to take secant lines
#
# FIXME:
# The main problem with numerical differentiation is that it is
# numerically unstable, in that you can't just let h go to zero and
# hope that the derivative converges, because in the definition of the
# derivative you take the difference of f(x+h) and f(x), which are very
# close numbers, so the difference is small and known to ever fewer digits of
# accuracy.
# Thus, it would make sense to jack up the precision when letting h get small
# if doing this in an automated way
# However, with the default precision in Dr. Genius 0.4.5, these methods
# seem to be pretty stable down to h=10^-20, and only start getting really bad
# around 10^-70

# These methods all return one value, f'(x0)

# FIXME:
# Currently only works for real/complex functions of a *real* variable
# Allow higher order (arbitrary order) derivatives

# One-sided three-point formula, Section 4.1, Formula 4.4, p. 160
function one_sided_three_point_formula(f,x0,h) =
# This has error term max(f''')h^2/3
	(
# check arguments
## check types
	if(not is_function(f)) then
		(error("one_sided_three_point_formula: argument 1 must be a function");bailout)
	else if(not is_real(h)) then
		(error("one_sided_three_point_formula: argument 2 must be real values");bailout);
## check bounds
	if(h==0) then (error("one_sided_three_point_formula: argument 2 must be non-zero (negative for left-handed derivative, positive for right-handed)");bailout);

# Start calculating
        (-3*f(x0)+4*f(x0+h)-f(x0+2*h))/(2*h)
	)
sethelp("one_sided_three_point_formula","compute one-sided derivative using three-point formula");
protect("one_sided_three_point_formula");

# Two-sided three-point formula, Section 4.1, Formula 4.5, p. 161
function two_sided_three_point_formula(f,x0,h) =
# This has error term max(f''')h^2/6
	(
# check arguments
## check types
	if(not is_function(f)) then
		(error("two_sided_three_point_formula: argument 1 must be a function");bailout)
	else if(not is_real(h)) then
		(error("two_sided_three_point_formula: argument 2 must be real values");bailout);
## check bounds
	if(h==0) then (error("two_sided_three_point_formula: argument 2 must be non-zero");bailout);

# Start calculating
        (f(x0+h)-f(x0-h))/(2*h)
	)
sethelp("two_sided_three_point_formula","compute two-sided derivative using three-point formula");
protect("two_sided_three_point_formula");

# One-sided five-point formula, Section 4.1, Formula 4.7, p. 161
function one_sided_five_point_formula(f,x0,h) =
# This has error term max(f''''')h^4/5
	(
# check arguments
## check types
	if(not is_function(f)) then
		(error("one_sided_five_point_formula: argument 1 must be a function");bailout)
	else if(not is_real(h)) then
		(error("one_sided_five_point_formula: argument 2 must be real values");bailout);
## check bounds
	if(h==0) then (error("one_sided_five_point_formula: argument 2 must be non-zero (negative for left-handed derivative, positive for right-handed)");bailout);

# Start calculating
        (-25*f(x0)+48*f(x0+h)-36*f(x0+2*h)+16*f(x0+3*h)-3*f(x0+4*h))/(12*h)
	)
sethelp("one_sided_five_point_formula","compute one-sided derivative using five point formula");
protect("one_sided_five_point_formula");

# Two-sided five-point formula, Section 4.1, Formula 4.6, p. 161
function two_sided_five_point_formula(f,x0,h) =
# This has error term max(f''''')h^4/30
	(
# check arguments
## check types
	if(not is_function(f)) then
		(error("two_sided_five_point_formula: argument 1 must be a function");bailout)
	else if(not is_real(h)) then
		(error("two_sided_five_point_formula: argument 2 must be real values");bailout);
## check bounds
	if(h==0) then (error("two_sided_five_point_formula: argument 2 must be non-zero");bailout);

# Start calculating
        (f(x0-2*h)-8*f(x0-h)+8*f(x0+h)-f(x0+2*h))/(12*h)
	)
sethelp("two_sided_five_point_formula","compute two-sided derivative using five-point formula");
protect("two_sided_five_point_formula");

# FIXME: There should be a better way of setting parameters
derivative_tolerance=10^(-5);
derivative_sfs=20;
derivative_number_of_tries=100;

#FIXME: This file requires limits.gel and misc.gel for the simple NDerivative function
# and for is_differentiable

# Simple test for differentiability
function is_differentiable(f,x0) =
	(
	# First, differentiable functions have to be CONTINUOUS
	if not(is_continuous(f,x0)) then return null;
	if LeftLimit(`(h)=(one_sided_five_point_formula(f,x0,h),0))==RightLimit(`(h)=(one_sided_five_point_formula(f,x0,h),0))
	 then return true
	 else return false
	)

# Simple derivative function
#FIXME!!!! BROKEN!!!!
function NDerivative(f,x0) =
# returns f'(x0)
	(
	if (is_differentiable(f,x0))
	 then return numerical_limit_at_infinity(`(x)=(two_sided_five_point_formula(f,x0,x)),`(x)=(2^(-x)),derivative_tolerance,derivative_sfs,derivative_number_of_tries)
         else return null
	)
