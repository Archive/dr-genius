#FIXME: This should be catalogued a bit better.

function apply_over_matrix1(a,func) = (
	if(not is_matrix(a)) then
		(error("apply_over_matrix1: argument 1 must be a matrix");bailout)
	else if(not is_function(func)) then
		(error("apply_over_matrix1: argument 2 must be a function");bailout);
	for i = 1 to rows(a) do (
		for j = 1 to columns(a) do (
			r@(i,j) = func(a@(i,j))
		)
	);
	r
);
protect("apply_over_matrix1")

function apply_over_matrix2(a,b,func) = (
	if(not is_matrix(a) and not is_matrix(b)) then
		(error("apply_over_matrix2: argument 1 or 2 must be a matrix");bailout)
	else if(not is_function(func)) then
		(error("apply_over_matrix2: argument 3 must be a function");bailout)

	else if(is_matrix(a) and is_matrix(b) and
		(rows(a)!=rows(b) or columns(a)!=columns(b))) then
		(error("apply_over_matrix2: cannot apply a function over two matrixes of different sizes");bailout);
	
	for i = 1 to rows(a) do (
		for j = 1 to columns(a) do (
			if(is_matrix(a)) then
				aa = a@(i,j)
			else
				aa = a;
			if(is_matrix(b)) then
				bb = b@(i,j)
			else
				bb = b;
			r@(i,j) = func(aa,bb)
		)
	);
	r
);
protect("apply_over_matrix2")

#calculate a trace function
sethelp("trace","Calculate the trace of a matrix");
function trace(m) = (
	if(not is_matrix(m) or not is_value_only(m)) then
		(error("trace: argument not a value only matrix");bailout)
	else if(rows(m)!=columns(m)) then
		(error("trace: matrix not a square");bailout);
	a=0;
	for i = 1 to rows(m) do (a=a+m@(i,i))
);
protect("trace")

#calculate convolution of two horizontal vectors
sethelp("convol","Calculate convolution of two horizontal vectors");
function convol(a,b) = (
	if(not is_matrix(a) or not is_value_only(a) or
	   not is_matrix(b) or not is_value_only(b) or
	   rows(a)>1 or rows(b)>1) then
		(error("convol: arguments not value only horizontal vectors");bailout)
	else if(columns(a)!=columns(b)) then
		(error("convol: arguments must be identical vectors");bailout);
	sum = 0;
	for i = 1 to columns(a) do (
		sum = sum + a@(1,i)*b@(1,columns(a)-i+1)
	)
);
protect("convol")

#calculate convolution of two horizontal vectors and return the result
#not added together but in a vector
sethelp("convol_vec","Calculate convolution of two horizontal vectors");
function convol_vec(a,b) = (
	if(not is_matrix(a) or not is_value_only(a) or
	   not is_matrix(b) or not is_value_only(b) or
	   rows(a)>1 or rows(b)>1) then
		(error("convol_vec: arguments not value only horizontal vectors");bailout)
	else if(columns(a)!=columns(b)) then
		(error("convol_vec: arguments must be identical vectors");bailout);
	r = set_size([0],1,columns(a));
	for i = 1 to columns(a) do (
		r@(1,i) = a@(1,i)*b@(1,columns(a)-i+1)
	);
	r
);
protect("convol_vec")

#calculate the sum of all elements in a matrix
sethelp("matsum","Calculate the sum of all elements in a matrix");
function matsum(a) = (
	if(not is_matrix(a) or not is_value_only(a)) then
		(error("matsum: argument not a value only matrix");bailout);
	sum = 0;

	for n in a do
		sum = sum + n
);
protect("matsum")

sethelp("matsum_sq","Calculate the sum of squares of all elements in a matrix");
function matsum_sq(a) = (
	if(not is_matrix(a) or not is_value_only(a)) then
		(error("matsum_sq: argument not a value only matrix");bailout);
	sum = 0;

	for n in a do
		sum = sum + n^2
);
protect("matsum_sq")

#calculate the product of all elements in a matrix
sethelp("matprod","Calculate the product of all elements in a matrix");
function matprod(a) = (
	if(not is_matrix(a) or not is_value_only(a)) then
		(error("matprod: argument not a value only matrix");bailout);
	prod = 1;
	for n in a do
		prod = prod * n
);
protect("matprod")

sethelp("delrowcol","Remove column and row from a matrix");
function delrowcol(m,row,col) = (
	if(not is_matrix(m) or not is_integer(row) or not is_integer(col)) then
		(error("swaprow: arguments are not the right type");bailout)
	else if(row>rows(m) or col>columns(m)) then
		(error("delrowcol: arguments out of range");bailout);
	if row==1 then (
		if col==1 then
			m@(2..rows(m),2..columns(m))
		else if col==columns(m) then
			m@(2..rows(m),1..columns(m)-1)
		else
			[m@(2..rows(m),1..col-1),\
			 m@(2..rows(m),col+1..columns(m))]
	) else if row==rows(m) then (
		if col==1 then
			m@(1..rows(m)-1,2..columns(m))
		else if col==columns(m) then
			m@(1..rows(m)-1,1..columns(m)-1)
		else
			[m@(1..rows(m)-1,1..col-1),\
			 m@(1..rows(m)-1,col+1..columns(m))]
	) else (
		if col==1 then
			[m@(1..row-1,2..columns(m))
			 m@(row+1..rows(m),2..columns(m))]
		else if col==columns(m) then
			[m@(1..row-1,1..columns(m)-1)
			 m@(row+1..rows(m),1..columns(m)-1)]
		else
			[m@(1..row-1,1..col-1),m@(1..row-1,col+1..columns(m))
			 m@(row+1..rows(m),1..col-1),\
			 m@(row+1..rows(m),col+1..columns(m))]
	)
);
protect("delrowcol")

# Minor of a matrix (determinant of a submatrix given by deleting
# one row and one column)
# FIXME: allow k x k minors, arbitrary submatrices, etc.
function Minor(M,i,j)=det(delrowcol(M,i,j))
protect("Minor");

#adjoint of a matrix
sethelp("adj","Get the adjoint of a matrix");
function adj(m) = (
	if(not is_matrix(m) or not is_value_only(m)) then
		(error("adj: argument not a value-only matrix");bailout)
	else if(rows(m)!=columns(m)) then
		(error("adj: argument not a square matrix");bailout)
	else if(rows(m)<2) then
		(error("adj: argument cannot be 1x1 matrix");bailout);

	a = set_size([0],rows(m),rows(m));
	for i = 1 to rows(m) do (
		for j = 1 to rows(m) do (
			a@(j,i) = ((-1)^(i+j))*Minor(m,i,j);
		);
	);
	a
);
protect("adj")

sethelp("minimize","Find the first value where f(x)=0");
function minimize(func,x,incr) = (
	if(not is_value(x) or not is_value(incr)) then
		(error("minimize: x,incr arguments not values");bailout)
	else if(not is_function(func)) then
		(error("minimize: func argument not a function");bailout);
	while(func(x)>0) do x=x+incr;
	x
);
protect("minimize")

sethelp("diagonal","Make diagonal matrix from a vector");
function diagonal(v) = (
	if(not is_matrix(v) or rows(v)>1) then
		(error("diagonal: argument not a horizontal vector");bailout);
	r = set_size([0],columns(v),columns(v));
	for i = 1 to columns(v) do (
		r@(i,i) = v@(1,i)
	);
	r
);
protect("diagonal")

sethelp("swaprow","Swap two rows in a matrix");
function swaprow(m,row1,row2) = (
	if(not is_matrix(m) or not is_integer(row1) or
	   not is_integer(row2)) then
		(error("swaprow: arguments are not the right type");bailout)
	else if(row1>rows(m) or row2>rows(m)) then
		(error("swaprow: argument out of range");bailout)
	else if(row1 != row2) then (
		tmp = m@(row1,);
		m@(row1,) = m@(row2,);
		m@(row2,) = tmp
	);
	m
);
protect("swaprow")

sethelp("rowsum","Calculate sum of each row in a matrix");
function rowsum(m) = (
	if not is_matrix(m) then
		(error("rowsum: argument not matrix");bailout);
	r = set_size([0],rows(m),1);
	for i = 1 to rows(m) do (
		for j = 1 to columns(m) do
			r@(i,1) = r@(i,1) + m@(i,j)
	);
	r
);
protect("rowsum")

sethelp("rowsum_sq","Calculate sum of squares of each row in a matrix");
function rowsum_sq(m) = (
	if not is_matrix(m) then
		(error("rowsum_sq: argument not matrix");bailout);
	r = set_size([0],rows(m),1);
	for i = 1 to rows(m) do (
		for j = 1 to columns(m) do
			r@(i,1) = r@(i,1) + m@(i,j)^2
	);
	r
);
protect("rowsum_sq")

#sort a horizontal vector
sethelp("sortv","Sort a horizontal vector");
function sortv(v) = (
	if not is_matrix(v) or rows(v)>1 then
		(error("sortv: argument not a horizontal vector");bailout);
	j = 1;
	do (
		sorted = 1;
		for i = 1 to columns(v)-j do (
			if v@(1,i)>v@(1,i+1) then (
				t = v@(1,i);
				v@(1,i) = v@(1,i+1);
				v@(1,i+1) = t;
				sorted = 0
			)
		);
		j=j+1
	) while not sorted;
	v
);
protect("sortv")

sethelp("reversev","Reverse elements in a vector");
function reversev(v) = (
	if not is_matrix(v) or rows(v)>1 then
		(error("reversev: argument not a horizontal vector");bailout);
	r=set_size([0],1,columns(v));
	for i = 1 to columns(v) do
		r@(1,i) = v@(1,columns(v)-i+1);
	r
);
protect("reversev")
