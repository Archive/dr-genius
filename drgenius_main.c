
#include <config.h>
#include <gnome.h>
#include <glade/glade.h>
#include <libgnorba/gnorba.h>

#include "drgenius_mdi.h"

#include "genius_stuff.h"


static void session_die(GnomeClient* client, gpointer client_data);

static gint save_session(GnomeClient *client, gint phase, 
                         GnomeSaveStyle save_style,
                         gint is_shutdown, GnomeInteractStyle interact_style,
                         gint is_fast, gpointer client_data);


static char* file_name  = NULL;

struct poptOption options[] = {
  { 
    "file",
    'f',
    POPT_ARG_STRING,
    &file_name,
    0,
    N_("File to load"),
    N_("MESSAGE")
  },
  {
    NULL,
    '\0',
    0,
    NULL,
    0,
    NULL,
    NULL
  }
};

GnomeMDI* mdi;

int 
main(int argc, char* argv[])
{
  CORBA_ORB orb;
  CORBA_Environment ev;

  poptContext pctx;

  const char** args;

  GnomeClient* client;

  bindtextdomain(PACKAGE, GNOMELOCALEDIR);  
  textdomain(PACKAGE);

  CORBA_exception_init(&ev);
  orb = gnome_CORBA_init_with_popt_table(PACKAGE, VERSION, &argc, argv, 
					 options, 0, &pctx,
					 GNORBA_INIT_SERVER_FUNC, &ev);  
  CORBA_exception_free(&ev);

  /* Init Glade stuff */
  glade_gnome_init ();
  
  /* Argument parsing */

  args = poptGetArgs(pctx);

  /* XXX: Handle command line parameters here.  */

  poptFreeContext(pctx);

  /* Session Management */
  
  client = gnome_master_client ();
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
                      GTK_SIGNAL_FUNC (save_session), argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
                      GTK_SIGNAL_FUNC (session_die), NULL);

  /* Initialize the genius engine */
  genius_init(argv[0]);
  

  /* Main app */

  mdi = drgenius_mdi_new(file_name);

  /* Display the main application.  */
  gnome_mdi_open_toplevel(mdi);

  gtk_main();

  return 0;
}

static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
  gchar** argv;
  guint argc;

  /* allocate 0-filled, so it will be NULL-terminated */
  /* XXX: handle session management properly.  */
  argv = g_malloc0(sizeof(gchar*)*4);
  argc = 1;

  argv[0] = client_data;

  if (file_name)
    {
      argv[1] = "--file";
      argv[2] = file_name;
      argc = 3;
    }
  
  gnome_client_set_clone_command (client, argc, argv);
  gnome_client_set_restart_command (client, argc, argv);

  return TRUE;
}

static void
session_die(GnomeClient* client, gpointer client_data)
{
  gtk_main_quit();
}


