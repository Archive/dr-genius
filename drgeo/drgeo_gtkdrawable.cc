/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-2000
 * hilaire@ofset.org 
 * 
 * (C) Copyright Laurent Gauthier 1999
 * lolo@seul.org
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glade/glade.h>

#include "drgeo_gtkdrawable.h"
#include "drgeo_menu.h"
#include "drgenius_menus.h"

#define WIDGET_HEIGHT(w) (((w)->allocation).height)
#define WIDGET_WIDTH(w) (((w)->allocation).width)

static void drawing_area_button_press_cb (GtkWidget * w, GdkEventButton * event,
					  gpointer data);
static void drawing_area_button_release_cb (GtkWidget * w, GdkEventButton * event,
					    gpointer data);
static void drawing_area_move_cb (GtkWidget * w, GdkEventMotion * event,
				  gpointer data);
static void drawing_area_expose_cb (GtkWidget * w, gpointer data);
static void drawing_area_configure_cb (GtkWidget * w, gpointer data);
static int timer_cb (drgeoGtkDrawable * drawable);
static void vadjustment_cb (GtkAdjustment * w, gpointer data);
static void hadjustment_cb (GtkAdjustment * w, gpointer data);
static gint choose_item_cb (GtkWidget * w, geometricObject * choice);
static void dummy_cb (GtkWidget * w, gpointer data);


drgeoGtkDrawable::
drgeoGtkDrawable (drgeoFigure *figure)
{
	GtkWidget *widget, *toolbar, *icon;
	static int counter = 1;	/* Used to generated title for new
				   documents.  */
	char *view_title;
	GladeXML *xmlWidget;

	this->figure = figure;

	scale = 30.0;		/* default is 30 pixels for 1 unit.  */
	origin_x = 0.0;
	origin_y = 0.0;

	/* Create a table to hold the GUI.  */
	table = gtk_table_new (4, 3, FALSE);
	gtk_widget_show (table);
	gtk_widget_hide (table);	/* Don't show the widget initially.  */

	/* The drawing area.  */
	drawing_area = gtk_drawing_area_new ();
	paintThere = doubleBuffering = NULL;
	figure_gc = NULL;

	gtk_table_attach (GTK_TABLE (table), drawing_area, 1, 2, 2, 3,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL | GTK_SHRINK),
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL | GTK_SHRINK),
			  0, 0);
	gtk_widget_grab_focus (drawing_area);
	gtk_widget_set_events (drawing_area, GDK_BUTTON_PRESS_MASK
			       | GDK_BUTTON_RELEASE_MASK
			       | GDK_POINTER_MOTION_MASK
			       | GDK_EXPOSURE_MASK
			       | GDK_POINTER_MOTION_HINT_MASK);

	gtk_signal_connect (GTK_OBJECT (drawing_area),
			    "button_press_event",
			    GTK_SIGNAL_FUNC (drawing_area_button_press_cb),
			    NULL);

	gtk_signal_connect (GTK_OBJECT (drawing_area),
			    "button_release_event",
			    GTK_SIGNAL_FUNC (drawing_area_button_release_cb),
			    NULL);

	gtk_signal_connect (GTK_OBJECT (drawing_area),
			    "motion_notify_event",
			    GTK_SIGNAL_FUNC (drawing_area_move_cb),
			    NULL);

	gtk_signal_connect (GTK_OBJECT (drawing_area),
			    "expose_event",
			    GTK_SIGNAL_FUNC (drawing_area_expose_cb),
			    NULL);

	gtk_signal_connect (GTK_OBJECT (drawing_area),
			    "configure_event",
			    GTK_SIGNAL_FUNC (drawing_area_configure_cb),
			    NULL);


	gtk_widget_show (drawing_area);


	// attach a popup menu to the drawing area
	gnome_popup_menu_attach (build_drgeo_popup_menu (),
				 drawing_area,
				 (gpointer) this);



	/* Create the vertical and horizontal rulers.  */
	hrule = gtk_hruler_new ();
	vrule = gtk_vruler_new ();

	/* Horizontal adjustment.  */
	hadjustment = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, -100.0, 100.0, 0.1, 0.5, 2.0));

	gtk_signal_connect (GTK_OBJECT (hadjustment),
			    "value_changed",
			    GTK_SIGNAL_FUNC (hadjustment_cb),
			    (gpointer) hrule);

	/* Vertical adjustment.  */
	vadjustment = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, -100.0, 100.0, 0.1, 0.5, 2.0));

	gtk_signal_connect (GTK_OBJECT (vadjustment),
			    "value_changed",
			    GTK_SIGNAL_FUNC (vadjustment_cb),
			    (gpointer) vrule);

	/* Horizontal scrollbar. */
	widget = gtk_hscrollbar_new (GTK_ADJUSTMENT (hadjustment));

	gtk_table_attach (GTK_TABLE (table), widget, 1, 2, 3, 4,
			  (GtkAttachOptions) (GTK_FILL | GTK_SHRINK),
			  (GtkAttachOptions) (GTK_SHRINK),
			  0, 0);

	gtk_widget_show (widget);

	/* Vertical scrollbar. */
	widget = gtk_vscrollbar_new (GTK_ADJUSTMENT (vadjustment));

	gtk_table_attach (GTK_TABLE (table), widget, 2, 3, 2, 3,
			  (GtkAttachOptions) (GTK_SHRINK),
			  (GtkAttachOptions) (GTK_FILL | GTK_SHRINK),
			  0, 0);

	gtk_widget_show (widget);

	/* Macro definition used for rulers.  */
#define EVENT_METHOD(i, x) GTK_WIDGET_CLASS(GTK_OBJECT(i)->klass)->x

	/* Setup the horizontal ruler.  */
	gtk_ruler_set_metric (GTK_RULER (hrule), GTK_PIXELS);

	gtk_signal_connect_object (GTK_OBJECT (drawing_area),
				   "motion_notify_event",
		  (GtkSignalFunc) EVENT_METHOD (hrule, motion_notify_event),
				   GTK_OBJECT (hrule));

	gtk_table_attach (GTK_TABLE (table), hrule, 1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_FILL | GTK_SHRINK),
			  (GtkAttachOptions) (GTK_SHRINK),
			  0, 0);

	gtk_widget_show (hrule);

	/* setup the vertical ruler */
	gtk_ruler_set_metric (GTK_RULER (vrule), GTK_PIXELS);

	gtk_signal_connect_object (GTK_OBJECT (drawing_area),
				   "motion_notify_event",
		  (GtkSignalFunc) EVENT_METHOD (vrule, motion_notify_event),
				   GTK_OBJECT (vrule));

	gtk_table_attach (GTK_TABLE (table), vrule, 0, 1, 2, 3,
			  (GtkAttachOptions) (GTK_SHRINK),
			  (GtkAttachOptions) (GTK_FILL | GTK_SHRINK),
			  0, 0);

	gtk_widget_show (vrule);

	/* Associate the information about the figure to each of the
	   important widgets for it.  And keep track of these widgets in the
	   figure data structure.  */
	gtk_object_set_data (GTK_OBJECT (table), "figure", this);
	gtk_object_set_data (GTK_OBJECT (drawing_area), "figure", this);
	gtk_object_set_data (GTK_OBJECT (hrule), "figure", this);
	gtk_object_set_data (GTK_OBJECT (vrule), "figure", this);
	gtk_object_set_data (GTK_OBJECT (hadjustment), "figure", this);
	gtk_object_set_data (GTK_OBJECT (vadjustment), "figure", this);

}

drgeoGtkDrawable::
~drgeoGtkDrawable ()
{
	// XXX Fill this.
	printf ("Remove timer\n");
	g_free (figure_gc);
	g_free (font_gc);
	g_free (object_gc);
	gtk_timeout_remove (timeoutTag);
}

void drgeoGtkDrawable::
drawPoint (drgeoStyle & style, drgeoPoint & point)
{
	int x, y, radius;
	double width, height;
	char mode;

	mode = this->getFigure ()->getMode ();

	switch (style.type) {
	case drgeoLineInvisible:
		if (mode != MISE_EN_FORME_MODE)
			return;
		radius = 3;
		break;
	case drgeoLineDashed:
		radius = 2;
		break;
	case drgeoLineNormal:
		radius = 3;
		break;
	case drgeoLineThick:
		radius = 4;
		break;
	}

	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);

	// Compute the correct on screen location.
	x = (int) (width / 2.0 - origin_x * scale + point.getX () * scale);
	y = (int) (height / 2.0 + origin_y * scale - point.getY () * scale);

	// Adapt the GC to the style.
	gdk_gc_set_foreground (figure_gc, &color[style.color]);

	switch (style.pointShape) {
	case drgeoPointRound:
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		gdk_draw_arc (paintThere, figure_gc, style.fill, x - radius,
			   y - radius, radius * 2, radius * 2, 0, 360 * 64);
		break;
	case drgeoPointX:
		gdk_gc_set_line_attributes (figure_gc, 2, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);

		gdk_draw_line (paintThere, figure_gc, x - radius,
			       y - radius, x + radius, y + radius);
		gdk_draw_line (paintThere, figure_gc, x - radius,
			       y + radius, x + radius, y - radius);
		break;
	case drgeoPointRec:
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		gdk_draw_rectangle (paintThere, figure_gc, style.fill,
			  x - radius, y - radius, radius << 1, radius << 1);
		break;
	}
}

void drgeoGtkDrawable::
drawLine (drgeoStyle & style, drgeoPoint & start, drgeoPoint & end)
{
	// start : one point on the line
	// end : a direction vector of the line
	static drgeoPoint m, c[2];
	char pt = 0;
	// world boundaries
	double worldLeft, worldTop, worldRight, worldBottom, d;
	double width, height;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible and we are not in style mode
		return;
	}
	// size of the drawing area in world unit
	width = pixelToWorld (WIDGET_WIDTH (drawing_area));
	height = pixelToWorld (WIDGET_HEIGHT (drawing_area));
	worldLeft = origin_x - width / 2;
	worldRight = origin_x + width / 2;
	worldTop = origin_y + height / 2;
	worldBottom = origin_y - height / 2;

	if (end.getX () == 0 && end.getY () == 0)
		return;

	// m a point on the line
	m = start + 10 * end;
	if (ABS (start.getX () - m.getX ()) < EPSILON) 
	{
		c[0].set (start.getX (), worldTop);
		c[1].set (start.getX (), worldBottom);
		pt = 2;
	} 
	else 
	{
		// line slope
		d = end.getY () / end.getX ();
		if (ABS (d) < EPSILON) 
		{
			c[0].set (worldLeft, start.getY ());
			c[1].set (worldRight, start.getY ());
			pt = 2;
		} 
		else 
		{
			// ordonnee a l'origine
			double b = start.getY () - d * start.getX ();
			double y = d * worldLeft + b;
			if (y >= worldBottom && y < worldTop) 
			{
				c[pt++].set (worldLeft, y);
			}
			y = d * worldRight + b;
			if (y >= worldBottom && y < worldTop) 
			{
				c[pt++].set (worldRight, y);
			}
			double x = (worldTop - b) / d;
			if (x >= worldLeft && x < worldRight && pt < 2) 
			{
				c[pt++].set (x, worldTop);
			}
			x = (worldBottom - b) / d;
			if (x >= worldLeft && x < worldRight && pt < 2) {
				c[pt++].set (x, worldBottom);
			}
		}
	}
	if (pt == 2)
		drawSegment (style, c[0], c[1]);
}

void drgeoGtkDrawable::
drawHalfLine (drgeoStyle & style, drgeoPoint & point, drgeoVector & vect)
{
	static drgeoPoint c[2];
	char inDrawingArea = FALSE;
	double worldLeft, worldTop, worldRight, worldBottom;
	double width, height;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible and we are not in style mode
		return;
	}
	// size of the drawing area in world unit
	width = pixelToWorld (WIDGET_WIDTH (drawing_area));
	height = pixelToWorld (WIDGET_HEIGHT (drawing_area));
	worldLeft = origin_x - width / 2;
	worldRight = origin_x + width / 2;
	worldTop = origin_y + height / 2;
	worldBottom = origin_y - height / 2;

	if (vect.getX () == 0 && vect.getY () == 0)
		return;
	c[0] = point;
	if (ABS (vect.getX ()) < EPSILON) 
	{
		if (vect.getY () > 0)
			c[1].set (point.getX (), worldTop);
		else
			c[1].set (point.getX (), worldBottom);
		inDrawingArea = TRUE;
	} 
	else 
	{
		double d = vect.getY () / vect.getX ();
		if (ABS (d) < EPSILON) 
		{
			if (vect.getX () > 0)
				c[1].set (worldRight, point.getY ());
			else
				c[1].set (worldLeft, point.getY ());
			inDrawingArea = TRUE;
		} 
		else 
		{
			// ordonnee a l'origine
			double b = point.getY () - d * point.getX ();
			double y = d * worldLeft + b;
			if (y >= worldBottom && y < worldTop && point.getX () > worldLeft && vect.getX () < 0) 
			{
				c[1].set (worldLeft, y);
				inDrawingArea = TRUE;
				goto end_halfLine_externalClipping;
			}
			y = d * worldRight + b;
			if (y >= worldBottom && y < worldTop && point.getX () < worldRight && vect.getX () > 0) 
			{
				c[1].set (worldRight, y);
				inDrawingArea = TRUE;
				goto end_halfLine_externalClipping;
			}
			double x = (worldBottom - b) / d;
			if (x >= worldLeft && x < worldRight && point.getY () > worldBottom && vect.getY () < 0) 
			{
				c[1].set (x, worldBottom);
				inDrawingArea = TRUE;
				goto end_halfLine_externalClipping;
			}
			x = (worldTop - b) / d;
			if (x >= worldLeft && x < worldRight && point.getY () < worldTop && vect.getY () > 0) 
			{
				c[1].set (x, worldTop);
				inDrawingArea = TRUE;
				goto end_halfLine_externalClipping;
			}
		}
	}
      end_halfLine_externalClipping:
	if (inDrawingArea)
		drawSegment (style, c[0], c[1]);
}

void drgeoGtkDrawable::
drawSegment (drgeoStyle & style, drgeoPoint & start, drgeoPoint & end)
{
	int x1, y1, x2, y2;
	double width, height;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible and we are not in style mode
		return;
	}
	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);
	// Compute the correct on screen location.
	x1 = (int) (width / 2.0 - origin_x * scale + start.getX () * scale);
	y1 = (int) (height / 2.0 + origin_y * scale - start.getY () * scale);
	x2 = (int) (width / 2.0 - origin_x * scale + end.getX () * scale);
	y2 = (int) (height / 2.0 + origin_y * scale - end.getY () * scale);


	// Adapt the GC to the style.
	gdk_gc_set_foreground (figure_gc, &color[style.color]);
	switch (style.type) {
	case drgeoLineInvisible:
		gdk_gc_set_background (figure_gc, &color[drgeoColorYellow]);
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_DOUBLE_DASH,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineDashed:
		gdk_gc_set_background (figure_gc, &color[drgeoColorWhite]);
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_DOUBLE_DASH,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineNormal:
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineThick:
		gdk_gc_set_line_attributes (figure_gc, 2, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	}

	// Draw the line.
	gdk_draw_line (paintThere, figure_gc, x1, y1, x2, y2);
}

void drgeoGtkDrawable::
drawCircle (drgeoStyle & style, drgeoPoint & center, drgeoPoint & point)
{
	double radius;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible.
		return;
	}
	radius = (point - center).norm ();
	drawCircle (style, center, radius);
}

void drgeoGtkDrawable::
drawCircle (drgeoStyle & style, drgeoPoint & center, double radius)
{
	int x, y, iradius;
	double width, height;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible.
		return;
	}
	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);

	// Compute the correct on screen location.
	x = (int) (width / 2.0 - origin_x * scale + center.getX () * scale);
	y = (int) (height / 2.0 + origin_y * scale - center.getY () * scale);
	iradius = (int) (radius * scale);

	// Adapt the GC to the style.
	gdk_gc_set_foreground (figure_gc, &color[style.color]);

	switch (style.type) {
	case drgeoLineInvisible:
		gdk_gc_set_background (figure_gc, &color[drgeoColorYellow]);
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_DOUBLE_DASH,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineDashed:
		gdk_gc_set_background (figure_gc, &color[drgeoColorWhite]);
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_DOUBLE_DASH,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineNormal:
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineThick:
		gdk_gc_set_line_attributes (figure_gc, 2, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	}

	// Draw the arc.
	gdk_draw_arc (paintThere, figure_gc, style.fill,
	   x - iradius, y - iradius, iradius * 2, iradius * 2, 0, 360 * 64);
}

void drgeoGtkDrawable::
drawArc (drgeoStyle & style, drgeoPoint & center,
	 double radius, double start, double length)
{
	int x, y, iradius;
	double width, height;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible.
		return;
	}
	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);

	// Compute the correct on screen location.
	x = (int) (width / 2.0 - origin_x * scale + center.getX () * scale);
	y = (int) (height / 2.0 + origin_y * scale - center.getY () * scale);
	iradius = (int) (radius * scale);

	// Adapt the GC to the style.
	gdk_gc_set_foreground (figure_gc, &color[style.color]);

	switch (style.type) {
	case drgeoLineInvisible:
		gdk_gc_set_background (figure_gc, &color[drgeoColorYellow]);
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_DOUBLE_DASH,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineDashed:
		gdk_gc_set_background (figure_gc, &color[drgeoColorWhite]);
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_DOUBLE_DASH,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineNormal:
		gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	case drgeoLineThick:
		gdk_gc_set_line_attributes (figure_gc, 2, GDK_LINE_SOLID,
					    GDK_CAP_ROUND, GDK_JOIN_MITER);
		break;
	}

	// Draw the arc.
	gdk_draw_arc (paintThere, figure_gc, style.fill,
		      x - iradius, y - iradius, iradius * 2, iradius * 2,
		      (gint) (180.0 * start / M_PI) * 64,
		      (gint) (180.0 * length / M_PI) * 64);
}

void drgeoGtkDrawable::
drawText (drgeoPoint & where, char *text)
{
	int x, y;
	double width, height;

	if (!text)
		return;
	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);

	// Compute the correct on screen location.
	x = (int) (width / 2.0 - origin_x * scale + where.getX () * scale);
	y = (int) (height / 2.0 + origin_y * scale - where.getY () * scale);
	
	// Draw the string.
	gdk_draw_string (paintThere, font_text, font_gc,
			 x, y, text);
}

double drgeoGtkDrawable::
stringWidth (char *text)
{
	return pixelToWorld (gdk_string_width (font_text, text));
}

double drgeoGtkDrawable::
stringHeight (char *text)
{
	return pixelToWorld (gdk_string_height (font_text, text));
}

void drgeoGtkDrawable::
showTip (drgeoPoint & where, char *text)
{
	// The same thing as drawText for now.  XXX
	int x, y, h, w;
	double width, height;

	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);

	// Compute the correct on screen location.
	// FIXME: I've to use ascent and descent font attribute.
	// Using Gtk tooltips may be even more consistent.
	h = worldToPixel (stringHeight (text)) + 2;
	w = worldToPixel (stringWidth (text));

	x = (int) (width / 2.0 - origin_x * scale + where.getX () * scale + 16);
	y = (int) (height / 2.0 + origin_y * scale - where.getY () * scale - 16);

	gdk_gc_set_line_attributes (figure_gc, 0, GDK_LINE_SOLID,
				    GDK_CAP_ROUND, GDK_JOIN_MITER);
	gdk_gc_set_foreground (figure_gc, &color[drgeoColorYellow]);
	gdk_draw_rectangle (paintThere, figure_gc, TRUE, 
			    x - 2, y - h, w + 4, h + 4);
	gdk_gc_set_foreground (figure_gc, &color[drgeoColorBlack]);
	gdk_draw_rectangle (paintThere, figure_gc, FALSE, 
			    x - 2, y - h, w + 4, h + 4);

	// Draw the string.
	gdk_draw_string (paintThere, font_text, font_gc,
			 x, y, text);
}

void drgeoGtkDrawable::
setCursor (drgeoCursor cursor)
{
	GdkCursor * c;
	switch (cursor) 
	{
	case hand:
		c = gdk_cursor_new (GDK_HAND2);
		break;
	case arrow:
		c = gdk_cursor_new (GDK_LEFT_PTR);
		break;
	case pen:
		c = gdk_cursor_new (GDK_PENCIL);
		break;
	}
	gdk_window_set_cursor (drawing_area->window ,c);
	gdk_cursor_destroy (c);
}

double drgeoGtkDrawable::
getRange ()
{
	// 3 pixels on screen.
	return (4.0 / scale);
}

void drgeoGtkDrawable::
clear ()
{
  // Clear the whole drawable area.
  gint widthi, heighti;
  gdk_window_get_size ((drawing_area->window), &widthi, &heighti);
  gdk_draw_rectangle (paintThere, drawing_area->style->white_gc, TRUE, 
		      0, 0, widthi, heighti);
}

void drgeoGtkDrawable::
chooseItem (liste_elem * list)
{
	// This method is responsible for prompting the user to select an
	// item from a list and add it to the current selection.  It is
	// onvoked when there are several items under the mouse.
	GtkWidget *menu, *menu_item;
	int nb_choice, i;
	geometricObject *choice;

	menu = gtk_menu_new ();
	nb_choice = list->nb_elem;
	list->init_lire ();
	for (i = 0; i < nb_choice; i++) {
		choice = (geometricObject *) list->lire (0);
		menu_item = gtk_menu_item_new_with_label (choice->getTypeName ());
		gtk_menu_append (GTK_MENU (menu), menu_item);

		// The callback will be called with the choice pointer, so we
		// can know wich figure object was choosen.  The drawable is
		// passed to the signal handler.

		gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
				    GTK_SIGNAL_FUNC (choose_item_cb),
				    (gpointer) choice);
		gtk_object_set_data (GTK_OBJECT (menu_item), "figure", this);
		gtk_widget_show (menu_item);
	}
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
			1, 0);
}

static gint
choose_item_cb (GtkWidget * w, geometricObject * choice)
{
	drgeoDrawableUI *drawable;
	drgeoPoint where;

	drawable = (drgeoDrawableUI *) gtk_object_get_data (GTK_OBJECT (w), "figure");
	if (drawable != NULL) {
		drawable->handleChoice (choice);
	}
}

bool drgeoGtkDrawable::
askOkCancel (char *message)
{
	GtkWidget *dialog;
	int reply;

	// This method is used to prompt the user for a yes or no question.
	dialog = gnome_message_box_new (message,
					GNOME_MESSAGE_BOX_QUESTION,
					GNOME_STOCK_BUTTON_OK,
					GNOME_STOCK_BUTTON_CANCEL,
					NULL);

	gnome_dialog_set_default (GNOME_DIALOG (dialog), GNOME_CANCEL);
	gtk_widget_show (dialog);

	reply = gnome_dialog_run (GNOME_DIALOG (dialog));

	if (reply == GNOME_OK)
		return TRUE;
	else
		return FALSE;

}
void drgeoGtkDrawable::
adjustRulersRange ()
{
	double height, width;

	/* Compute width and height of the drawing_area.  */
	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);

	gtk_ruler_set_range (GTK_RULER (hrule),
			     origin_x - width / (scale * 2.0),
			     origin_x + width / (scale * 2.0),
			     origin_x,
			     origin_x + width / (scale * 2.0));
	gtk_ruler_set_range (GTK_RULER (vrule),
			     origin_y + height / (scale * 2.0),
			     origin_y - height / (scale * 2.0),
			     origin_y,
			     origin_y + height / (scale * 2.0));
}

GtkWidget *drgeoGtkDrawable::
getGtkWidget ()
{
	return (table);
}

void drgeoGtkDrawable::
pressCallback (GdkEventButton * event)
{
	drgeoPoint where;
	double width, height;

	// Only invoke the handlePress() method if the button1 is pressed.
	// We are ignoring all other press events.
	if (event->button == 1) {
		height = (double) WIDGET_HEIGHT (drawing_area);
		width = (double) WIDGET_WIDTH (drawing_area);

		// Compute the correct on screen location.
		where.set (origin_x + (event->x - width / 2.0) / scale ,
			   origin_y - (event->y - height / 2.0) / scale);
		// Handle correctly the event.
		handlePress (where);
	}
}

void drgeoGtkDrawable::
releaseCallback (GdkEventButton * event)
{
	drgeoPoint where;
	double width, height;

	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);

	// Compute the correct on screen location.
	where.set (origin_x + (event->x - width / 2.0) / scale,
		   origin_y - (event->y - height / 2.0) / scale);
	// Handle correctly the event.
	handleRelease (where);
}

void drgeoGtkDrawable::
moveCallback (GdkEventMotion * event)
{
	static GdkModifierType state;
	drgeoPoint where;
	double width, height;
	
	height = (double) WIDGET_HEIGHT (drawing_area);
	width = (double) WIDGET_WIDTH (drawing_area);

	gdk_window_get_pointer (drawing_area->window, NULL, NULL, &state);

	if ((state & GDK_BUTTON1_MASK) == GDK_BUTTON1_MASK)
	{
		gint widthi, heighti;
		gdk_window_get_size ((drawing_area->window), &widthi, &heighti);
		if (doubleBuffering == NULL)
		{
			/* 
			   The user starts to move the figure,
			   create a GdkPixmap to draw then use double buffering
			*/
			doubleBuffering = gdk_pixmap_new (drawing_area->window, widthi, heighti, -1);
			paintThere = doubleBuffering;
		}
		else 
		{
			/* 
			   We are in motion situation, update the drawing_area
			   with the GdkPixmap doubleBuffering
			*/
			gdk_draw_pixmap (drawing_area->window, drawing_area->style->black_gc,
					 doubleBuffering, 0, 0, 0, 0, widthi, heighti);
		}
		
	}
	else if (doubleBuffering != NULL)
	{
		/* 
		   The user just stops to move, destroy the GdkPixmap 
		   and adjust paintThere
		*/
		paintThere = drawing_area->window;
		gdk_pixmap_unref (doubleBuffering);
		doubleBuffering = NULL;
	}


	// Compute the correct on screen location.
	where.set (origin_x + (event->x - width / 2.0) / scale,
		   origin_y - (event->y - height / 2.0) / scale);
	// Handle correctly the event.
	handleMouseAt (where);
}

void drgeoGtkDrawable::
exposeCallback ()
{
	// Redraw everything.
	refresh ();
}

void drgeoGtkDrawable::
configureCallback ()
{
	if (paintThere == NULL)
		paintThere = drawing_area->window;
	if (figure_gc == NULL) {
		/* Create necessary GCs.  */
		figure_gc = gdk_gc_new (paintThere);
		font_gc = gdk_gc_new (paintThere);
		object_gc = gdk_gc_new (paintThere);

#define SET_COLOR(color, R,G,B) \
{ \
      color.pixel = 0; \
      color.red = R << 8; \
      color.green = G << 8; \
      color.blue = B << 8;  \
      gdk_color_alloc (gtk_widget_get_colormap(drawing_area), &(color)); \
}

		// Fill the color map. XXX: Add error handling.
		SET_COLOR (color[drgeoColorBlack], 0, 0, 0);
		SET_COLOR (color[drgeoColorWhite], 255, 255, 255);
		SET_COLOR (color[drgeoColorBlue], 80, 177, 255);
		SET_COLOR (color[drgeoColorDarkBlue], 45, 56, 255);
		SET_COLOR (color[drgeoColorGrey], 170, 170, 170);
		SET_COLOR (color[drgeoColorDarkGrey], 90, 90, 90);
		SET_COLOR (color[drgeoColorGreen], 0, 100, 0);
		SET_COLOR (color[drgeoColorDarkGreen], 0, 235, 0);
		SET_COLOR (color[drgeoColorRed], 235, 0, 0);
		SET_COLOR (color[drgeoColorBordeaux], 145, 0, 0);
		SET_COLOR (color[drgeoColorYellow], 255, 240, 33);
		SET_COLOR (color[drgeoColorOrange], 255, 153, 43);

		gdk_gc_set_foreground (font_gc, &color[drgeoColorBlack]);
		gdk_gc_set_foreground (figure_gc, &color[drgeoColorBlack]);
		gdk_gc_set_foreground (object_gc, &color[drgeoColorBlack]);

		gdk_window_set_background (paintThere,
					   &color[drgeoColorWhite]);

		/* Load appropriate fonts.  */
		font_tip = gdk_font_load ("-adobe-courier-medium-r-normal--12-120-75-75-m-70-iso8859-1");
		font_text = gdk_font_load ("-adobe-courier-medium-r-normal--12-120-75-75-m-70-iso8859-1");

		// Initialize the timer for regular flashes.
		timeoutTag = gtk_timeout_add (500, (GtkFunction) timer_cb, (gpointer) this);
	}
	adjustRulersRange ();
}

static int
timer_cb (drgeoGtkDrawable * drawable)
{
	// Invoke the timerCallBack() method.
	drawable->timerCallback ();
	return (TRUE);		// Make sure the CallBack will be
	// called again and again... 

}

void drgeoGtkDrawable::
timerCallback ()
{
	// Make the current selection blink.
	counter++;
	counter &= 1;
	if (figure && counter == 1) {
		figure->flashSelection (TRUE);
	} else if (figure && counter == 0) {
		figure->flashSelection (FALSE);
	}
}

void drgeoGtkDrawable::
horizontalScrollCallback ()
{
	origin_x = hadjustment->value;
	adjustRulersRange ();
	refresh ();
}

void drgeoGtkDrawable::
verticalScrollCallback ()
{
	origin_y = -vadjustment->value;
	adjustRulersRange ();
	refresh ();
}

void drgeoGtkDrawable::
setScale (double newScale)
{
	scale = newScale;
	adjustRulersRange ();
}

drgeoPoint drgeoGtkDrawable::
getAreaCenter ()
{
	drgeoPoint c;
	c.set (origin_x, origin_y);
	return c;
		
}

drgeoPoint drgeoGtkDrawable::
getAreaSize ()
{
	drgeoPoint s;
	s.set (pixelToWorld (WIDGET_WIDTH (drawing_area)),
	       (pixelToWorld (WIDGET_HEIGHT (drawing_area))));
	return s;
}

void drgeoGtkDrawable::
updateUndoState ()
{
	reconcile_grayout_undo ();
}



// Regular signal handlers.  You don't have to care about that.

static void
drawing_area_button_press_cb (GtkWidget * w, GdkEventButton * event,
			      gpointer data)
{
	drgeoGtkDrawable *drawable = NULL;

	drawable = (drgeoGtkDrawable *) gtk_object_get_data (GTK_OBJECT (w), "figure");
	drawable->pressCallback (event);
}

static void
drawing_area_button_release_cb (GtkWidget * w, GdkEventButton * event,
				gpointer data)
{
	drgeoGtkDrawable *drawable = NULL;

	drawable = (drgeoGtkDrawable *) gtk_object_get_data (GTK_OBJECT (w), "figure");
	drawable->releaseCallback (event);
}

static void
drawing_area_move_cb (GtkWidget * w, GdkEventMotion * event, gpointer data)
{
	drgeoGtkDrawable *drawable = NULL;

	drawable = (drgeoGtkDrawable *) gtk_object_get_data (GTK_OBJECT (w), "figure");				
	drawable->moveCallback (event);	
}

static void
drawing_area_expose_cb (GtkWidget * w, gpointer data)
{
	drgeoGtkDrawable *drawable = NULL;

	drawable = (drgeoGtkDrawable *) gtk_object_get_data (GTK_OBJECT (w), "figure");
	drawable->exposeCallback ();
}

static void
drawing_area_configure_cb (GtkWidget * w, gpointer data)
{
	drgeoGtkDrawable *drawable = NULL;

	drawable = (drgeoGtkDrawable *) gtk_object_get_data (GTK_OBJECT (w), "figure");
	drawable->configureCallback ();
}

static void
hadjustment_cb (GtkAdjustment * w, gpointer data)
{
	drgeoGtkDrawable *drawable = NULL;

	drawable = (drgeoGtkDrawable *) gtk_object_get_data (GTK_OBJECT (data), "figure");
	drawable->horizontalScrollCallback ();
}

static void
vadjustment_cb (GtkAdjustment * w, gpointer data)
{
	drgeoGtkDrawable *drawable = NULL;

	drawable = (drgeoGtkDrawable *) gtk_object_get_data (GTK_OBJECT (data), "figure");
	drawable->verticalScrollCallback ();
}

drgeoMacroBuildDialog *drgeoGtkDrawable::
createMacroBuildDialog (drgeoMacroBuilder * builder)
{
	class drgeoGtkMacroBuildDialog *dialog;

	this->builder = builder;
	// Create the dialog, and return a pointer to it.
	dialog = new drgeoGtkMacroBuildDialog (builder);
	dialog->show ();
	return dialog;
}

drgeoMacroPlayDialog *drgeoGtkDrawable::
createMacroPlayDialog (drgeoMacroPlayer * player)
{
	class drgeoGtkMacroPlayDialog *dialog;

	this->player = player;
	// Create the dialog, and return a pointer to it.
	dialog = new drgeoGtkMacroPlayDialog (player);
	dialog->show ();
	return dialog;
}

drgeoStyleDialog *drgeoGtkDrawable::
createStyleDialog ()
{
	class drgeoStyleDialog *dialog;

	// Create the dialog, and return a pointer to it.
	dialog = new drgeoGtkStyleDialog (this);
	return dialog;
}

void
dummy_cb (GtkWidget * w, gpointer data)
{
	printf ("Dummy callback\n");
}

void
on_zoom_valeur_changed (GtkWidget * w, gpointer data)
{
	/* FIXME: this code is not operable
	   see in drgeo_menu.c instead */
	gchar *zoom;
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	zoom = gtk_entry_get_text (GTK_ENTRY (w));
	if (!strcmp (zoom, "25%"))
		gtkfig->setScale (7.5);
	else if (!strcmp (zoom, "50%"))
		gtkfig->setScale (15.0);
	else if (!strcmp (zoom, "75%"))
		gtkfig->setScale (22.5);
	else if (!strcmp (zoom, "100%"))
		gtkfig->setScale (30.0);
	else if (!strcmp (zoom, "125%"))
		gtkfig->setScale (37.5);
	else if (!strcmp (zoom, "150%"))
		gtkfig->setScale (45.0);
	else if (!strcmp (zoom, "175%"))
		gtkfig->setScale (52.5);
	else if (!strcmp (zoom, "200%"))
		gtkfig->setScale (60.0);

	gtkfig->refresh ();
}
