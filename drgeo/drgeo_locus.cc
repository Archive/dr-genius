/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  2000
 * hilaire@seul.org 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"
#include "drgeo_locus.h"
#include "drgeo_point.h"
#include "drgeo_drawable.h"
#include "traite.h"   /* FIXME : move the method updateItems as a static method in drgeo_locus.* */


enum locusState {
  onMax,
  onMin,
  normal
};

locus::
locus (liste_elem& parents,gboolean createdFromMacro, 
       liste_elem *figureList):
	geometricObject (createdFromMacro, figureList)
{
	category = LOCUS;
	/* FREE_POINT_ON_CURVE - CONSTRAINED_POINT */
	parents.init_lire ();
	parentList = g_list_append (parentList,
	      (gpointer) searchForCategory (parents, FREE_PT_ON_CURVE));
	parents.init_lire ();
	parentList = g_list_append (parentList,
	      (gpointer) searchForCategory (parents, CONSTRAINED_PT));	

	initName ();
	update ();
}
		
locus::
locus (xmlNodePtr tree, GHashTable *itemIdToAdress, liste_elem *figureList):
	geometricObject (tree, figureList)
{
	xmlNodePtr item;
	void *obj;
	category = LOCUS;
	/* get the parent ref */
	item = xml_search_next (tree->childs, "parent");
	while (item)
	  {
	    if ((obj = xmlInsertParent (item, itemIdToAdress)) == NULL)
	      exit (1); /* FIXME: implement a nicer way */
	    parentList = g_list_append (parentList, obj); 
	    item = xml_search_next (item->next, "parent");
	  }
	update ();
	initName ();
}
  
void locus::
draw (drgeoDrawable& area, char force)
{
	gint n;
	if ((mask == alway) || (mask == yes && force == FALSE)
	    || !exist)
		return;	
	for (n = 0; n < sampleNumber - 1; n++)	
	  area.drawSegment (style, p[n], p[n + 1]);
}

void locus::
update ()
{
	gint n, locusIndex, freePointIndex;
	gdouble x, x0, step, step0, stepMin, stepMax, d;
	locusState state;
	point *free, *constrained;
	exist = parentExist ();
	if (!exist)
		return;
	free = (point *) getFirst;
	constrained = (point *) getLast;
	x0 = free->getAbscissa ();
	step0 = 1.0 / LOCUS_SAMPLE;
	step =  step0;
	stepMin = step / (2 * 2 * 2);
	stepMax = step * 2 * 2 * 2 * 2 * 2;
	//locusIndex = figureList->position ((void *) this);
	locusIndex = figureList->position ((void *) constrained);
	freePointIndex = figureList->position ((void *) free);
	
	/* search for the first point valide and not off screen*/
	/* FIXME: offsereen criteria should depend on the actual 
	   display but update doesn't receive any
	   information about that */
	x = 0;
	do {
	  free->setAbscissa (x);
	  updateItems (figureList, freePointIndex, locusIndex, FALSE);
	  x += stepMin;
	  p[0] = constrained->getCoordinate ();
	} while (!constrained->objectExist () || ( x < 1. && p[0].squareNorm () > 30000.));

	if (x >= 1)
	  {
	    /* the locus doesn't exist */
	    exist = FALSE;
	    free->setAbscissa (x0);
	    updateItems (figureList, freePointIndex, locusIndex, FALSE);
	    return;
	  }
	for (n = 1 , state = normal; n < LOCUS_SAMPLE && x <= 1; n++, x += step)
	{
		free->setAbscissa (x);
		updateItems (figureList, freePointIndex, locusIndex, FALSE);
		if (!constrained->objectExist ())
		  {
		    n--;
		    if (step > stepMin)
		    {
			    x -= step;
			    step /= 2;
			    state = onMax;
		    }
		    else
			    state = normal;
		    continue;
		  }
		p[n] = constrained->getCoordinate ();
		if (p[n].squareNorm () > 30000.)
		  {
		    /* point off screen, we don't want it */
		    n--;
		    state = normal;
		  }
		else if ( (p[n-1] - p[n]).squareNorm () > .4 && step > stepMin)
		  {
		    x -= step;
		    n--;
		    step /= 2;		    
		    state = onMax;
		  }
		else if ( (p[n-1] - p[n]).squareNorm () < .08 && step < stepMax && state != onMax)
		  {
		    x -= step;
		    n--;
		    step *= 2;
		    state = onMin;
		  }
		else
		  {
		    /* not rejected so we take it */
		    state = normal;
		  }
	}
	sampleNumber = n;
	free->setAbscissa (x0);
	updateItems (figureList, freePointIndex, locusIndex, FALSE);
}

gboolean locus::
overObject (drgeoPoint& mouse, gdouble range)
{
	gint n;
	for (n = 0; n < LOCUS_SAMPLE; n++)
		if ((p[n] - mouse).norm () < range)
			return TRUE;
	return FALSE;
}

void locus::
move (drgeoVector& t)
{
	/* FIXME: we can eventually move the curve to which
	   beong the FREE_POINT_ON_CURVE point.
	*/
}

void locus::
initName ()
{
	gchar vide[1]={0};
	point *free, *constrained;
	geometricObject *curve;
	int l;
	if (mask == alway)
		return;

	g_free (typeName);

	free = (point *) getFirst;
	constrained = (point *) getLast;
	curve = (geometricObject *) 
		(g_list_first (free->getParentList ())->data);

	/* compute the string length */
	l = strlen (_("%1's locus when %2 describes %3")) + 1;
	if (constrained->getName ()?strlen (constrained->getName()):0)
		l += strlen (constrained->getName ());
	if (free->getName ()?strlen (free->getName()):0)
		l += strlen (free->getName ());
	if (curve->getName ()?strlen (curve->getName()):0)
		l += strlen (curve->getName ());

	typeName = (gchar *) g_malloc (l);
	strcpy (typeName, _("%1's locus when %2 describes %3"));
	/* insert the names when they exist*/
	if (constrained->getName ())
		strinsmsg (typeName, constrained->getName (), "%1");
	else
		strinsmsg (typeName, vide, "%1");
	if (free->getName ())
		strinsmsg (typeName, free->getName (), "%2");
	else
		strinsmsg (typeName, vide, "%2");
	if (curve->getName ())
		strinsmsg (typeName, curve->getName (), "%3");
	else
		strinsmsg (typeName, vide, "%3");
}

void locus::
save (xmlNodePtr tree, liste_elem& figureList)
{
	xmlNodePtr item;
	GList *parent;

	parent = g_list_first (parentList);
	item = xmlNewChild (tree, NULL, BAD_CAST "locus", NULL);
	save_attribute (item, this, "None");

	while (parent)
	{
		xmlAddParent (item, (geometricObject *)parent->data);
		parent = g_list_next (parent);
	}	
}
