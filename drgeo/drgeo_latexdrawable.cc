/*
 *  Dr Genius an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-2000
 * hilaire@seul.org 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "couleur.h"
#include "drgeo_drgeoStyle.h"
#include "drgeo_latexdrawable.h"

static gchar *latexPointSize[4]=
{"linestyle=dotted,", "dotscale=1.5,","","dotscale=2.5,"};

static gchar *latexPointFilled[3]=
{"dotstyle=*,","","dotstyle=square*,"};

static gchar *latexPointEmpty[3]=
{"dotstyle=o,","","dotstyle=square,"};

static gchar *latexLineType[4]=
{"linestyle=dotted,","linestyle=dashed,","","linewidth=0.05,"};

static gchar *latexColor[DRGEO_NUMBER_COLOR] =
{"linecolor=black", "linecolor=darkgrey", "linecolor=gray", "linecolor=white",
 "linecolor=green", "linecolor=green", "linecolor=blue", "linecolor=blue",
 "linecolor=red", "linecolor=red", "linecolor=yellow", "linecolor=yellow"};

drgeoLatexDrawable::
drgeoLatexDrawable (drgeoFigure *figure, FILE *fileHandle, drgeoPoint origin, 
		    drgeoPoint size, gdouble scale)
{
	this->figure = figure;
	this->fileHandle = fileHandle;
	this->origin = origin;
	this->size = size;
	this->scale = scale;
	fprintf (fileHandle, "\\begin{pspicture*}(0,0)(%f,%f)\n",
		 size.getX(), size.getY());
	/* set the default color and shape of the object */
	/* point are by defaut red cross */
	fprintf (fileHandle, "\\psset{dotstyle=x, dotscale=2.0, linewidth=0.02}\n");
	/* draw the frame where will be located the figure */
	fprintf (fileHandle, "\\psframe(0,0)(%f,%f)\n",
		 size.getX(), size.getY());
}

void drgeoLatexDrawable::
drawPoint (drgeoStyle & style, drgeoPoint & point)
{
	char mode;
	drgeoPoint p;
	
	mode = this->getFigure()->getMode();
	if (style.type == drgeoLineInvisible 
	    && mode != MISE_EN_FORME_MODE)
		return;

	p = areaToLatex (point);
	if (style.fill)
		fprintf (fileHandle, 
			 "\\psdots[%s%s%s](%f,%f)\n",
			 latexPointSize[style.type],
			 latexPointFilled[style.pointShape],
			 latexColor[style.color],
			 p.getX(), p.getY());
	else
		fprintf (fileHandle, 
			 "\\psdots[%s%s%s](%f,%f)\n",
			 latexPointSize[style.type],
			 latexPointEmpty[style.pointShape],
			 latexColor[style.color],
			 p.getX(), p.getY());
}

void drgeoLatexDrawable::
drawLine (drgeoStyle & style, drgeoPoint & start, drgeoPoint & end)
{
	// start : one point on the line
	// end : a direction vector of the line
	static drgeoPoint m, c[2];
	char pt = 0;
	// world boundaries
	double worldLeft, worldTop, worldRight, worldBottom, d;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible and we are not in style mode
		return;
	}
	worldLeft = origin.getX() - size.getX() / 2;
	worldRight = origin.getX() + size.getX() / 2;
	worldTop = origin.getY() + size.getY() / 2;
	worldBottom = origin.getY() - size.getX() / 2;

	if (end.getX () == 0 && end.getY () == 0)
		return;

	// m a point on the line
	m = start + 10 * end;
	if (ABS (start.getX () - m.getX ()) < EPSILON) 
	{
		c[0].set (start.getX (), worldTop);
		c[1].set (start.getX (), worldBottom);
		pt = 2;
	} 
	else 
	{
		// line slope
		d = end.getY () / end.getX ();
		if (ABS (d) < EPSILON) 
		{
			c[0].set (worldLeft, start.getY ());
			c[1].set (worldRight, start.getY ());
			pt = 2;
		} 
		else 
		{
			// ordonnee a l'origine
			double b = start.getY () - d * start.getX ();
			double y = d * worldLeft + b;
			if (y >= worldBottom && y < worldTop) 
			{
				c[pt++].set (worldLeft, y);
			}
			y = d * worldRight + b;
			if (y >= worldBottom && y < worldTop) 
			{
				c[pt++].set (worldRight, y);
			}
			double x = (worldTop - b) / d;
			if (x >= worldLeft && x < worldRight && pt < 2) 
			{
				c[pt++].set (x, worldTop);
			}
			x = (worldBottom - b) / d;
			if (x >= worldLeft && x < worldRight && pt < 2) {
				c[pt++].set (x, worldBottom);
			}
		}
	}
	if (pt == 2)
		drawSegment (style, c[0], c[1]);
}

void drgeoLatexDrawable::
drawHalfLine (drgeoStyle & style, drgeoPoint & start, 
	      drgeoVector & vect)
{
	static drgeoPoint c[2];
	char inDrawingArea = FALSE;
	double worldLeft, worldTop, worldRight, worldBottom;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible and we are not in style mode
		return;
	}
	worldLeft = origin.getX() - size.getX() / 2;
	worldRight = origin.getX() + size.getX() / 2;
	worldTop = origin.getY() + size.getY() / 2;
	worldBottom = origin.getY() - size.getY() / 2;

	if (vect.getX () == 0 && vect.getY () == 0)
		return;
	c[0] = start;
	if (ABS (vect.getX ()) < EPSILON) 
	{
		if (vect.getY () > 0)
			c[1].set (start.getX (), worldTop);
		else
			c[1].set (start.getX (), worldBottom);
		inDrawingArea = TRUE;
	} 
	else 
	{
		double d = vect.getY () / vect.getX ();
		if (ABS (d) < EPSILON) 
		{
			if (vect.getX () > 0)
				c[1].set (worldRight, start.getY ());
			else
				c[1].set (worldLeft, start.getY ());
			inDrawingArea = TRUE;
		} 
		else 
		{
			// ordonnee a l'origine
			double b = start.getY () - d * start.getX ();
			double y = d * worldLeft + b;
			if (y >= worldBottom && y < worldTop && start.getX () > worldLeft && vect.getX () < 0) 
			{
				c[1].set (worldLeft, y);
				inDrawingArea = TRUE;
				goto end_halfLine_externalClipping;
			}
			y = d * worldRight + b;
			if (y >= worldBottom && y < worldTop && start.getX () < worldRight && vect.getX () > 0) 
			{
				c[1].set (worldRight, y);
				inDrawingArea = TRUE;
				goto end_halfLine_externalClipping;
			}
			double x = (worldBottom - b) / d;
			if (x >= worldLeft && x < worldRight && start.getY () > worldBottom && vect.getY () < 0) 
			{
				c[1].set (x, worldBottom);
				inDrawingArea = TRUE;
				goto end_halfLine_externalClipping;
			}
			x = (worldTop - b) / d;
			if (x >= worldLeft && x < worldRight && start.getY () < worldTop && vect.getY () > 0) 
			{
				c[1].set (x, worldTop);
				inDrawingArea = TRUE;
				goto end_halfLine_externalClipping;
			}
		}
	}
      end_halfLine_externalClipping:
	if (inDrawingArea)
		drawSegment (style, c[0], c[1]);
}

void drgeoLatexDrawable::
drawSegment (drgeoStyle & style, drgeoPoint & start, drgeoPoint & end)
{
	drgeoPoint p1, p2;
	char mode;
	
	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible and we are not in style mode
		return;
	}
	p2 = areaToLatex (end);
	p1 = areaToLatex (start);
	fprintf (fileHandle,
		 "\\psline[%s%s](%f,%f)(%f,%f)\n",
		 latexLineType[style.type], latexColor[style.color],
		 p1.getX(), p1.getY(),p2.getX(), p2.getY());
}

void drgeoLatexDrawable::
drawCircle (drgeoStyle & style, drgeoPoint & center, drgeoPoint & point)
{
	double radius;
	char mode;

	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible.
		return;
	}
	radius = (point - center).norm ();
	drawCircle (style, center, radius);
}

void drgeoLatexDrawable::
drawCircle (drgeoStyle & style, drgeoPoint & center, double radius)
{
	drgeoPoint p;
	char mode;
	
	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible and we are not in style mode
		return;
	}
	p = areaToLatex (center);
	fprintf (fileHandle, "\\pscircle[%s%s](%f,%f){%f}",
		 latexLineType[style.type], latexColor[style.color],
		 p.getX(), p.getY(),radius);
}

void drgeoLatexDrawable::
drawArc (drgeoStyle & style, drgeoPoint & center, double radius,
	 double start, double length)
{
	drgeoPoint p;
	char mode;
	
	mode = this->getFigure ()->getMode ();

	if (style.type == drgeoLineInvisible && mode != MISE_EN_FORME_MODE) {
		// Do nothing if the line is invisible and we are not in style mode
		return;
	}
	p = areaToLatex (center);
	if (length > 0)
		fprintf (fileHandle, "\\psarc[%s%s](%f,%f){%f}{%f}{%f}",
			 latexLineType[style.type], latexColor[style.color],
			 p.getX(), p.getY(),radius, 
			 start * 180.0 / M_PI, (start + length) * 180.0 / M_PI);
	else
		fprintf (fileHandle, "\\psarc[%s%s](%f,%f){%f}{%f}{%f}",
			 latexLineType[style.type], latexColor[style.color],
			 p.getX(), p.getY(),radius, 
			 (start + length) * 180.0 / M_PI, start * 180.0 / M_PI);

}
	
void drgeoLatexDrawable::
drawText (drgeoPoint & where, char *text)
{
	drgeoPoint p;
	
	if (!text)
		return;
	p = areaToLatex (where);
	if (p.getX() <= 0. || p.getY () <= 0.
	    || p.getX() > size.getX() || p.getY() > size.getY())
		return;
	fprintf (fileHandle, "\\rput[bl]{0}(%f,%f){%s}\n",
		 p.getX() + 0.1, p.getY() + 0.1, text);
}

drgeoPoint drgeoLatexDrawable::
getAreaCenter ()
{
	return origin;
}

drgeoPoint drgeoLatexDrawable::
getAreaSize ()
{
	return size;
}


double drgeoLatexDrawable::
pixelToWorld (int pixels)
{
	return (double) (((double) pixels) / (50.0 * scale));
}

int drgeoLatexDrawable::
worldToPixel (double world)
{
	return (int) (world * scale * 50);
}


/* private */
drgeoPoint drgeoLatexDrawable::
areaToLatex (drgeoPoint p)
{
	return  p + size / 2 - origin;
}



