/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-1999
 * hilaire.fernandes@iname.com 
 * 
 * This code is copyright Laurent Gauthier 1999
 * lolo@seul.org
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef DRGEO_GTKDRAWABLE_H
#define DRGEO_GTKDRAWABLE_H

#include "config.h"
#include <gnome.h>
#include "drgeo_drawable.h"
#include "drgeo_gtkmacro.h"

// Signal handler for the widget build from XML data
void on_zoom_valeur_changed (GtkWidget * w, gpointer data);


// Implementation of Gtk drawable.
class drgeoGtkDrawable:public drgeoDrawableUI {
	public:
	drgeoGtkDrawable (drgeoFigure *figure);		
	~drgeoGtkDrawable ();
	// These are the concrete implementation of the
	void drawPoint (drgeoStyle & style, drgeoPoint & point);
	void drawLine (drgeoStyle & style, drgeoPoint & start, drgeoPoint & end);
	void drawHalfLine (drgeoStyle & style, drgeoPoint & start, drgeoVector & vect);
	void drawSegment (drgeoStyle & style, drgeoPoint & start, drgeoPoint & end);
	void drawCircle (drgeoStyle & style, drgeoPoint & center, double radius);
	void drawCircle (drgeoStyle & style, drgeoPoint & center, drgeoPoint & point);
	void drawArc (drgeoStyle & style, drgeoPoint & center,
		      double radius, double start, double length);
	void drawText (drgeoPoint & where, char *text);
	double stringWidth (char *text);
	double stringHeight (char *text);

	void showTip (drgeoPoint & where, char *text);
	void setCursor (drgeoCursor cursor);
	double getRange ();

	void clear ();
	void chooseItem (liste_elem * list);
	bool askOkCancel (char *message);
	class drgeoMacroBuildDialog *
	  createMacroBuildDialog (drgeoMacroBuilder * builder);
	class drgeoMacroPlayDialog *
	  createMacroPlayDialog (drgeoMacroPlayer * player);
	class drgeoStyleDialog *createStyleDialog ();

	// Interface specific to the Gtk interface.  This methods are to be
	// invoked only by the GUI code, and in particular the Gtk signal
	// handlers.  Consider these as private.
	GtkWidget *getGtkWidget ();
	void adjustRulersRange ();
	void exposeCallback ();
	void configureCallback ();
	void pressCallback (GdkEventButton * event);
	void releaseCallback (GdkEventButton * event);
	void moveCallback (GdkEventMotion * event);
	void horizontalScrollCallback ();
	void verticalScrollCallback ();
	void timerCallback ();
	void setScale (double newScale);
	drgeoPoint getAreaCenter ();
	drgeoPoint getAreaSize ();
	void updateUndoState ();

	GdkColor color[drgeoColorNumber];

 private:
	friend class drgeoGtkStyleDialog;

	GdkGC *figure_gc, *font_gc, *object_gc;
	GdkFont *font_tip, *font_text;
	GtkWidget *table, *drawing_area, *hrule, *vrule;
	GdkPixmap *doubleBuffering, *paintThere;

	GtkAdjustment *hadjustment, *vadjustment;
	double origin_x, origin_y;	
        // Coordinates of the point located at
	// the center of the drawable (not in
	// pixels, but in world units).

	gint timeoutTag;
	int counter;
};

#endif
