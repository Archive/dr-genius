#include <string.h>

#include "drgeo_tool.h"


// -------------------------------------------------
// Implementation for the Null tool, this tool does nothing.

void drgeoNullTool::
initialize (drgeoDrawableUI * drawable)
{
	// Do nothing.
	printf ("initialize()\n");
}

void drgeoNullTool::
handlePress (drgeoDrawableUI * drawable,
	     drgeoPoint & where)
{
	// Do nothing.
	printf ("handlePress (%lf,%lf)\n", where.getX (), where.getY ());
}

void drgeoNullTool::
handleRelease (drgeoDrawableUI * drawable,
	       drgeoPoint & where)
{
	// Do nothing.
	printf ("handleRelease (%lf,%lf)\n", where.getX (), where.getY ());
}

void drgeoNullTool::
handleMouseAt (drgeoDrawableUI * drawable,
	       drgeoPoint & where)
{
	// Do nothing.
	printf ("handleMouseAt (%lf,%lf)\n", where.getX (), where.getY ());
}

void drgeoNullTool::
handleChoice (drgeoDrawableUI * drawable,
	      geometricObject * item)
{
	// Do nothing.
	printf ("HandleChoice()\n");
}

void drgeoNullTool::
finish (drgeoDrawableUI * drawable)
{
	// Do nothing.
	printf ("finish()\n");
}

// -------------------------------------------------
// Implementation for the Select tool.

void drgeoSelectTool::
initialize (drgeoDrawableUI * drawable)
{
	// This code is invoked when the tool selected.
	state = drgeoStateNeutral;
	tipOn = FALSE;
}

void drgeoSelectTool::
handlePress (drgeoDrawableUI * drawable,
	     drgeoPoint & where)
{
	liste_elem *list;
	int i;

	// Do nothing if no figure is currently associated with this drawable.
	if (drawable->figure != NULL) {
		if (tipOn) {
			drawable->refresh ();
			tipOn = FALSE;
		}
		switch (state) {
		case drgeoStateItemChoice:
			// The user has not chosen any item, so revert to neutral
			// state.
			state = drgeoStateNeutral;
			// Fall through!  This is really what is wanted here.
		case drgeoStateNeutral:
			// The mouse button is being pressed, check if there are items
			// near the mouse.
			list = drawable->figure->mouseSelection (where);
			list->init_lire ();
			if (list->nb_elem == 1) {
				// Only one item is selected.
				state = drgeoStateItemGrabbed;
				handleChoice (drawable, (geometricObject *) list->lire (0));
				last = where;
			} else if (list->nb_elem > 1) {
				// Several items are selected at once, we must prompt the
				// user for a choice.  When the user is done with his
				// choice the handleChoice method will be invoked.
				state = drgeoStateItemChoice;
				last = where;
				drawable->chooseItem (list);
			}
			else
				state = drgeoStateNeutral;
			break;
		case drgeoStateItemSelected:
			// The mouse button is being pressed, check if the last
			// selected item is near the mouse.
			list = drawable->figure->mouseSelection (where);
			list->init_lire ();
			if (list->nb_elem >= 1) {
				// Check each item under the mouse to see if the last item
				// selected is there.  If so select it, if not go back to
				// neutral state.
				for (i = 0; i < list->nb_elem; i++) {
					if (this->item == (geometricObject *) list->lire (0)) {
						state = drgeoStateItemGrabbed;
						// The following is not necessary because it has
						// already been performed, when the item has been
						// selected among multiple items.
						// handleChoice (drawable, this->item);
						last = where;
						break;	// Get out of the loop!  We found it.

					}
				}
				if (state == drgeoStateItemSelected) {
					// The item last selected is not under the mouse at
					// this point, so we are back to neutral state.
					state = drgeoStateNeutral;
				}
			} else {
				// No item under the mouse go back to the Neutral state.
				state = drgeoStateNeutral;
			}
			break;
		default:
			// If we get there, then there's a problem with the event
			// handling code... XXX
			break;
		}
	}
}

void drgeoSelectTool::
handleRelease (drgeoDrawableUI * drawable,
	       drgeoPoint & where)
{
	// Do nothing if no figure is currently associated with this drawable.
	if (drawable->figure != NULL) {
		liste_elem *list;
		geometricObject *item;

		if (tipOn) {
			drawable->refresh ();
			tipOn = FALSE;
		}
		switch (state) {
		case drgeoStateItemChoice:
		case drgeoStateItemSelected:
			// The user has not chosen any item, so revert to neutral
			// state.
			state = drgeoStateNeutral;
			// Fall through!  This is really what is wanted here.
		case drgeoStateNeutral:
			// The mouse button is just being released with nothing being
			// dragged, so we only show a tip displaying the full name of
			// the nearest item.
			list = drawable->figure->mouseSelection (where);
			list->init_lire ();
			if (list->nb_elem == 1) {
				item = (geometricObject *) list->lire (0);
				drawable->showTip (where, item->getTypeName ());
				tipOn = TRUE;
			} else if (list->nb_elem > 1) {
				// There are several items under the mouse.
				drawable->showTip (where, "???");
				tipOn = TRUE;
			}
			break;
		case drgeoStateItemGrabbed:
			// The user is just releasing the mouse.  Reset the state to
			// neutral and that's all.
			state = drgeoStateNeutral;
			break;
		case drgeoStateItemDragged:
			// The drag is finished, so we just drop the selected ietms
			// where the mouse currently is.
			state = drgeoStateNeutral;
			drawable->figure->moveItem (NULL, where - start);
			drawable->figure->dropSelection (last, where);
			/* record the drag path */
			drawable->refresh ();
			break;
		default:
			// If we get there, then there's a problem with the event
			// handling code... XXX
			break;
		}
	}
}

void drgeoSelectTool::
handleMouseAt (drgeoDrawableUI * drawable,
	       drgeoPoint & where)
{
	int i;

	// Do nothing if no figure is currently associated with this drawable.
	if (drawable->figure != NULL) {
		liste_elem *list;
		geometricObject *item;

		if (tipOn) {
			drawable->refresh ();
			tipOn = FALSE;
		}
		switch (state) {
		case drgeoStateItemChoice:
			// The user has not chosen any item, so revert to neutral
			// state.
			state = drgeoStateNeutral;
			// Fall through!  This is really what is wanted here.
		case drgeoStateNeutral:
			// The mouse is just moving over the figure, so we just query
			// the figure to know which items are found near the current
			// mouse position.  And a tooltip is shown displaying the full
			// name of the nearest item.
			list = drawable->figure->mouseSelection (where);
			list->init_lire ();
			if (list->nb_elem == 1) {
				item = (geometricObject *) list->lire (0);
				drawable->showTip (where, item->getTypeName ());
				tipOn = TRUE;
				drawable->setCursor (hand);
			} else if (list->nb_elem > 1) {
				// There are several items under the mouse.
				drawable->showTip (where, "???");
				tipOn = TRUE;
				drawable->setCursor (hand);
			}
			else drawable->setCursor (arrow);

			break;
		case drgeoStateItemSelected:
			// The mouse is moving over the figure, check if we are moving
			// away from the spot where the last item was selected.  If so
			// go back to neutral state.
			if (away
			    && (spot - where).squareNorm () > (last - spot).squareNorm () 
					   + 4.0 * drawable->getRange ()) {
				state = drgeoStateNeutral;
			} else {
				// If the last selected item is under the mouse, then show
				// a tip about it.
				list = drawable->figure->mouseSelection (where);
				list->init_lire ();
				if (list->nb_elem >= 1) {
					// Check each item under the mouse to see if the last item
					// selected is there.  If so show its tip.
					for (i = 0; i < list->nb_elem; i++) {
						if (this->item == (geometricObject *) list->lire (0)) {
							drawable->showTip (where, this->item->getTypeName ());
							tipOn = TRUE;
							break;	// Get out of the loop!  We found it.

						}
					}
				}
				away = TRUE;
				last = where;
			}
			break;
		case drgeoStateItemGrabbed:
			// The user is just starting a mouse move with an item
			// grabbed, this means a drag is starting.  Notify the
			// figure.
			state = drgeoStateItemDragged;
			drawable->figure->dragSelection (last, where);
			drawable->refresh ();
			last = where;
			start = where;
			break;
		case drgeoStateItemDragged:
			// The drag is going on.
			drawable->figure->dragSelection (last, where);
			drawable->refresh ();
			last = where;
			break;
		default:
			// If we get there, then there's a problem with the event
			// handling code... XXX
			break;
		}
	}
}

void drgeoSelectTool::
handleChoice (drgeoDrawableUI * drawable,
	      geometricObject * item)
{
	drgeoPoint xxx;

	if (drawable->figure) {
		if (state == drgeoStateItemChoice) {
			state = drgeoStateItemSelected;
			this->item = item;	// Keep track of the last item
			// selected for later reference.

			away = FALSE;
			spot = last;
		}
		drawable->figure->clearSelection ();
		drawable->figure->addToSelection (xxx, item);
	}
}

void drgeoSelectTool::
finish (drgeoDrawableUI * drawable)
{
	// This code is invoked when the tool is deselected.
}

// -------------------------------------------------
// Implementation for the Build tool.

void drgeoBuildTool::
initialize (drgeoDrawableUI * drawable)
{
	// This code is invoked when the tool selected.
	state = drgeoStateNeutral;
	tipOn = FALSE;
}

void drgeoBuildTool::
handlePress (drgeoDrawableUI * drawable,
	     drgeoPoint & where)
{
	liste_elem *list;
	int i;

	// Do nothing if no figure is currently associated with this drawable.
	if (drawable->figure != NULL) {
		if (tipOn) {
			drawable->refresh ();
			tipOn = FALSE;
		}
		switch (state) {
		case drgeoStateItemChoice:
			// The user has not chosen any item, so revert to neutral
			// state.
			state = drgeoStateNeutral;
			// Fall through!  This is really what is wanted here.
		case drgeoStateNeutral:
			// The mouse button is being pressed, check if there are items
			// near the mouse.
			list = drawable->figure->mouseSelection (where);
			list->init_lire ();
			if (list->nb_elem == 0) {
				// no item was selected.  Just invoke handleChoice with a
				// NULL parameter that will be transmitted to
				// figure->addToSelection() for appropriate processing.
				last = where;
				handleChoice (drawable, NULL);
			} else if (list->nb_elem == 1) {
				// Only one item is selected, add it to the selection.
				last = where;
				handleChoice (drawable, (geometricObject *) list->lire (0));
			} else if (list->nb_elem > 1) {
				// Several items are selected at once, we must prompt the
				// user for a choice.  When the user is done with his
				// choice the handleChoice method will be invoked.
				state = drgeoStateItemChoice;
				last = where;
				drawable->chooseItem (list);
			}
			break;
		default:
			// If we get there, then there's a problem with the event
			// handling code... XXX
			break;
		}
	}
}

void drgeoBuildTool::
handleRelease (drgeoDrawableUI * drawable,
	       drgeoPoint & where)
{
	// Do nothing if no figure is currently associated with this drawable.
	if (drawable->figure != NULL) {
		liste_elem *list;
		geometricObject *item;

		if (tipOn) {
			drawable->refresh ();
			tipOn = FALSE;
		}
		switch (state) {
		case drgeoStateItemChoice:
			// The user has not chosen any item, so revert to neutral
			// state.
			state = drgeoStateNeutral;
			// Fall through!  This is really what is wanted here.
		case drgeoStateNeutral:
			// The mouse button is just being released with nothing being
			// dragged, so we only show a tip displaying the full name of
			// the nearest item.
			list = drawable->figure->mouseSelection (where);
			list->init_lire ();
			if (list->nb_elem == 1) {
				item = (geometricObject *) list->lire (0);
				drawable->showTip (where, item->getTypeName ());
				tipOn = TRUE;
			} else if (list->nb_elem > 1) {
				// There are several items under the mouse.
				drawable->showTip (where, "???");
				tipOn = TRUE;
			}
			break;
		default:
			// If we get there, then there's a problem with the event
			// handling code... XXX
			break;
		}
	}
}

void drgeoBuildTool::
handleMouseAt (drgeoDrawableUI * drawable,
	       drgeoPoint & where)
{
	int i;

	// Do nothing if no figure is currently associated with this drawable.
	if (drawable->figure != NULL) {
		liste_elem *list;
		geometricObject *item;

		if (tipOn) {
			drawable->refresh ();
			tipOn = FALSE;
		}
		switch (state) {
		case drgeoStateItemChoice:
			// The user has not chosen any item, so revert to neutral
			// state.
			state = drgeoStateNeutral;
			// Fall through!  This is really what is wanted here.
		case drgeoStateNeutral:
			// The mouse is just moving over the figure, so we just query
			// the figure to know which items are found near the current
			// mouse position.  And a tooltip is shown displaying the full
			// name of the nearest item.
			list = drawable->figure->mouseSelection (where);
			list->init_lire ();
			if (list->nb_elem == 1) {
				item = (geometricObject *) list->lire (0);
				drawable->showTip (where, item->getTypeName ());
				tipOn = TRUE;
				drawable->setCursor (pen);
			} else if (list->nb_elem > 1) {
				// There are several items under the mouse.
				drawable->showTip (where, "???");
				tipOn = TRUE;
				drawable->setCursor (pen);
			}
			else drawable->setCursor (arrow);

			break;
		default:
			// If we get there, then there's a problem with the event
			// handling code... XXX
			break;
		}
	}
}

void drgeoBuildTool::
handleChoice (drgeoDrawableUI * drawable,
	      geometricObject * item)
{
	if (drawable->figure) {
		// Add the selected item to the current figure's selection.
		drawable->figure->addToSelection (last, item);
	}
}

void drgeoBuildTool::
finish (drgeoDrawableUI * drawable)
{
	// This code is invoked when the tool is deselected.
}



// -----------------------------------------------------
// Macro Build Tool, used for macro creation.
// -----------------------------------------------------

void drgeoMacroBuildTool::
initialize (drgeoDrawableUI * drawable)
{
	// First invoke the initialize() method of drgeoBuildTool
	// super-class.
      drgeoBuildTool::initialize (drawable);

	// Now let's do our stuff.
	builder = new drgeoMacroBuilder (drawable->getFigure ());

	// Create the macro dialog that will control the builder.
	dialog = drawable->createMacroBuildDialog (builder);
}

// All the other methods are inherited from drgeoBuildTool.  We only
// redefine the handleChoice() method, and of course the initialize()
// and finish().

void drgeoMacroBuildTool::
handleChoice (drgeoDrawableUI * drawable,
	      geometricObject * item)
{
	// Forward this item to the macro builder.
	if (dialog && item) {
		dialog->add (item);
	}
}

void drgeoMacroBuildTool::
finish (drgeoDrawableUI * drawable)
{
	// First invoke the initialize() method of drgeoBuildTool
	// super-class.
      drgeoBuildTool::finish (drawable);

	// Remove the macro dialog that is controlling the builder.
	delete dialog;

	// Free the drgeoMacroBuilder.
	delete builder;
}

// -----------------------------------------------------
// Macro Play Tool, used for macro execution.
// -----------------------------------------------------

void drgeoMacroPlayTool::
initialize (drgeoDrawableUI * drawable)
{
	// First invoke the initialize() method of drgeoBuildTool
	// super-class.
      drgeoBuildTool::initialize (drawable);

	// Now let's do our stuff.
	player = new drgeoMacroPlayer (drawable->getFigure ());

	// Create the macro dialog that will control the Player.
	dialog = drawable->createMacroPlayDialog (player);
}

// All the other methods are inherited from drgeoBuildTool.  We only
// redefine the handleChoice() method, and of course the initialize()
// and finish().

void drgeoMacroPlayTool::
handleChoice (drgeoDrawableUI * drawable,
	      geometricObject * item)
{
	drgeoPoint xxx;

	// Forward this item to the macro Player.
	if (dialog && item) {
		dialog->add (item);
	}
}

void drgeoMacroPlayTool::
finish (drgeoDrawableUI * drawable)
{
	// First invoke the initialize() method of drgeoPlayTool
	// super-class.
      drgeoBuildTool::finish (drawable);

	// Remove the macro dialog that is controlling the Player.
	delete dialog;

	// Free the drgeoMacroPlayer.
	delete player;
}

// -----------------------------------------------------
// Delete Tool, used for object deletion.
// -----------------------------------------------------

void drgeoDeleteTool::
handleChoice (drgeoDrawableUI * drawable,
	      geometricObject * item)
{
	char *message;

	// Forward this item to the macro builder.
	if (drawable->figure && item != NULL) {
		// Hide the removed items just to show the consequence of the
		// removal.
		drawable->figure->hideRemovedItems (item);

		// Ask the user if he wants to proceed with the item deletion.
		if (item->getName ())
		{
			if (strlen (item->getName ())) 
				message = g_strdup_printf (N_ ("Remove %s ?"), item->getName ());
		}
		else 
			message = g_strdup (N_ ("Remove selected item ?"));		
		if (drawable->askOkCancel (message)) {
			// Remove the item if the user wants it.
			drawable->figure->removeItem (item);
		}
		// Show again the list of items to be removed.
		drawable->figure->showRemovedItems ();
		drawable->figure->redraw (FALSE);
	}
}






// -----------------------------------------------------
// Style Editor Tool, used to adjust colors, ...
// -----------------------------------------------------

void drgeoStyleTool::
initialize (drgeoDrawableUI * drawable)
{
	// First invoke the initialize() method of drgeoBuildTool
	// super-class.
      drgeoBuildTool::initialize (drawable);

	dialog = drawable->createStyleDialog ();
}

// All the other methods are inherited from drgeoBuildTool.  We only
// redefine the handleChoice() method, and of course the initialize()
// and finish().

void drgeoStyleTool::
handleChoice (drgeoDrawableUI * drawable,
	      geometricObject * item)
{

	// Forward this item to the style dialog.
	if (dialog && item) {
		// Update the dialog that match the item nwly selected.
		dialog->edit (item);

	}
}

void drgeoStyleTool::
finish (drgeoDrawableUI * drawable)
{
	// First invoke the initialize() method of drgeoBuildTool
	// super-class.
      drgeoBuildTool::finish (drawable);

	// Remove the macro dialog that is used to adjust style.
	if (dialog) {
		delete dialog;
	}
}
