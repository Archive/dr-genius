/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes, Laurent Gauthier  1997-2000
 * hilaire@seul.org 
 * lolo@seul.org
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef DRGEO_GTK_MACRO_H
#define DRGEO_GTK_MACRO_H

#include <gnome.h>

#include "drgeo_drawable.h"
#include "drgeo_macro.h"

class drgeoGtkMacroBuildDialog:public drgeoMacroBuildDialog {
	public:
	drgeoGtkMacroBuildDialog (class drgeoMacroBuilder * builder);
	~drgeoGtkMacroBuildDialog ();
	void show ();
	void hide ();
	void clear ();
	void add (geometricObject * item);
	void handleNext (GnomeDruidPage * druidPage);
	void handleBack (GnomeDruidPage * druidPage);
	void handleFinish ();
	void handleCancel ();
	  private:
	  GtkWidget * dialog, *input, *output, *text, *entry;
	// need this to know the number page we are in in druid
	GnomeDruidPage *startPage, *inputPage, *outputPage, *descriptionPage,
	 *finishPage;
};

class drgeoGtkMacroPlayDialog:public drgeoMacroPlayDialog {
	public:
	drgeoGtkMacroPlayDialog (drgeoMacroPlayer * player);
	~drgeoGtkMacroPlayDialog ();
	void show ();
	void hide ();
	void add (geometricObject * item);
	void handleFinish ();
	void handleSelect (int row, int column);
	void handleUnSelect ();
	  private:
	char *macroName;
	GtkWidget *dialog, *list, *description;
	GnomeDruidPage *druidListMacro;
};

class drgeoGtkStyleDialog:public drgeoStyleDialog {
	public:
	drgeoGtkStyleDialog (class drgeoGtkDrawable * aDrawable);
	~drgeoGtkStyleDialog ();
	void show ();
	void hide ();
	void setColor (drgeoColorType color);
	void setSize (drgeoLineType size);
	void setShape (drgeoPointType shape);
	void setFill (bool fill);
	void setName (char *name);
	void setExpression (char *expression);
	void edit (class geometricObject * aItem);
	  private:
	  drgeoGtkDrawable * drawable;
	geometricObject *item;
	GtkWidget *dialog, *fillButton, *nameEntry, *expressionEntry;
	GtkWidget *colorButton[12];
	GtkWidget *sizeButton[4], *shapeButton[4];
};

#endif
