/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-1999
 * hilaire.fernandes@iname.com 
 * 
 * This code is copyright Laurent Gauthier 1999
 * lolo@seul.org
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gnome.h>
#include "drgeo_drawable.h"

drgeoDrawable::drgeoDrawable ()
{
	// Nothing special there
}

void drgeoDrawable::
setFigure (drgeoFigure * figure)
{
	this->figure = figure;
	//  refresh ();
}

drgeoFigure *drgeoDrawable::
getFigure ()
{
	return (figure);
}

double drgeoDrawable::
pixelToWorld (int pixels)
{
	return (double) (((double) pixels) / scale);
}

int drgeoDrawable::
worldToPixel (double world)
{
	return (int) (world * scale);
}


// The following methods are invoked by subclasses of the
// drgeoDrawable class, and they implement the event handling
// behaviour of the drgeoDrawable.

// In particular they are responsible for invocation of drag, drop and
// selection related methods of the drgeoFigure object.

// These methods are operating using real-world coordinates, and not
// screen coordinates.

void drgeoDrawable::
refresh ()
{
	clear ();
	// Do nothing if no figure is currently associated with this drawable.
	if (figure != NULL) {
		figure->redraw (FALSE);
	}
}

// Implementation of drgeoDrawableUI methods.

drgeoDrawableUI::drgeoDrawableUI ()
{
	this->tool = NULL;
	selectTool = new drgeoSelectTool;
	buildTool = new drgeoBuildTool;
	styleTool = new drgeoStyleTool;
	macroBuildTool = new drgeoMacroBuildTool;
	deleteTool = new drgeoDeleteTool;
	macroPlayTool = new drgeoMacroPlayTool;
	setTool (selectTool);
}

drgeoDrawableUI::~drgeoDrawableUI ()
{
	delete macroPlayTool;
	delete deleteTool;
	delete macroBuildTool;
	delete styleTool;
	delete selectTool;
}

void drgeoDrawableUI::
setTool (drgeoTool * tool)
{
	if (this->tool) {
		// Finish with the current tool.
		this->tool->finish (this);
	}
	this->tool = tool;
	if (this->tool) {
		// And initialize the new one.
		// The parameter received could be a NULL pointer, this is why
		// we are running initialize inside this test.
		this->tool->initialize (this);
	}
}

void drgeoDrawableUI::
handlePress (drgeoPoint & where)
{
	if (tool) {
		tool->handlePress (this, where);
	}
}

void drgeoDrawableUI::
handleRelease (drgeoPoint & where)
{
	if (tool) {
		tool->handleRelease (this, where);
	}
}

void drgeoDrawableUI::
handleMouseAt (drgeoPoint & where)
{
	if (tool) {
		tool->handleMouseAt (this, where);
	}
}

void drgeoDrawableUI::
handleChoice (void *item)
{
	if (tool) {
		tool->handleChoice (this, (geometricObject *) item);
	}
}
