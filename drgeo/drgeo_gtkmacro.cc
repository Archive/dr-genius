/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes, Laurent Gauthier  1997-2000
 * hilaire@seul.org 
 * lolo@seul.org
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glade/glade.h>

#include "drgeo_gtkmacro.h"
#include "drgeo_gtkdrawable.h"
#include "drgeo_numeric.h"
#include "drgeo_command.h"

// Event handlers.

static gboolean
delete_event_cb (GtkWidget * widget, GdkEventAny * event,
		 drgeoGtkMacroBuildDialog * dialog)
{
	dialog->hide ();
	dialog->clear ();
	return true;
}

static void
cancel_event_cb (GtkWidget * widget, drgeoGtkMacroBuildDialog * dialog)
{
	dialog->handleCancel ();
}

static void
finish_event_cb (GtkWidget * widget, gpointer druidpage,
		 drgeoGtkMacroBuildDialog * dialog)
{
	dialog->handleFinish ();
}

static gboolean
next_event_cb (GnomeDruidPage * page, GnomeDruid * druid,
	       drgeoGtkMacroBuildDialog * dialog)
{
	dialog->handleNext (page);
	return false;
}

static gboolean
back_event_cb (GnomeDruidPage * page, GnomeDruid * druid,
	       drgeoGtkMacroBuildDialog * dialog)
{
	dialog->handleBack (page);
	return false;
}


void drgeoGtkMacroBuildDialog::
show ()
{
	gtk_widget_show (dialog);
}

void drgeoGtkMacroBuildDialog::
hide ()
{
	gtk_widget_hide (dialog);
}

void drgeoGtkMacroBuildDialog::
clear ()
{
	// Clear the input and output lists.
	gtk_clist_clear (GTK_CLIST (input));
	gtk_clist_clear (GTK_CLIST (output));

	// Clear the description field.
	gtk_text_set_point (GTK_TEXT (text), 0);
	gtk_text_forward_delete (GTK_TEXT (text),
				 gtk_text_get_length (GTK_TEXT (text)));

	// Clear the macro name field.
	gtk_entry_set_text (GTK_ENTRY (entry), "");

	// Reset the builder.
	builder->clearOutput ();
	builder->clearInput ();
	builder->setName (NULL);
	builder->setDescription (NULL);
	// XXX dirty, this prevent a crash when user close the dialog
	// and still select items
	builder->setMode (drgeoMacroDescriptionMode);
}

void drgeoGtkMacroBuildDialog::
add (geometricObject * item)
{
	gchar *array[2];
	gint row;

	// Add the name of this item to the list of item selected.
	array[0] = item->getTypeName ();
	array[1] = NULL;

	switch (builder->getMode ()) {
	case drgeoMacroInputMode:
		builder->addInput (item);
		row = gtk_clist_find_row_from_data (GTK_CLIST (input), (gpointer) item);
		if (row >= 0) {
			//item already selected, remove it from the list
			gtk_clist_remove (GTK_CLIST (input), row);
		} else {
			row = gtk_clist_append (GTK_CLIST (input), array);
			gtk_clist_set_row_data (GTK_CLIST (input), row, item);
		}
		break;
	case drgeoMacroOutputMode:
		builder->addOutput (item);
		row = gtk_clist_find_row_from_data (GTK_CLIST (output), (gpointer) item);
		if (row >= 0) {
			//item already selected, remove it from the list
			gtk_clist_remove (GTK_CLIST (output), row);
		} else {
			row = gtk_clist_append (GTK_CLIST (output), array);
			gtk_clist_set_row_data (GTK_CLIST (output), row, item);
		}
		break;
	default:
		// XXX This should not happen.
		break;
	}
}

void drgeoGtkMacroBuildDialog::
handleNext (GnomeDruidPage * druidPage)
{
	if (druidPage == startPage) {
		// we enter the input mode
		builder->setMode (drgeoMacroInputMode);
	} else if (druidPage == inputPage) {
		// we enter the output mode
		builder->setMode (drgeoMacroOutputMode);
	} else if (druidPage == outputPage) {
		// we enter the description mode
		builder->setMode (drgeoMacroDescriptionMode);
	} else if (druidPage == descriptionPage) {
		// we enter the finish mode
		builder->setMode (drgeoMacroFinishMode);
	} else {
		printf ("No default in drgeoGtkMacroBuildDialog::handleNext\n");
	}
}

void drgeoGtkMacroBuildDialog::
handleBack (GnomeDruidPage * druidPage)
{
	if (druidPage == inputPage) {
		// we enter the start mode
		builder->setMode (drgeoMacroStartMode);
	} else if (druidPage == outputPage) {
		// we enter the input mode
		builder->setMode (drgeoMacroInputMode);
	} else if (druidPage == descriptionPage) {
		// we enter the output mode
		builder->setMode (drgeoMacroOutputMode);
	} else if (druidPage == finishPage) {
		// we enter the description mode
		builder->setMode (drgeoMacroDescriptionMode);
	} else {
		printf ("No default in drgeoGtkMacroBuildDialog::handleBack\n");
	}
}

void drgeoGtkMacroBuildDialog::
handleFinish ()
{
	macro *aMacro;
	drgeoMacroRegistry *registry;

	// Invoke the builder to create the new macro.
	builder->setName (gtk_entry_get_text (GTK_ENTRY (entry)));
	builder->setDescription (gtk_editable_get_chars (GTK_EDITABLE (text),
							 0,
							 gtk_text_get_length
							 (GTK_TEXT (text))));
	if (builder->checkParameters ()) {
		// Create the new macro.
		aMacro = builder->createMacro ();

		// Register it.
	      registry = drgeoMacroRegistry::get ();
		registry->add (aMacro);

		// Clean everything.
		hide ();
		clear ();
	}
}

void drgeoGtkMacroBuildDialog::
handleCancel ()
{
	hide ();
	clear ();
}

drgeoGtkMacroBuildDialog::drgeoGtkMacroBuildDialog (drgeoMacroBuilder * builder)
{
	GladeXML *xmlDialogWidget;
	GtkWidget *widget;


	this->builder = builder;

	xmlDialogWidget = glade_xml_new (DRGENIUS_GLADEDIR "/drgeo.glade",
					 "buildMacroDialog");

	dialog = glade_xml_get_widget (xmlDialogWidget, "buildMacroDialog");
	input = glade_xml_get_widget (xmlDialogWidget, "input");
	output = glade_xml_get_widget (xmlDialogWidget, "output");
	entry = glade_xml_get_widget (xmlDialogWidget, "entry");
	text = glade_xml_get_widget (xmlDialogWidget, "text");

	// Connect the signals of the ...
	widget = glade_xml_get_widget (xmlDialogWidget, "buildMacroDruid");
	gtk_signal_connect (GTK_OBJECT (widget), "cancel",
			GTK_SIGNAL_FUNC (cancel_event_cb), (gpointer) this);
	// ...Start page
	startPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageStart");
	gtk_signal_connect (GTK_OBJECT (startPage), "next",
			    GTK_SIGNAL_FUNC (next_event_cb),
			    (gpointer) this);
	// ...Input page
	inputPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageInput");
	gtk_signal_connect (GTK_OBJECT (inputPage), "next",
			    GTK_SIGNAL_FUNC (next_event_cb),
			    (gpointer) this);
	gtk_signal_connect (GTK_OBJECT (inputPage), "back",
			    GTK_SIGNAL_FUNC (back_event_cb),
			    (gpointer) this);
	// ...Output page
	outputPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageOutput");
	gtk_signal_connect (GTK_OBJECT (outputPage), "next",
			    GTK_SIGNAL_FUNC (next_event_cb),
			    (gpointer) this);
	gtk_signal_connect (GTK_OBJECT (outputPage), "back",
			    GTK_SIGNAL_FUNC (back_event_cb),
			    (gpointer) this);
	// ...Description page
	descriptionPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageDescription");
	gtk_signal_connect (GTK_OBJECT (descriptionPage), "next",
			    GTK_SIGNAL_FUNC (next_event_cb),
			    (gpointer) this);
	gtk_signal_connect (GTK_OBJECT (descriptionPage), "back",
			    GTK_SIGNAL_FUNC (back_event_cb),
			    (gpointer) this);
	// ...Finish page
	finishPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageFinish");
	gtk_signal_connect (GTK_OBJECT (finishPage), "back",
			    GTK_SIGNAL_FUNC (back_event_cb),
			    (gpointer) this);
	gtk_signal_connect (GTK_OBJECT (finishPage), "finish",
			    GTK_SIGNAL_FUNC (finish_event_cb),
			    (gpointer) this);

	gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
			    GTK_SIGNAL_FUNC (delete_event_cb), this);
}

drgeoGtkMacroBuildDialog::~drgeoGtkMacroBuildDialog ()
{
	gtk_widget_destroy (dialog);
}

// Event handlers.

static gboolean
play_delete_event_cb (GtkWidget * widget, GdkEventAny * event,
		      drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleFinish ();
	return true;
}

static void
play_finish_event_cb (GtkWidget * widget, gpointer druidpage,
		      drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleFinish ();
}
static void
play_cancel_event_cb (GtkWidget * widget, drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleFinish ();
}

static void
play_select_event_cb (GtkWidget * widget,
		      gint row,
		      gint column,
		      GdkEventButton * event,
		      drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleSelect (row, column);
}

static void
play_unselect_event_cb (GtkWidget * widget,
			gint row,
			gint column,
			GdkEventButton * event,
			drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleUnSelect ();
}

void drgeoGtkMacroPlayDialog::
handleSelect (int row, int column)
{
	// Get the name of the macro selected.
	gtk_clist_get_text (GTK_CLIST (list), row, column, &macroName);
	// Inform the player of the new macro
	player->setMacro (macroName);
	// make sure there is nothing left in the description buffer
	gtk_text_set_point (GTK_TEXT (description), 0);
	gtk_text_forward_delete (GTK_TEXT (description),
			      gtk_text_get_length (GTK_TEXT (description)));
	// Update the description buffer of the dialog
	gtk_text_insert (GTK_TEXT (description), NULL, NULL, NULL,
			 player->getDescription (), -1);
}

void drgeoGtkMacroPlayDialog::
handleUnSelect ()
{
	// update description text buffer in case the user change it
	player->setDescription (gtk_editable_get_chars (GTK_EDITABLE (description),
							0,
							gtk_text_get_length
						 (GTK_TEXT (description))));
	// Clear item selection
	player->setMacro (NULL);
	// Forget the selected macro
	macroName = NULL;
	// clear the description buffer of dialog
	gtk_text_set_point (GTK_TEXT (description), 0);
	gtk_text_forward_delete (GTK_TEXT (description),
			      gtk_text_get_length (GTK_TEXT (description)));
}

void drgeoGtkMacroPlayDialog::
show ()
{
	gtk_widget_show (dialog);
}

void drgeoGtkMacroPlayDialog::
hide ()
{
	gtk_widget_hide (dialog);
}

void drgeoGtkMacroPlayDialog::
handleFinish ()
{
	// update description text buffer in case the user change it
	player->setDescription (gtk_editable_get_chars (GTK_EDITABLE (description),
							0,
							gtk_text_get_length
							(GTK_TEXT (description))));
	gtk_clist_clear (GTK_CLIST (list));
	// clear the dialog description buffer
	gtk_text_set_point (GTK_TEXT (description), 0);
	gtk_text_forward_delete (GTK_TEXT (description),
			      gtk_text_get_length (GTK_TEXT (description)));
	// unselect the macro and the selected item
	player->setMacro (NULL);
	hide ();
}

void drgeoGtkMacroPlayDialog::
add (geometricObject * item)
{
	// only valid item come there
	player->add (item);
}

drgeoGtkMacroPlayDialog::drgeoGtkMacroPlayDialog (drgeoMacroPlayer * player)
{
	GladeXML *xmlDialogWidget;
	GtkWidget *widget;
	drgeoMacroRegistry *registry;
	macro *aMacro;
	char *array[2];
	int found;

	this->player = player;
	macroName = NULL;
	player->setMacro (NULL);
	xmlDialogWidget = glade_xml_new (DRGENIUS_GLADEDIR "/drgeo.glade",
					 "playMacroDialog");

	dialog = glade_xml_get_widget (xmlDialogWidget, "playMacroDialog");
	description = glade_xml_get_widget (xmlDialogWidget, "descriptionPlay");
	list = glade_xml_get_widget (xmlDialogWidget, "listPlay");
	druidListMacro = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget,
							  "druidListMacro");

	// Connect the signals
	gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
			    GTK_SIGNAL_FUNC (play_delete_event_cb), this);
	gtk_signal_connect (GTK_OBJECT (list),
			    "select_row",
			    GTK_SIGNAL_FUNC (play_select_event_cb),
			    this);
	gtk_signal_connect (GTK_OBJECT (list),
			    "unselect_row",
			    GTK_SIGNAL_FUNC (play_unselect_event_cb),
			    this);
	widget = glade_xml_get_widget (xmlDialogWidget, "playMacroDruid");
	gtk_signal_connect (GTK_OBJECT (widget), "cancel",
		   GTK_SIGNAL_FUNC (play_cancel_event_cb), (gpointer) this);
	widget = glade_xml_get_widget (xmlDialogWidget, "druidPageFinish");
	gtk_signal_connect (GTK_OBJECT (widget), "finish",
		   GTK_SIGNAL_FUNC (play_finish_event_cb), (gpointer) this);

	// Fill the list with the macro names.
      registry = drgeoMacroRegistry::get ();
	array[1] = NULL;
	found = 0;
	for (aMacro = registry->first ();
	     !registry->isDone ();
	     aMacro = registry->next ()) {

		// Append this macro name to the list.
		array[0] = aMacro->getName ();
		gtk_clist_append (GTK_CLIST (list), array);
		found = 1;
	}
	if (found) {
		gtk_clist_select_row (GTK_CLIST (list), 0, 0);
		gtk_clist_get_text (GTK_CLIST (list), 0, 0, &macroName);
		player->setMacro (macroName);
		gtk_text_set_point (GTK_TEXT (description), 0);
		gtk_text_forward_delete (GTK_TEXT (description),
			      gtk_text_get_length (GTK_TEXT (description)));
		gtk_text_insert (GTK_TEXT (description), NULL, NULL, NULL,
				 player->getDescription (), -1);
	}
}

drgeoGtkMacroPlayDialog::~drgeoGtkMacroPlayDialog ()
{
	gtk_widget_destroy (dialog);
}

// Event handlers.

static void
style_color_button_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkStyleDialog *dialog;

	dialog = (drgeoGtkStyleDialog *)
		gtk_object_get_data (GTK_OBJECT (widget), "drgeo_style");
	dialog->setColor ((drgeoColorType) GPOINTER_TO_INT (data));
}
static void
style_size_button_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkStyleDialog *dialog;

	dialog = (drgeoGtkStyleDialog *)
		gtk_object_get_data (GTK_OBJECT (widget), "drgeo_style");
	dialog->setSize ((drgeoLineType) GPOINTER_TO_INT (data));
}

static void
style_shape_button_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkStyleDialog *dialog;

	dialog = (drgeoGtkStyleDialog *)
		gtk_object_get_data (GTK_OBJECT (widget), "drgeo_style");
	dialog->setShape ((drgeoPointType) GPOINTER_TO_INT (data));
}

static void
style_fill_button_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkStyleDialog *dialog;

	dialog = (drgeoGtkStyleDialog *)
		gtk_object_get_data (GTK_OBJECT (widget), "drgeo_style");
	dialog->setFill ((bool) data);
}
static void
style_change_name_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkStyleDialog *dialog;

	dialog = (drgeoGtkStyleDialog *)
		gtk_object_get_data (GTK_OBJECT (widget), "drgeo_style");
	dialog->setName (gtk_entry_get_text (GTK_ENTRY (widget)));
}
static void
style_change_expression_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkStyleDialog *dialog;

	dialog = (drgeoGtkStyleDialog *)
		gtk_object_get_data (GTK_OBJECT (widget), "drgeo_style");
	dialog->setExpression (gtk_entry_get_text (GTK_ENTRY (widget)));
}

static void
style_delete_event_cb (GtkWidget * widget, drgeoGtkStyleDialog * dialog)
{
	dialog->hide ();
}

static void
style_close_event_cb (GtkWidget * widget, drgeoGtkStyleDialog * dialog)
{
	dialog->hide ();
}


void drgeoGtkStyleDialog::
show ()
{
	gtk_widget_show_all (dialog);
}

void drgeoGtkStyleDialog::
hide ()
{
	gtk_widget_hide (dialog);
}

void drgeoGtkStyleDialog::
edit (geometricObject * aItem)
{
	// Show the dialog and update the state of the various buttons.
	item = aItem;

	// Adjust the buttons to the properties of the item.
	if (item->getStyle().color >= 0 && item->getStyle().color <= 11)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (colorButton[item->getStyle().color]), 
					      TRUE);
	if (item->getStyle().type >= 0 && item->getStyle().type <= 3)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (sizeButton[item->getStyle().type]), TRUE);
	if (item->getStyle().pointShape >= 1 && item->getStyle().pointShape <= 3)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
			   (shapeButton[item->getStyle().pointShape - 1]), TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (fillButton),
				      item->getStyle().fill);
	if (item->getName ())
		gtk_entry_set_text (GTK_ENTRY (nameEntry), item->getName ());
	else
		gtk_entry_set_text (GTK_ENTRY (nameEntry), "");
	{
		gchar out[256] = "";
		if (item->getCategory () & VALUE) 
			sprintf (out, "%f", ((value *) item)->getValue ());
		else if (item->getCategory () & EQUATION)
		{
		}
		gtk_entry_set_text (GTK_ENTRY (expressionEntry), out);
	}
	show ();
}

void drgeoGtkStyleDialog::
setColor (drgeoColorType color)
{
	gpointer p;
	// Set the current item color.
	/* does something new happen ? */
	if (item->getStyle().color == color)
		return;
	p = g_malloc (sizeof (drgeoColorType));
	*((drgeoColorType *) p) = color;
	drawable->getFigure()->setItemAttribute (item, drgeoColor, p);
}

void drgeoGtkStyleDialog::
setSize (drgeoLineType size)
{
	gpointer p;
	// Set the current item size.
	/* does something new happen ? */	
	if (item->getStyle().type == size)
		return;
	p = g_malloc (sizeof (drgeoLineType));
	*((drgeoLineType *) p) = size;
	drawable->getFigure()->setItemAttribute (item, drgeoSize, p);
}

void drgeoGtkStyleDialog::
setShape (drgeoPointType shape)
{
	gpointer p;
	// Set the current item shape.
	if (item->getStyle().pointShape == shape)
		return;
	p = g_malloc (sizeof (drgeoPointType));
	*((drgeoPointType *) p) = shape;
	drawable->getFigure()->setItemAttribute (item, drgeoShape, p);
}

void drgeoGtkStyleDialog::
setFill (bool fill)
{
	gpointer p;
	// Set the current item fill.
	if (item->getStyle().fill == fill)
		return;
	p = g_malloc (sizeof (bool));
	*((bool *) p) = fill;
	drawable->getFigure()->setItemAttribute (item, drgeoFill, p);
}

void drgeoGtkStyleDialog::
setName (char *name)
{
	gpointer p;

	p = (gpointer ) g_strdup (name);
	drawable->getFigure()->setItemAttribute (item, drgeoName, p);
}

void drgeoGtkStyleDialog::
setExpression (char *expression)
{
	// Set the current item expression.
	gdouble *val;

	if ((item->getCategory () & VALUE) 
	    && (item->getType () & FREE_VALUE) ) 
	{
		val = (gdouble *) g_malloc (sizeof (gdouble));
		*val = strtod (expression, NULL);
		drawable->getFigure()->setItemAttribute (item, drgeoValue, 
							 (gpointer) val);
	}
}

// Include icons for the style box.
// The style line for the style dialog box
#include "icones/dotline.xpm"
#include "icones/dashline.xpm"
#include "icones/normalline.xpm"
#include "icones/largeline.xpm"
// The style point shape for the style dialog box
#include "icones/square.xpm"
#include "icones/round.xpm"
#include "icones/cross.xpm"
#include "icones/fill.xpm"

drgeoGtkStyleDialog::drgeoGtkStyleDialog (drgeoGtkDrawable * aDrawable)
{
	GtkWidget *table_style, *color_button, *size_button, *shape_button,
	 *selected_button, *iconw, *vbox, *hbox, *label, *entry_change_name,
	 *entry_change_expression, *areaw;

	GSList *group;
	GtkStyle *color_style, *defstyle;
	GdkPixmap *icon;
	GdkBitmap *mask;
	GtkWidget *area;
	int a;

	drawable = aDrawable;
	area = aDrawable->drawing_area;

	// Create the dialog to change object style
	dialog = gnome_dialog_new (N_ ("Change the object style"),
				   GNOME_STOCK_BUTTON_CLOSE,
				   NULL);

	//  gnome_dialog_set_default (GNOME_DIALOG(dialog), GNOME_CLOSE);
	gtk_window_set_policy (GTK_WINDOW (dialog), FALSE, FALSE, FALSE);
	table_style = gtk_table_new (4, 5, FALSE);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
			    table_style, TRUE, TRUE, 0);

	// pack the color button
	defstyle = gtk_widget_get_default_style ();
	color_style = gtk_widget_get_default_style ();
	group = NULL;
	for (a = 0; a < 12; a++) {
		color_button = gtk_radio_button_new (group);
		group = gtk_radio_button_group (GTK_RADIO_BUTTON (color_button));
		gtk_container_border_width (GTK_CONTAINER (color_button), 1);
		gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (color_button), FALSE);
		gtk_table_attach_defaults (GTK_TABLE (table_style), color_button,
				  1 + a % 4, (a % 4) + 2, a / 4, a / 4 + 1);
		color_style = gtk_style_copy (defstyle);
		color_style->bg[0] = aDrawable->color[a];
		color_style->bg[1] = aDrawable->color[a];
		color_style->bg[2] = aDrawable->color[a];
		color_style->bg[3] = aDrawable->color[a];
		color_style->bg[4] = aDrawable->color[a];
		// gtk_widget_show (color_button[a]);
		gtk_widget_set_style (color_button, color_style);
		gtk_object_set_data (GTK_OBJECT (color_button), "drgeo_style", this);
		gtk_signal_connect (GTK_OBJECT (color_button), "pressed",
				    GTK_SIGNAL_FUNC (style_color_button_cb), GINT_TO_POINTER (a));

		colorButton[a] = color_button;
	}


	// pack the size button on the left
	// dot line (invisible)
	icon = gdk_pixmap_create_from_xpm_d (area->window, &mask,
					     &area->style->white,
					     dotline_xpm);
	iconw = gtk_pixmap_new (icon, mask);

	size_button = gtk_radio_button_new (NULL);
	gtk_container_add (GTK_CONTAINER (size_button), iconw);
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (size_button));
	gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (size_button), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table_style), size_button,
				   0, 1, 0, 1);
	gtk_object_set_data (GTK_OBJECT (size_button), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (size_button), "pressed",
	       GTK_SIGNAL_FUNC (style_size_button_cb), GINT_TO_POINTER (0));

	sizeButton[0] = size_button;

	// dash line
	icon = gdk_pixmap_create_from_xpm_d (area->window, &mask,
					     &area->style->white,
					     dashline_xpm);
	iconw = gtk_pixmap_new (icon, mask);

	size_button = gtk_radio_button_new (group);
	gtk_container_add (GTK_CONTAINER (size_button), iconw);
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (size_button));
	gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (size_button), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table_style), size_button,
				   0, 1, 1, 2);
	gtk_object_set_data (GTK_OBJECT (size_button), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (size_button), "pressed",
	       GTK_SIGNAL_FUNC (style_size_button_cb), GINT_TO_POINTER (1));

	sizeButton[1] = size_button;

	// normal line
	icon = gdk_pixmap_create_from_xpm_d (area->window, &mask,
					     &area->style->white,
					     normalline_xpm);
	iconw = gtk_pixmap_new (icon, mask);

	size_button = gtk_radio_button_new (group);
	gtk_container_add (GTK_CONTAINER (size_button), iconw);
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (size_button));
	gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (size_button), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table_style), size_button,
				   0, 1, 2, 3);
	gtk_object_set_data (GTK_OBJECT (size_button), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (size_button), "pressed",
	       GTK_SIGNAL_FUNC (style_size_button_cb), GINT_TO_POINTER (2));

	sizeButton[2] = size_button;

	// large line
	icon = gdk_pixmap_create_from_xpm_d (area->window, &mask,
					     &area->style->white,
					     largeline_xpm);
	iconw = gtk_pixmap_new (icon, mask);

	size_button = gtk_radio_button_new (group);
	gtk_container_add (GTK_CONTAINER (size_button), iconw);
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (size_button));
	gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (size_button), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table_style), size_button,
				   0, 1, 3, 4);
	gtk_object_set_data (GTK_OBJECT (size_button), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (size_button), "pressed",
	       GTK_SIGNAL_FUNC (style_size_button_cb), GINT_TO_POINTER (3));

	sizeButton[3] = size_button;

	// pack the shape button
	// round shape
	icon = gdk_pixmap_create_from_xpm_d (area->window, &mask,
					     &area->style->white,
					     round_xpm);
	iconw = gtk_pixmap_new (icon, mask);

	group = NULL;

	shape_button = gtk_radio_button_new (group);
	gtk_container_add (GTK_CONTAINER (shape_button), iconw);
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (shape_button));
	gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (shape_button), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table_style), shape_button,
				   1, 2, 3, 4);
	gtk_object_set_data (GTK_OBJECT (shape_button), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (shape_button), "pressed",
	      GTK_SIGNAL_FUNC (style_shape_button_cb), GINT_TO_POINTER (0));

	shapeButton[0] = shape_button;

	// cross shape
	icon = gdk_pixmap_create_from_xpm_d (area->window, &mask,
					     &area->style->white,
					     cross_xpm);
	iconw = gtk_pixmap_new (icon, mask);

	shape_button = gtk_radio_button_new (group);
	gtk_container_add (GTK_CONTAINER (shape_button), iconw);
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (shape_button));
	gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (shape_button), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table_style), shape_button,
				   2, 3, 3, 4);
	gtk_object_set_data (GTK_OBJECT (shape_button), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (shape_button), "pressed",
	      GTK_SIGNAL_FUNC (style_shape_button_cb), GINT_TO_POINTER (1));

	shapeButton[1] = shape_button;

	// square shape
	icon = gdk_pixmap_create_from_xpm_d (area->window, &mask,
					     &area->style->white,
					     square_xpm);
	iconw = gtk_pixmap_new (icon, mask);

	shape_button = gtk_radio_button_new (group);
	gtk_container_add (GTK_CONTAINER (shape_button), iconw);
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (shape_button));
	gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (shape_button), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table_style), shape_button,
				   3, 4, 3, 4);
	gtk_object_set_data (GTK_OBJECT (shape_button), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (shape_button), "pressed",
	      GTK_SIGNAL_FUNC (style_shape_button_cb), GINT_TO_POINTER (2));

	shapeButton[2] = shape_button;


	// fill shape
	icon = gdk_pixmap_create_from_xpm_d (area->window, &mask,
					     &area->style->white,
					     fill_xpm);
	iconw = gtk_pixmap_new (icon, mask);

	shape_button = gtk_toggle_button_new ();
	gtk_container_add (GTK_CONTAINER (shape_button), iconw);
	gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (shape_button), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table_style), shape_button,
				   4, 5, 3, 4);
	gtk_object_set_data (GTK_OBJECT (shape_button), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (shape_button), "pressed",
	     GTK_SIGNAL_FUNC (style_fill_button_cb), GINT_TO_POINTER (128));

	fillButton = shape_button;


	// prepare a vbox for entry
	//gtk_box_set_homogeneous (GTK_BOX (GTK_DIALOG (dialog)->action_area), FALSE);
	vbox = gtk_vbox_new (FALSE, 2);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 2);
	gtk_container_add (GTK_CONTAINER (GTK_BOX (GNOME_DIALOG
					     (dialog)->action_area)), vbox);

	// Setup the entry widget to change the object name a hbox to put
	// the label and entry.
	hbox = gtk_hbox_new (FALSE, 2);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

	label = gtk_label_new (_ ("Rename this object : "));
	entry_change_name = gtk_entry_new_with_max_length (LONGUEUR_NOM);

	gtk_box_pack_start (GTK_BOX (hbox),
			    label, FALSE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (hbox),
			    entry_change_name, TRUE, TRUE, 0);
	gtk_widget_show (entry_change_name);
	gtk_widget_set_usize (entry_change_name, LONGUEUR_NOM * 8, 20);

	gtk_object_set_data (GTK_OBJECT (entry_change_name), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (entry_change_name), "activate",
			    GTK_SIGNAL_FUNC (style_change_name_cb),
			    (gpointer) NULL);

	nameEntry = entry_change_name;


	// Setup the entry widget to change the value/expression of a
	// numeric value or expression object a hbox to put the label and
	// entry.
	hbox = gtk_hbox_new (FALSE, 2);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

	label = gtk_label_new (_ ("Expression : "));
	entry_change_expression = gtk_entry_new_with_max_length (255);

	gtk_box_pack_start (GTK_BOX (hbox),
			    label, FALSE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (hbox),
			    entry_change_expression, TRUE, TRUE, 0);

	gtk_widget_set_usize (entry_change_expression, LONGUEUR_NOM * 8, 20);

	gtk_object_set_data (GTK_OBJECT (entry_change_expression), "drgeo_style", this);
	gtk_signal_connect (GTK_OBJECT (entry_change_expression), "activate",
			    GTK_SIGNAL_FUNC (style_change_expression_cb),
			    (gpointer) NULL);
	expressionEntry = entry_change_expression;

	// Connect the close button to the right event handler.
	gnome_dialog_button_connect (GNOME_DIALOG (dialog), 0,
				     GTK_SIGNAL_FUNC (style_close_event_cb),
				     (gpointer) this);
}

drgeoGtkStyleDialog::~drgeoGtkStyleDialog ()
{
	hide ();
	gtk_widget_destroy (dialog);
}
