/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-1999
 * hilaire.fernandes@iname.com 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* This file declare the Dr Geo API */

/* This part of the API is mainly used by the XML parser */

/****************/
/* Point Object */
/****************/
drgeo_point_free (gchar * name, gdouble x, gdouble y);
/* point2 free point  */
drgeo_point_coordinates (gchar * name, gchar * value1, gchar * value2);
/* point1 point defines by two values */
drgeo_point_middle (gchar * name, gchar * pt1, gchar * pt2);
/* point_milieu_2points */
drgeo_point_middle (gchar * name, gchar * segment);
/* point_milieu_segment */
drgeo_point_on_circle (gchar * name, *gchar circle, gdouble x);
/* point_sur_cercle */
/* x is the curviligne abscisse in radian */
drgeo_point_on_circle_and_circle (gchar * name, gchar * circle1, gchar * circle2, gchar signe);
/* point_sur_cercle_et_cercle */
/* in fact both intersection point are build, but we give name to the one marked with signe */
drgeo_point_on_circle_and_half_line (gchar * name, gchar * circle, gchar * half_line, gchar signe);
/* point_sur_cercle_et_demi_droite */
drgeo_point_on_circle_and_line (gchar * name, gchar * circle, gchar * line, gchar signe);
/* point_sur_cercle_et_droite */
drgeo_point_on_circle_and_segment (gchar * name, gchar * circle, gchar * segment, gchar signe);
/* point_sur_cercle_et_segment */
drgeo_point_on_half_line (gchar * name, *gchar half_line, gdouble x);
/* point_sur_demi_droite */
/* x is the abscisse, from the line origin (normaly the first point involved in the line) and an unit vector */
drgeo_point_on_half_line_and_half_line (gchar * name, *gchar half_line1, *gchar half_line2,
					gdouble x);
/* point_sur_demi_droite_et_demi_droite */
drgeo_point_on_half_line_and_segment (gchar * name, *gchar half_line, *gchar segment,
				      gdouble x);
/* point_sur_demi_droite_et_segment */
drgeo_point_on_line (gchar * name, *gchar line, gdouble x);
/* point_sur_droite */
/* x is the abscisse, from the line origin (normaly the first point involved in the line) and an unit vector */
drgeo_point_on_line_and_half_line (gchar * name, *gchar line, *gchar half_line,
				   gdouble x);
/* point_sur_droite_et_demi_droite */
drgeo_point_on_line_and_segment (gchar * name, *gchar line, *gchar segment,
				 gdouble x);
/* point_sur_droite_et_segment */
drgeo_point_on_locus (gchar * name, *gchar line, gint n);
/* point_sur_locus */
/*  n is  the n th point of the locus */
drgeo_point_on_segment (gchar * name, *gchar segment, gdouble x);
/* point_sur_segment */
/* x belong to [0;1] */
/*This part of the API is mainly used by the embedded interpeter */
drgeo_point_on_segment_and_segment (gchar * name, *gchar segment1, gchar * segment2,
				    gdouble x);
/* point_sur_segment_et_segment */
drgeo_point_reflexion (gchar * name, gchar * point, gchar * axe);
/* reflexion_point */
drgeo_point_rotation (gchar * name, gchar * point, gchar * center, gchar * angle);
/* rotation_point */
drgeo_point_symetry (gchar * name, gchar * point, gchar * center);
/* symetrie_point */
drgeo_point_translation (gchar * name, gchar * point, gchar * vector);
/* translation_point */

/****************/
/* Value object */
/****************/
drgeo_value (gchar * name, double v);
drgeo_abscissa_point (gchar * name, gchar * point);
drgeo_ordinate_point (gchar * name, gchar * point);
drgeo_abscissa_vector (gchar * name, gchar * vector);
drgeo_ordinate_vector (gchar * name, gchar * vector);
drgeo_angle_2vectors (gchar * name, gchar * vector1, gchar * vector2);
drgeo_angle_3points (gchar * name, gchar * point1, gchar * point2, gchar * point3);
drgeo_distance_point_to_circle (gchar * name, gchar * point, gchar * circle);
drgeo_distance_point_to_line (gchar * name, gchar * point, gchar * line);
drgeo_distance_point_to_point (gchar * name, gchar * point1, gchar * point2);
drgeo_segment_length (gchar * name, gchar * segment);
drgeo_vector_norm (gchar * name, gchar * vector);
drgeo_circle_perimeter (gchar * name, gchar * circle);

/*****************/
/* Circle object */
/*****************/
drgeo_circle_center_and_point (gchar * name, gchar * center, gchar * point);
drgeo_circle_center_and_segment (gchar * name, gchar * center, gchar * segment);
drgeo_circle_center_and_radius (gchar * name, gchar * center, gchar * radius);
drgeo_circle_reflexion (gchar * name, gchar * circle, gchar * axe);
drgeo_circle_rotation (gchar * name, gchar * circle, gchar * center, gchar * angle);
drgeo_circle_symetry (gchar * name, gchar * circle, gchar * center);
drgeo_circle_translation (gchar * name, gchar * circle, gchar * vector);

/***************/
/* Line object */
/***************/
drgeo_line (gchar * name, gchar * point1, gchar * point2);
drgeo_line_perpendicular (gchar * name, gchar * point, gchar * line);
drgeo_line_orthogonal (gchar * name, gchar * point, gchar * point1, gchar * point2);
drgeo_line_parallel (gchar * name, gchar * point, gchar * line);
drgeo_line_parallel2 (gchar * name, gchar * point, gchar * point1, gchar * point2);
drgeo_line_reflexion (gchar * name, gchar * line, gchar * axe);
drgeo_line_rotation (gchar * name, gchar * line, gchar * center, gchar * angle);
drgeo_line_symetry (gchar * name, gchar * line, gchar * center);
drgeo_line_translation (gchar * name, gchar * line, gchar * vector);

/********************/
/* Half line object */
/********************/
drgeo_half_line (gchar * name, gchar * point1, gchar * point2);
drgeo_half_line_reflexion (gchar * name, gchar * half_line, gchar * axe);
drgeo_half_line_rotation (gchar * name, gchar * half_line, gchar * center, gchar * angle);
drgeo_half_line_symetry (gchar * name, gchar * half_line, gchar * center);
drgeo_half_line_translation (gchar * name, gchar * half_line, gchar * vector);

/******************/
/* Segment object */
/******************/
drgoe_segment (gchar * name, gchar * point1, gchar * point2);
drgeo_segment_reflexion (gchar * name, gchar * segment, gchar * axe);
drgeo_segment_rotation (gchar * name, gchar * segment, gchar * center, gchar * angle);
drgeo_segment_symetry (gchar * name, gchar * segment, gchar * center);
drgeo_segment_translation (gchar * name, gchar * segment, gchar * vector);

/*****************/
/* Vector object */
/*****************/
drgeo_vector (gchar * name, gchar * point1, gchar * point2);
drgeo_vector_reflexion (gchar * name, gchar * vector, gchar * axe);
drgeo_vector_rotation (gchar * name, gchar * vector, gchar * center, gchar * angle);
drgeo_vector_translation (gchar * name, gchar * vector, gchar * vector);

/*******************/
/* Equation object */
/*******************/
drgeo_equation_circle (gchar * name, gchar * circle);
drgeo_equation_line (gchar * name, gchar * line);

/**************************/
/* Geometric locus object */
/**************************/
drgeo_locus (gchar * name, gchar * mobile_point, gchar * locus_point);
