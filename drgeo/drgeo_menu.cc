/* Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-1999
 * hilaire.fernandes@iname.com 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include "drgeo_menu.h"
#include "define.h"
#include "drgeo_drawable.h"
#include "drgeo_gtkdrawable.h"
#include "drgeo_figure.h"
#include "../gobobjs/drgenius-mdi-child.h"
#include "../gobobjs/drgeo-mdi-child.h"

extern GnomeMDI *mdi;

GtkWidget *drgeoPopupMenu;

static gpointer common_icon_clicked (GtkWidget *w);
static void drgeo_menu_cb (GtkWidget * widget, gpointer data);
static void drgeo_free_point_cb (GtkWidget * widget, gpointer);
static void drgeo_middle_point_cb (GtkWidget * widget, gpointer);
static void drgeo_intersection_cb (GtkWidget * widget, gpointer);
static void drgeo_mark_point_cb (GtkWidget * widget, gpointer);
static void drgeo_line_cb (GtkWidget * widget, gpointer);
static void drgeo_half_line_cb (GtkWidget * widget, gpointer);
static void drgeo_segment_cb (GtkWidget * widget, gpointer);
static void drgeo_vector_cb (GtkWidget * widget, gpointer);
static void drgeo_circle_cb (GtkWidget * widget, gpointer);
static void drgeo_arc_circle_cb (GtkWidget * widget, gpointer);
static void drgeo_locus_point_cb (GtkWidget * widget, gpointer);
static void drgeo_parallel_cb (GtkWidget * widget, gpointer);
static void drgeo_orthogonal_cb (GtkWidget * widget, gpointer);
static void drgeo_reflexion_cb (GtkWidget * widget, gpointer);
static void drgeo_symmetry_cb (GtkWidget * widget, gpointer);
static void drgeo_translation_cb (GtkWidget * widget, gpointer);
static void drgeo_rotation_cb (GtkWidget * widget, gpointer);
static void drgeo_scale_cb (GtkWidget * widget, gpointer);
static void drgeo_numeric_cb (GtkWidget * widget, gpointer);
static void drgeo_angle_cb (GtkWidget * widget, gpointer);
static void drgeo_coordinates_cb (GtkWidget * widget, gpointer);

static void drgeo_macro_build_cb (GtkWidget * widget, gpointer);
static void drgeo_macro_play_cb (GtkWidget * widget, gpointer);

static void drgeo_zoom_25_cb (GtkWidget * widget, gpointer);
static void drgeo_zoom_50_cb (GtkWidget * widget, gpointer);
static void drgeo_zoom_75_cb (GtkWidget * widget, gpointer);
static void drgeo_zoom_100_cb (GtkWidget * widget, gpointer);
static void drgeo_zoom_125_cb (GtkWidget * widget, gpointer);
static void drgeo_zoom_150_cb (GtkWidget * widget, gpointer);
static void drgeo_zoom_175_cb (GtkWidget * widget, gpointer);
static void drgeo_zoom_200_cb (GtkWidget * widget, gpointer);

static void drgeo_delete_cb (GtkWidget * widget, gpointer);
static void drgeo_style_cb (GtkWidget * widget, gpointer);
static void drgeo_select_cb (GtkWidget * widget, gpointer);

static GnomeUIInfo point_menu[] =
{
	{
		GNOME_APP_UI_ITEM, N_ ("Free _point"),
     N_ ("Point on an object or the background plane"), drgeo_free_point_cb,
		NULL, NULL, (GnomeUIPixmapType) 0, 0, 'p',
		GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Middle"),
		N_ ("The midpoint of a segment or between two points"), drgeo_middle_point_cb,
		NULL, NULL, (GnomeUIPixmapType) 0, 0, 'm',
		GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Intersection"),
		N_ ("The point(s) of intersection between two objects"), drgeo_intersection_cb,
		NULL, NULL, (GnomeUIPixmapType) 0, 0, 'i', GDK_CONTROL_MASK
	},
	{
 GNOME_APP_UI_ITEM, N_ ("_Coordinates"), N_ ("Point given its coordinates"),
		drgeo_mark_point_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0
	},
	GNOMEUIINFO_END
};

static GnomeUIInfo curve_menu[] =
{
	{
	 GNOME_APP_UI_ITEM, N_ ("_Line"), N_ ("Line defined by two points"),
  drgeo_line_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'd', GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Half-line"),
		N_ ("Half-Line defined by two points, the first selected point is the origin"),
	      drgeo_half_line_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'e',
		GDK_CONTROL_MASK
	},
	{
   GNOME_APP_UI_ITEM, N_ ("_Segment"), N_ ("Segment defined by two points"),
		drgeo_segment_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 's',
		GDK_CONTROL_MASK
	},
	{
     GNOME_APP_UI_ITEM, N_ ("_Vector"), N_ ("Vector defined by two points"),
drgeo_vector_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'v', GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Circle"),
		N_ ("Circle defines by center and point, radius or segment"),
drgeo_circle_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'c', GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Arc Circle"),
		N_ ("Arc circle defined by three points"),
	     drgeo_arc_circle_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'a',
		GDK_CONTROL_MASK

	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Geometric locus"),
		N_ ("Locus defined by a free point and a relative point"),
	    drgeo_locus_point_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'l',
		GDK_CONTROL_MASK
	},
	GNOMEUIINFO_END
};

static GnomeUIInfo transformation_menu[] =
{
	{
		GNOME_APP_UI_ITEM, N_ ("_Parallel line"),
		N_ ("Line passing through one point and parallel to a line, half-line, etc."),
		drgeo_parallel_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'r',
		GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Orthogonal line"),
		N_ ("Line passing through one point and orthogonal to a line, half-line, etc."),
	     drgeo_orthogonal_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'o',
		GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Axial symmetry"),
		N_ ("Axial symmetry of an object. When ambiguity, the first selected line is the line to transform"),
		drgeo_reflexion_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Central symmetry"),
		N_ ("Central symmetry of an object. When ambiguity, the first selected point is the point to transform"),
		drgeo_symmetry_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Translation"),
		N_ ("Translation of an object. When ambiguity, the first selected vector is the vector to translate"),
		drgeo_translation_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 't',
		GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Rotation"),
		N_ ("Rotation of an object. When ambiguity, the first selected point is the point to rotate"),
		drgeo_rotation_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Scale"),
		N_ ("Scale an object. When ambiguity, the first selected point is the point to transform"),
		drgeo_scale_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'h', GDK_CONTROL_MASK
	},
	GNOMEUIINFO_END
};

static GnomeUIInfo numeric_menu[] =
{
	{
		GNOME_APP_UI_ITEM, N_ ("_Distance & length"),
	  N_ ("Distance between objects, curve length, or edit free value"),
		drgeo_numeric_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'n',
		GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Angle"),
		N_ ("Angle defined by three points or two vectors"),
 drgeo_angle_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'g', GDK_CONTROL_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Coordinates & equation"),
	      N_ ("Vector and point coordinates, line and circle equation"),
		drgeo_coordinates_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0
	},
	GNOMEUIINFO_END
};

static GnomeUIInfo macro_construction_menu[] =
{
	{
       GNOME_APP_UI_ITEM, N_ ("_Construct macro"), N_ ("Construct a macro"),
		drgeo_macro_build_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Execute macro"),
		N_ ("Execute pre-built macro"),
		drgeo_macro_play_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0
	},
	GNOMEUIINFO_END
};

static GnomeUIInfo zoom_value[] =
{
	GNOMEUIINFO_RADIOITEM (N_ ("Default 100%"), N_ ("Zoom to 100%"),
			       drgeo_zoom_100_cb, NULL),
	GNOMEUIINFO_RADIOITEM ("200%", N_ ("Zoom to 200%"),
			       drgeo_zoom_200_cb, NULL),
	GNOMEUIINFO_RADIOITEM ("175%", N_ ("Zoom to 175%"),
			       drgeo_zoom_175_cb, NULL),
	GNOMEUIINFO_RADIOITEM ("150%", N_ ("Zoom to 150%"),
			       drgeo_zoom_150_cb, NULL),
	GNOMEUIINFO_RADIOITEM ("125%", N_ ("Zoom to 125%"),
			       drgeo_zoom_125_cb, NULL),
	GNOMEUIINFO_RADIOITEM ("75%", N_ ("Zoom to 75%"),
			       drgeo_zoom_75_cb, NULL),
	GNOMEUIINFO_RADIOITEM ("50%", N_ ("Zoom to 50%"),
			       drgeo_zoom_50_cb, NULL),
	GNOMEUIINFO_RADIOITEM ("25%", N_ ("Zoom to 25%"),
			       drgeo_zoom_25_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo zoom_menu[] =
{
	GNOMEUIINFO_RADIOLIST (zoom_value),
	GNOMEUIINFO_END
};

static GnomeUIInfo drgeo_menu[] =
{
	GNOMEUIINFO_SUBTREE (N_ ("_Point"), point_menu),
	GNOMEUIINFO_SUBTREE (N_ ("_Curve"), curve_menu),
	GNOMEUIINFO_SUBTREE (N_ ("_Transformation"), transformation_menu),
	GNOMEUIINFO_SUBTREE (N_ ("_Numeric"), numeric_menu),
	GNOMEUIINFO_SUBTREE (N_ ("_Macro"), macro_construction_menu),
	GNOMEUIINFO_SUBTREE (N_ ("_Zoom"), zoom_menu),
	GNOMEUIINFO_SEPARATOR,
	{
		GNOME_APP_UI_ITEM, N_ ("M_ove an object"),
		N_ ("Select and move an object"),
   drgeo_select_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'm', GDK_MOD1_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Delete an object"),
		N_ ("Delete an object and its relatives"),
   drgeo_delete_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 'd', GDK_MOD1_MASK
	},
	{
		GNOME_APP_UI_ITEM, N_ ("_Style"),
		N_ ("Change the style of an object"),
    drgeo_style_cb, NULL, NULL, (GnomeUIPixmapType) 0, 0, 's', GDK_MOD1_MASK
	},
	GNOMEUIINFO_END
};

static void
drgeo_menu_cb (GtkWidget * widget, gpointer data)
{
	printf ("%p\n", data);
}

static void
drgeo_free_point_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (POINT_MODE, &(figure->pointFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_middle_point_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (POINT_MILIEU_MODE, &(figure->middlePointFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_intersection_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (POINT_INTER_MODE, &(figure->intersPointFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_mark_point_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (POINT_REPERE_MODE, &(figure->markPointFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}
static void
drgeo_line_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (DROITE_MODE, &(figure->lineFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_half_line_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (DEMI_DROITE_MODE, &(figure->halfLineFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_segment_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (SEGMENT_MODE, &(figure->segmentFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_vector_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (VECTEUR_MODE, &(figure->vectorFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_circle_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (CERCLE_MODE, &(figure->circleFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_arc_circle_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (ARC_CERCLE_MODE, &(figure->arcCircleFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_locus_point_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (LIEU_POINT_MODE, &(figure->locusPointFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_parallel_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (DROITE_PARALLELE_MODE, &(figure->parallelLineFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_orthogonal_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (DROITE_ORTHOGONALE_MODE, &(figure->orthogonalLineFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_reflexion_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (REFLEXION_MODE, &(figure->reflexionFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_symmetry_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (SYMETRIE_MODE, &(figure->symmetryFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_translation_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (TRANSLATION_MODE, &(figure->translationFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_rotation_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (ROTATION_MODE, &(figure->rotationFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_scale_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (HOMOTHETIE_MODE, &(figure->scaleFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_numeric_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (NUMERIQUE_MODE, &(figure->numericFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_angle_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (ANGLE_MODE, &(figure->angleFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_coordinates_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->buildTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (COORDONNEES_MODE, &(figure->coordinatesFilter));

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_delete_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->deleteTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (EFFACE_OBJET_MODE, NULL);

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_style_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->styleTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (MISE_EN_FORME_MODE, NULL);

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_macro_build_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	//drgeoFigure * figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->macroBuildTool);

	//figure = area->figure;
	// Change the currend mode of the drgeo figure
	area->figure->setMode (MACRO_ENREGISTRE_MODE, NULL);

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_macro_play_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	//drgeoFigure * figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->macroPlayTool);

	//figure = area->figure;
	// Change the currend mode of the drgeo figure
	area->figure->setMode (MACRO_PLAY_MODE, NULL);

	((drgeoGtkDrawable *) data)->refresh ();
}

static void
drgeo_zoom_25_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	gtkfig->setScale (7.5);
	/* XXX Should update the rules also */

	gtkfig->refresh ();
}
static void
drgeo_zoom_50_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	gtkfig->setScale (15.0);
	/* XXX Should update the rules also */

	gtkfig->refresh ();
}
static void
drgeo_zoom_75_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	gtkfig->setScale (22.5);
	/* XXX Should update the rules also */

	gtkfig->refresh ();
}
static void
drgeo_zoom_100_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	gtkfig->setScale (30.0);
	/* XXX Should update the rules also */

	gtkfig->refresh ();
}
static void
drgeo_zoom_125_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	gtkfig->setScale (37.5);
	/* XXX Should update the rules also */

	gtkfig->refresh ();
}
static void
drgeo_zoom_150_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	gtkfig->setScale (45.0);
	/* XXX Should update the rules also */

	gtkfig->refresh ();
}
static void
drgeo_zoom_175_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	gtkfig->setScale (52.5);
	/* XXX Should update the rules also */

	gtkfig->refresh ();
}
static void
drgeo_zoom_200_cb (GtkWidget * widget, gpointer data)
{
	drgeoGtkDrawable *gtkfig;

	gtkfig = (drgeoGtkDrawable *) data;
	gtkfig->setScale (60.0);
	/* XXX Should update the rules also */

	gtkfig->refresh ();
}

static void
drgeo_select_cb (GtkWidget * widget, gpointer data)
{
	drgeoDrawableUI *area;
	drgeoFigure *figure;

	area = (drgeoDrawableUI *) data;
	// Change the current tool in the drawable
	area->setTool (area->selectTool);

	figure = area->figure;
	// Change the currend mode of the drgeo figure
	figure->setMode (SOURIS_SELECT_MODE, NULL);

	((drgeoGtkDrawable *) data)->refresh ();
}

void
drgeo_install_popup_menu (GtkWidget * w, gpointer area)
{
	gnome_popup_menu_attach (drgeoPopupMenu, w, area);
}

void on_menuBar_orientation_changed (GtkToolbar *bar, 
				     GtkOrientation orientation, 
				     gpointer data)
{
	GtkWidget *w;
	GtkToolbar *subbar;
	GList *list;
	int i;

	if (orientation == GTK_ORIENTATION_VERTICAL)	
		orientation = GTK_ORIENTATION_HORIZONTAL;
	else 
		orientation = GTK_ORIENTATION_VERTICAL;
			
	/* change the orientation of the sub toolbar */
	subbar = GTK_TOOLBAR (gtk_object_get_data (GTK_OBJECT (bar), "pointBar"));
	gtk_toolbar_set_orientation (subbar, orientation);
	subbar = GTK_TOOLBAR (gtk_object_get_data (GTK_OBJECT (bar), "curveBar"));
	gtk_toolbar_set_orientation (subbar, orientation);
	subbar = GTK_TOOLBAR (gtk_object_get_data (GTK_OBJECT (bar), "transformationBar"));
	gtk_toolbar_set_orientation (subbar, orientation);
	subbar = GTK_TOOLBAR (gtk_object_get_data (GTK_OBJECT (bar), "numericBar"));
	gtk_toolbar_set_orientation (subbar, orientation);
	subbar = GTK_TOOLBAR (gtk_object_get_data (GTK_OBJECT (bar), "macroBar"));
	gtk_toolbar_set_orientation (subbar, orientation);
	subbar = GTK_TOOLBAR (gtk_object_get_data (GTK_OBJECT (bar), "otherBar"));
	gtk_toolbar_set_orientation (subbar, orientation);

	/* Search for the separator and change their direction */
	for (list = gtk_container_children (GTK_CONTAINER (bar)), i = 0;
	     list; list = g_list_next (list), i++)
	{
		if (GTK_IS_SEPARATOR (GTK_OBJECT (list->data)))
		{
		    gtk_container_remove (GTK_CONTAINER(bar), 
					  GTK_WIDGET (list->data));
		    if (orientation == GTK_ORIENTATION_VERTICAL)
		    {
			    w = gtk_vseparator_new ();
			    gtk_object_set (GTK_OBJECT (w), "height", (gint) 20, 
					    "width", 32, NULL);
		    }
		    else
		    {
			    w = gtk_hseparator_new ();
			    gtk_object_set (GTK_OBJECT (w), "height", (gint) 32, 
					    "width", 20, NULL);
		    }
		    gtk_widget_show (w);
		    gtk_toolbar_insert_widget (bar, w, NULL, NULL, i);
		}
	}
}

void 
on_drgeoMenu_clicked (GtkWidget *w, gpointer data)
{
	GtkWidget *subbar;

	subbar = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (w), "subBar"));
	gtk_widget_show (subbar);
}

static gpointer 
common_icon_clicked (GtkWidget *w)
{
	DrGeniusMDIChild  *child;
	drgeoFigure *figure;
	gpointer data;
	
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	figure = (drgeoFigure *) drgeo_mdi_child_get_figure (child);
	data = (gpointer) (figure->getDrawable ());	
	gtk_widget_hide (w->parent->parent);
	return data;
}

void 
on_freePoint_clicked (GtkWidget *w, gpointer data)
{
	drgeo_free_point_cb (w, common_icon_clicked (w));
}

void 
on_middlePoint_clicked (GtkWidget *w, gpointer data)
{
	drgeo_middle_point_cb (w, common_icon_clicked (w));

}

void 
on_intersection_clicked (GtkWidget *w, gpointer data)
{
	drgeo_intersection_cb (w, common_icon_clicked (w));
}

void 
on_coordinatesPoint_clicked (GtkWidget *w, gpointer data)
{
	drgeo_mark_point_cb (w, common_icon_clicked (w));
}

void 
on_parallel_clicked (GtkWidget *w, gpointer data)
{
	drgeo_parallel_cb (w, common_icon_clicked (w));
}

void 
on_perpendicular_clicked (GtkWidget *w, gpointer data)
{
	drgeo_orthogonal_cb (w, common_icon_clicked (w));
}

void 
on_reflexion_clicked (GtkWidget *w, gpointer data)
{
	drgeo_reflexion_cb (w, common_icon_clicked (w));
}

void
on_symmetry_clicked (GtkWidget *w, gpointer data)
{
	drgeo_symmetry_cb (w, common_icon_clicked (w));
}

void 
on_translation_clicked (GtkWidget *w, gpointer data)
{
	drgeo_translation_cb (w, common_icon_clicked (w));
}
void
on_rotation_clicked (GtkWidget *w, gpointer data)
{
	drgeo_rotation_cb (w, common_icon_clicked (w));
}
void
on_scale_clicked (GtkWidget *w, gpointer data)
{
	drgeo_scale_cb (w, common_icon_clicked (w));
}
void
on_distance_clicked (GtkWidget *w, gpointer data)
{
	drgeo_numeric_cb (w, common_icon_clicked (w));
}
void
on_angle_clicked (GtkWidget *w, gpointer data)
{
	drgeo_angle_cb (w, common_icon_clicked (w));
}
void
on_equation_clicked (GtkWidget *w, gpointer data)
{
	drgeo_coordinates_cb (w, common_icon_clicked (w));
}
void
on_buildMacro_clicked (GtkWidget *w, gpointer data)
{
	drgeo_macro_build_cb (w, common_icon_clicked (w));
}
void
on_runMacro_clicked (GtkWidget *w, gpointer data)
{
	drgeo_macro_play_cb (w, common_icon_clicked (w));
}
void
on_line_clicked (GtkWidget *w, gpointer data)
{
	drgeo_line_cb (w, common_icon_clicked (w));
}
void
on_halfLine_clicked (GtkWidget *w, gpointer data)
{
	drgeo_half_line_cb (w, common_icon_clicked (w));
}
void
on_segment_clicked (GtkWidget *w, gpointer data)
{
	drgeo_segment_cb (w, common_icon_clicked (w));
}
void
on_vector_clicked (GtkWidget *w, gpointer data)
{
	drgeo_vector_cb (w, common_icon_clicked (w));
}
void
on_circle_clicked (GtkWidget *w, gpointer data)
{
	drgeo_circle_cb (w, common_icon_clicked (w));
}
void
on_arcCircle_clicked (GtkWidget *w, gpointer data)
{
	drgeo_arc_circle_cb (w, common_icon_clicked (w));
}
void
on_locus_clicked (GtkWidget *w, gpointer data)
{
	drgeo_locus_point_cb (w, common_icon_clicked (w));
}
void
on_moveItem_clicked (GtkWidget *w, gpointer data)
{
	drgeo_select_cb (w, common_icon_clicked (w));
}
void
on_styleItem_clicked (GtkWidget *w, gpointer data)
{
	drgeo_style_cb (w, common_icon_clicked (w));
}
void
on_deleteItem_clicked (GtkWidget *w, gpointer data)
{
	drgeo_delete_cb (w, common_icon_clicked (w));
}

void 
on_zoom_valeur_changed (GtkEditable *w, gpointer data)
{
	DrGeniusMDIChild  *child;
	drgeoFigure *figure;
	drgeoGtkDrawable *gtkfig;
	
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	figure = (drgeoFigure *) drgeo_mdi_child_get_figure (child);
	gtkfig = (drgeoGtkDrawable *) (figure->getDrawable ());	

	gtkfig->setScale (3. * strtod (gtk_entry_get_text 
				       (GTK_ENTRY (w)), NULL) / 10. );
	gtkfig->refresh ();
}

GtkWidget *
build_drgeo_popup_menu (void)
{
	drgeoPopupMenu = gnome_popup_menu_new (drgeo_menu);
	return (drgeoPopupMenu);
}
