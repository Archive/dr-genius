#ifndef DRGEO_TOOL_H
#define DRGEO_TOOL_H

class drgeoTool;

#include "drgeo_drawable.h"
#include "drgeo_macro.h"

class drgeoTool {
 public:

	virtual void initialize (class drgeoDrawableUI * drawable) = 0;
	virtual void handlePress (class drgeoDrawableUI * drawable,
				  class drgeoPoint & where) = 0;
	virtual void handleRelease (class drgeoDrawableUI * drawable,
				    class drgeoPoint & where) = 0;
	virtual void handleMouseAt (class drgeoDrawableUI * drawable,
				    class drgeoPoint & where) = 0;
	virtual void handleChoice (class drgeoDrawableUI * drawable,
				   class geometricObject * item) = 0;
	virtual void finish (class drgeoDrawableUI * drawable) = 0;
};

class drgeoNullTool:public drgeoTool {
 public:

	void initialize (class drgeoDrawableUI * drawable);
	void handlePress (class drgeoDrawableUI * drawable,
			  class drgeoPoint & where);
	void handleRelease (class drgeoDrawableUI * drawable,
			    class drgeoPoint & where);
	void handleMouseAt (class drgeoDrawableUI * drawable,
			    class drgeoPoint & where);
	void handleChoice (class drgeoDrawableUI * drawable,
			   class geometricObject * item);
	void finish (class drgeoDrawableUI * drawable);
};


enum drgeoState {
	drgeoStateNeutral,	// The mouse is neutral.
	 drgeoStateItemGrabbed,	// An item has just been grabbed, but
	// not yet dragged..
	 drgeoStateItemDragged,	// An item is being dragged.
	 drgeoStateItemChoice,	// A choice is being done by the user.
	 drgeoStateItemSelected	// An item has just been selected.
};

class drgeoSelectTool:public drgeoTool {
 public:

	void initialize (class drgeoDrawableUI * drawable);
	void handlePress (class drgeoDrawableUI * drawable,
			  class drgeoPoint & where);
	void handleRelease (class drgeoDrawableUI * drawable,
			    class drgeoPoint & where);
	void handleMouseAt (class drgeoDrawableUI * drawable,
			    class drgeoPoint & where);
	void handleChoice (class drgeoDrawableUI * drawable,
			   class geometricObject * item);
	void finish (class drgeoDrawableUI * drawable);

 private:

	drgeoState state;	// This state variable is used in
	// event handling methods.
	
	// position where start the drag
	drgeoPoint start;  
	drgeoPoint last;	// Last position of the mouse.  This
	// information is used during drag.
	
	drgeoPoint spot;	// This variable is used to store the
	// location of the last item selected
	// when one item has been selected
	// among multiple items.

	geometricObject *item;		// Item selected during the last
	// multiple choice selection.

	bool away;		// Used when tracking move relative to
	// spot.

	bool tipOn;
};

// Build tool
class drgeoBuildTool:public drgeoTool {
 public:

	void initialize (class drgeoDrawableUI * drawable);
	void handlePress (class drgeoDrawableUI * drawable,
			  class drgeoPoint & where);
	void handleRelease (class drgeoDrawableUI * drawable,
			    class drgeoPoint & where);
	void handleMouseAt (class drgeoDrawableUI * drawable,
			    class drgeoPoint & where);
	void handleChoice (class drgeoDrawableUI * drawable,
			   class geometricObject * item);
	void finish (class drgeoDrawableUI * drawable);

 protected:

	  drgeoState state;	// This state variable is used in
	// event handling methods.

	drgeoPoint spot;	// This variable is used to store the
	// location of the last item selected
	// when one item has been selected
	// among multiple items.

	drgeoPoint last;	// Last position of the mouse.  This
	// information is used during drag.

	geometricObject *item;		// Item selected during the last
	// multiple choice selection.

	bool away;		// Used when tracking move relative to
	// spot.

	bool tipOn;
};

class drgeoMacroBuildTool:public drgeoBuildTool {
 public:

	void initialize (class drgeoDrawableUI * drawable);

	// The following methods are inherited from drgeoBuildTool.

	// void handlePress (class drgeoDrawableUI *drawable,
	//                    class drgeoPoint& where);
	// void handleRelease (class drgeoDrawableUI *drawable,
	//                      class drgeoPoint& where);
	// void handleMouseAt (class drgeoDrawableUI *drawable,
	//                      class drgeoPoint& where);

	void handleChoice (class drgeoDrawableUI * drawable,
			   class geometricObject * item);

	void finish (class drgeoDrawableUI * drawable);

 private:

	  class drgeoMacroBuilder * builder;
	// The dialog used to control the macro builder.
	class drgeoMacroBuildDialog *dialog;
};

class drgeoMacroPlayTool:public drgeoBuildTool {
 public:

	void initialize (class drgeoDrawableUI * drawable);

	// The following methods are inherited from drgeoBuildTool.

	// void handlePress (class drgeoDrawableUI *drawable,
	//                    class drgeoPoint& where);
	// void handleRelease (class drgeoDrawableUI *drawable,
	//                      class drgeoPoint& where);
	// void handleMouseAt (class drgeoDrawableUI *drawable,
	//                      class drgeoPoint& where);

	void handleChoice (class drgeoDrawableUI * drawable,
			   class geometricObject * item);

	void finish (class drgeoDrawableUI * drawable);

 private:

	  class drgeoMacroPlayer * player;
	// The dialog used to control the macro builder.
	class drgeoMacroPlayDialog *dialog;
};

class drgeoStyleTool:public drgeoBuildTool {
 public:

	void initialize (class drgeoDrawableUI * drawable);

	// The following methods are inherited from drgeoBuildTool.

	// void handlePress (class drgeoDrawableUI *drawable,
	//                    class drgeoPoint& where);
	// void handleRelease (class drgeoDrawableUI *drawable,
	//                      class drgeoPoint& where);
	// void handleMouseAt (class drgeoDrawableUI *drawable,
	//                      class drgeoPoint& where);

	void handleChoice (class drgeoDrawableUI * drawable,
			   class geometricObject * item);

	void finish (class drgeoDrawableUI * drawable);

 private:

	// The dialog used to control the style of items.
	  class drgeoStyleDialog * dialog;
};

class drgeoDeleteTool:public drgeoBuildTool {
 public:

	// The following methods are inherited from drgeoBuildTool.

	// void initialize (class drgeoDrawableUI *drawable); 
	// void handlePress (class drgeoDrawableUI *drawable,
	//                    class drgeoPoint& where);
	// void handleRelease (class drgeoDrawableUI *drawable,
	//                      class drgeoPoint& where);
	// void handleMouseAt (class drgeoDrawableUI *drawable,
	//                      class drgeoPoint& where);

	void handleChoice (class drgeoDrawableUI * drawable,
			   class geometricObject * item);

	// void finish (class drgeoDrawableUI *drawable);

};

#endif
