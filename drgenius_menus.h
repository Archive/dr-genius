
#ifndef DRGEO_MENUS_H
#define DRGEO_MENUS_H

#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */

	void drgeo_install_menus_and_toolbar (GtkWidget * app);
	void reconcile_grayout_widget (void);
	void reconcile_grayout_undo (void);
/* Callback for the Session Save Dialog */
	void on_sessionSaveSelection_clicked (GtkWidget * widget, gpointer data);
	void on_sessionSaveAll_clicked (GtkWidget * widget, gpointer data);
	void on_sessionCancel_clicked (GtkWidget * widget, gpointer data);

#ifdef __cplusplus
}

#endif				/* __cplusplus */
#endif
