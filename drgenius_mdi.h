
#ifndef DRGENIUS_MDI_H
#define DRGENIUS_MDI_H

#include <gnome.h>
#include "xmlinclude.h"

#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */

	GnomeMDI *drgenius_mdi_new (const gchar * file_name);

	void drgenius_mdi_close (GnomeMDI * mdi);

/* Function to close the current view */
	void drgenius_mdi_close_view (GnomeMDI * mdi);

/* Function to open a session (multiple data view) */
	void drgenius_mdi_session_open (GnomeMDI * mdi, char *filename);

/* Function for opening a new figure */
	void drgenius_mdi_figure_open (GnomeMDI * mdi, xmlNodePtr tree);

/* Function for opening a new calculator console */
	void drgenius_mdi_calculator_open (GnomeMDI * mdi);

/* Function for opening a new canvas */
	void drgenius_mdi_canvas_open (GnomeMDI * mdi, xmlNodePtr tree);

/* Function for opening a new GEL editor view */
	void drgenius_mdi_gel_open (GnomeMDI * mdi, char *filename);

#ifdef __cplusplus
}

#endif				/* __cplusplus */
#endif
