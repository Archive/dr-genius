#include <config.h>
#include "drgeo_view.h"
#include "gobobjs/drgenius-mdi-child.h"

#include "drgeo_gtkdrawable.h"
#include "macro.h"

extern GnomeMDI *mdi;		// XXX ugly.


gboolean
drgeo_save_handler (DrGeniusMDIChild * child, char *filename)
{
	drgeoDrawable *drawable;

	if (!filename)
		return FALSE;

	/* XXX This seems very broken, since the figure data seems to be stored
	   on the view rather then separately on the child as it should be, this
	   will make a lot of problems for us if we want to support bonobo later */
	if (mdi->active_view) {
		drawable = (drgeoGtkDrawable *)
			gtk_object_get_data (GTK_OBJECT (mdi->active_view), "figure");
		// XXX better error checking here.

		// (drawable->getFigure ())->saveAs (filename);

		// Update the view title to match the new filename.  Just show
		// the file name in the view title, not its directory path.
		gnome_mdi_child_set_name (mdi->active_child, g_basename (filename));
	}
}
// Helper functions to get the name of avalaible macro in the registry
char *
firstMacroName ()
{
	drgeoMacroRegistry *registry;
	macro *mac;

      registry = drgeoMacroRegistry::get ();

	if ((mac = registry->first ()) == NULL)
		return NULL;
	return mac->getName ();
}
char *
nextMacroName ()
{
	drgeoMacroRegistry *registry;
	macro *mac;

      registry = drgeoMacroRegistry::get ();

	if ((mac = registry->next ()) == NULL)
		return NULL;
	return mac->getName ();
}

gboolean
saveFigure (GnomeMDIChild * child, xmlNodePtr tree)
{
	drgeoGtkDrawable *drawable;
	GtkWidget *view;

	if (child->views == NULL)
		return false;
	view = (GtkWidget *) g_list_nth_data (child->views, 0);
	if (view == NULL)
		return false;
	drawable = (drgeoGtkDrawable *)
		gtk_object_get_data (GTK_OBJECT (view), "figure");
	if (drawable == NULL)
		return FALSE;
	return drawable->getFigure ()->saveAs (tree, child->name);
}

gboolean
saveMacro (gchar * name, xmlNodePtr tree)
{
	drgeoMacroRegistry *registry;

      registry = drgeoMacroRegistry::get ();
	return registry->save (name, tree);
}

gboolean 
loadMacro (xmlNodePtr macroXml)
{
	drgeoMacroRegistry *registry;

      registry = drgeoMacroRegistry::get ();
	return registry->load (macroXml);
}

gboolean
exportFigureToLatex (GnomeMDIChild * child, gchar *fileName)
{
	drgeoGtkDrawable *drawable;
	GtkWidget *view;

	if (child->views == NULL)
		return FALSE;
	view = (GtkWidget *) g_list_nth_data (child->views, 0);
	if (view == NULL)
		return FALSE;

	drawable = (drgeoGtkDrawable *)
		gtk_object_get_data (GTK_OBJECT (view), "figure");
	if (drawable == NULL)
		return FALSE;
	return drawable->getFigure ()-> exportToLatex(fileName);
}

gboolean
exportFigureToPostScript (GnomeMDIChild * child, gchar *fileName)
{
	drgeoGtkDrawable *drawable;
	GtkWidget *view;

	if (child->views == NULL)
		return FALSE;
	view = (GtkWidget *) g_list_nth_data (child->views, 0);
	if (view == NULL)
		return FALSE;

	drawable = (drgeoGtkDrawable *)
		gtk_object_get_data (GTK_OBJECT (view), "figure");
	if (drawable == NULL)
		return FALSE;
	return drawable->getFigure ()-> exportToPostScript(fileName);
}
